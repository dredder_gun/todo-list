CREATE TABLE users (id serial PRIMARY KEY, name varchar(40) NOT NULL CHECK (name <> ''));

CREATE TABLE lists (id serial PRIMARY KEY, user_id integer REFERENCES users(id) ON DELETE CASCADE, title varchar(40) NOT NULL);
