// Compiled by ClojureScript 1.9.89 {}
goog.provide('figwheel.connect.build_client');
goog.require('cljs.core');
goog.require('todomvc.core');
goog.require('figwheel.client');
goog.require('figwheel.client.utils');
figwheel.client.start.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),(function() { 
var G__33315__delegate = function (x){
if(cljs.core.truth_(todomvc.core.main)){
return cljs.core.apply.call(null,todomvc.core.main,x);
} else {
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: :on-jsload hook 'todomvc.core/main' is missing");
}
};
var G__33315 = function (var_args){
var x = null;
if (arguments.length > 0) {
var G__33316__i = 0, G__33316__a = new Array(arguments.length -  0);
while (G__33316__i < G__33316__a.length) {G__33316__a[G__33316__i] = arguments[G__33316__i + 0]; ++G__33316__i;}
  x = new cljs.core.IndexedSeq(G__33316__a,0);
} 
return G__33315__delegate.call(this,x);};
G__33315.cljs$lang$maxFixedArity = 0;
G__33315.cljs$lang$applyTo = (function (arglist__33317){
var x = cljs.core.seq(arglist__33317);
return G__33315__delegate(x);
});
G__33315.cljs$core$IFn$_invoke$arity$variadic = G__33315__delegate;
return G__33315;
})()
,new cljs.core.Keyword(null,"build-id","build-id",1642831089),"client",new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),"ws://localhost:3450/figwheel-ws"], null));

//# sourceMappingURL=build_client.js.map?rel=1494938151088