// Compiled by ClojureScript 1.9.89 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('cljs.core.async');
goog.require('goog.object');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
goog.require('cljs.repl');
goog.require('figwheel.client.heads_up');
goog.require('cljs.reader');
figwheel.client._figwheel_version_ = "0.5.10";
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(var_args){
var args38148 = [];
var len__30996__auto___38151 = arguments.length;
var i__30997__auto___38152 = (0);
while(true){
if((i__30997__auto___38152 < len__30996__auto___38151)){
args38148.push((arguments[i__30997__auto___38152]));

var G__38153 = (i__30997__auto___38152 + (1));
i__30997__auto___38152 = G__38153;
continue;
} else {
}
break;
}

var G__38150 = args38148.length;
switch (G__38150) {
case 2:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args38148.length)].join('')));

}
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2 = (function (stream,args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"stream","stream",1534941648),stream,new cljs.core.Keyword(null,"args","args",1315556576),args], null)], null));

return null;
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1 = (function (args){
return figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);
});

figwheel.client.figwheel_repl_print.cljs$lang$maxFixedArity = 2;

figwheel.client.console_out_print = (function figwheel$client$console_out_print(args){
return console.log.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.console_err_print = (function figwheel$client$console_err_print(args){
return console.error.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.repl_out_print_fn = (function figwheel$client$repl_out_print_fn(var_args){
var args__31003__auto__ = [];
var len__30996__auto___38156 = arguments.length;
var i__30997__auto___38157 = (0);
while(true){
if((i__30997__auto___38157 < len__30996__auto___38156)){
args__31003__auto__.push((arguments[i__30997__auto___38157]));

var G__38158 = (i__30997__auto___38157 + (1));
i__30997__auto___38157 = G__38158;
continue;
} else {
}
break;
}

var argseq__31004__auto__ = ((((0) < args__31003__auto__.length))?(new cljs.core.IndexedSeq(args__31003__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__31004__auto__);
});

figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_out_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);

return null;
});

figwheel.client.repl_out_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_out_print_fn.cljs$lang$applyTo = (function (seq38155){
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq38155));
});

figwheel.client.repl_err_print_fn = (function figwheel$client$repl_err_print_fn(var_args){
var args__31003__auto__ = [];
var len__30996__auto___38160 = arguments.length;
var i__30997__auto___38161 = (0);
while(true){
if((i__30997__auto___38161 < len__30996__auto___38160)){
args__31003__auto__.push((arguments[i__30997__auto___38161]));

var G__38162 = (i__30997__auto___38161 + (1));
i__30997__auto___38161 = G__38162;
continue;
} else {
}
break;
}

var argseq__31004__auto__ = ((((0) < args__31003__auto__.length))?(new cljs.core.IndexedSeq(args__31003__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__31004__auto__);
});

figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_err_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"err","err",-2089457205),args);

return null;
});

figwheel.client.repl_err_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_err_print_fn.cljs$lang$applyTo = (function (seq38159){
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq38159));
});

figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

cljs.core.set_print_fn_BANG_.call(null,figwheel.client.repl_out_print_fn);

cljs.core.set_print_err_fn_BANG_.call(null,figwheel.client.repl_err_print_fn);

return null;
});
figwheel.client.autoload_QMARK_ = (function figwheel$client$autoload_QMARK_(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),true);
});
figwheel.client.toggle_autoload = (function figwheel$client$toggle_autoload(){
var res = figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),cljs.core.not.call(null,figwheel.client.autoload_QMARK_.call(null)));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Toggle autoload deprecated! Use (figwheel.client/set-autoload! false)")].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Figwheel autoloading "),cljs.core.str((cljs.core.truth_(figwheel.client.autoload_QMARK_.call(null))?"ON":"OFF"))].join(''));

return res;
});
goog.exportSymbol('figwheel.client.toggle_autoload', figwheel.client.toggle_autoload);
/**
 * Figwheel by default loads code changes as you work. Sometimes you
 *   just want to work on your code without the ramifications of
 *   autoloading and simply load your code piecemeal in the REPL. You can
 *   turn autoloading on and of with this method. 
 * 
 *   (figwheel.client/set-autoload false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_autoload = (function figwheel$client$set_autoload(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),b);
});
goog.exportSymbol('figwheel.client.set_autoload', figwheel.client.set_autoload);
figwheel.client.repl_pprint = (function figwheel$client$repl_pprint(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),true);
});
goog.exportSymbol('figwheel.client.repl_pprint', figwheel.client.repl_pprint);
/**
 * This method gives you the ability to turn the pretty printing of
 *   the REPL's return value on and off.
 *   
 *   (figwheel.client/set-repl-pprint false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_repl_pprint = (function figwheel$client$set_repl_pprint(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),b);
});
goog.exportSymbol('figwheel.client.set_repl_pprint', figwheel.client.set_repl_pprint);
figwheel.client.repl_result_pr_str = (function figwheel$client$repl_result_pr_str(v){
if(cljs.core.truth_(figwheel.client.repl_pprint.call(null))){
return figwheel.client.utils.pprint_to_string.call(null,v);
} else {
return cljs.core.pr_str.call(null,v);
}
});
goog.exportSymbol('figwheel.client.repl_result_pr_str', figwheel.client.repl_result_pr_str);
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel$client$get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__38163){
var map__38166 = p__38163;
var map__38166__$1 = ((((!((map__38166 == null)))?((((map__38166.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38166.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38166):map__38166);
var message = cljs.core.get.call(null,map__38166__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var class$ = cljs.core.get.call(null,map__38166__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return [cljs.core.str(class$),cljs.core.str(" : "),cljs.core.str(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__29921__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__29921__auto__)){
return or__29921__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__29909__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__29909__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__29909__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return (cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts)));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__32227__auto___38328 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto___38328,ch){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto___38328,ch){
return (function (state_38297){
var state_val_38298 = (state_38297[(1)]);
if((state_val_38298 === (7))){
var inst_38293 = (state_38297[(2)]);
var state_38297__$1 = state_38297;
var statearr_38299_38329 = state_38297__$1;
(statearr_38299_38329[(2)] = inst_38293);

(statearr_38299_38329[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (1))){
var state_38297__$1 = state_38297;
var statearr_38300_38330 = state_38297__$1;
(statearr_38300_38330[(2)] = null);

(statearr_38300_38330[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (4))){
var inst_38250 = (state_38297[(7)]);
var inst_38250__$1 = (state_38297[(2)]);
var state_38297__$1 = (function (){var statearr_38301 = state_38297;
(statearr_38301[(7)] = inst_38250__$1);

return statearr_38301;
})();
if(cljs.core.truth_(inst_38250__$1)){
var statearr_38302_38331 = state_38297__$1;
(statearr_38302_38331[(1)] = (5));

} else {
var statearr_38303_38332 = state_38297__$1;
(statearr_38303_38332[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (15))){
var inst_38257 = (state_38297[(8)]);
var inst_38272 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_38257);
var inst_38273 = cljs.core.first.call(null,inst_38272);
var inst_38274 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_38273);
var inst_38275 = [cljs.core.str("Figwheel: Not loading code with warnings - "),cljs.core.str(inst_38274)].join('');
var inst_38276 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),inst_38275);
var state_38297__$1 = state_38297;
var statearr_38304_38333 = state_38297__$1;
(statearr_38304_38333[(2)] = inst_38276);

(statearr_38304_38333[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (13))){
var inst_38281 = (state_38297[(2)]);
var state_38297__$1 = state_38297;
var statearr_38305_38334 = state_38297__$1;
(statearr_38305_38334[(2)] = inst_38281);

(statearr_38305_38334[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (6))){
var state_38297__$1 = state_38297;
var statearr_38306_38335 = state_38297__$1;
(statearr_38306_38335[(2)] = null);

(statearr_38306_38335[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (17))){
var inst_38279 = (state_38297[(2)]);
var state_38297__$1 = state_38297;
var statearr_38307_38336 = state_38297__$1;
(statearr_38307_38336[(2)] = inst_38279);

(statearr_38307_38336[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (3))){
var inst_38295 = (state_38297[(2)]);
var state_38297__$1 = state_38297;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38297__$1,inst_38295);
} else {
if((state_val_38298 === (12))){
var inst_38256 = (state_38297[(9)]);
var inst_38270 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_38256,opts);
var state_38297__$1 = state_38297;
if(cljs.core.truth_(inst_38270)){
var statearr_38308_38337 = state_38297__$1;
(statearr_38308_38337[(1)] = (15));

} else {
var statearr_38309_38338 = state_38297__$1;
(statearr_38309_38338[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (2))){
var state_38297__$1 = state_38297;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38297__$1,(4),ch);
} else {
if((state_val_38298 === (11))){
var inst_38257 = (state_38297[(8)]);
var inst_38262 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_38263 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_38257);
var inst_38264 = cljs.core.async.timeout.call(null,(1000));
var inst_38265 = [inst_38263,inst_38264];
var inst_38266 = (new cljs.core.PersistentVector(null,2,(5),inst_38262,inst_38265,null));
var state_38297__$1 = state_38297;
return cljs.core.async.ioc_alts_BANG_.call(null,state_38297__$1,(14),inst_38266);
} else {
if((state_val_38298 === (9))){
var inst_38257 = (state_38297[(8)]);
var inst_38283 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Figwheel: code autoloading is OFF");
var inst_38284 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_38257);
var inst_38285 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_38284);
var inst_38286 = [cljs.core.str("Not loading: "),cljs.core.str(inst_38285)].join('');
var inst_38287 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),inst_38286);
var state_38297__$1 = (function (){var statearr_38310 = state_38297;
(statearr_38310[(10)] = inst_38283);

return statearr_38310;
})();
var statearr_38311_38339 = state_38297__$1;
(statearr_38311_38339[(2)] = inst_38287);

(statearr_38311_38339[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (5))){
var inst_38250 = (state_38297[(7)]);
var inst_38252 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_38253 = (new cljs.core.PersistentArrayMap(null,2,inst_38252,null));
var inst_38254 = (new cljs.core.PersistentHashSet(null,inst_38253,null));
var inst_38255 = figwheel.client.focus_msgs.call(null,inst_38254,inst_38250);
var inst_38256 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_38255);
var inst_38257 = cljs.core.first.call(null,inst_38255);
var inst_38258 = figwheel.client.autoload_QMARK_.call(null);
var state_38297__$1 = (function (){var statearr_38312 = state_38297;
(statearr_38312[(9)] = inst_38256);

(statearr_38312[(8)] = inst_38257);

return statearr_38312;
})();
if(cljs.core.truth_(inst_38258)){
var statearr_38313_38340 = state_38297__$1;
(statearr_38313_38340[(1)] = (8));

} else {
var statearr_38314_38341 = state_38297__$1;
(statearr_38314_38341[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (14))){
var inst_38268 = (state_38297[(2)]);
var state_38297__$1 = state_38297;
var statearr_38315_38342 = state_38297__$1;
(statearr_38315_38342[(2)] = inst_38268);

(statearr_38315_38342[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (16))){
var state_38297__$1 = state_38297;
var statearr_38316_38343 = state_38297__$1;
(statearr_38316_38343[(2)] = null);

(statearr_38316_38343[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (10))){
var inst_38289 = (state_38297[(2)]);
var state_38297__$1 = (function (){var statearr_38317 = state_38297;
(statearr_38317[(11)] = inst_38289);

return statearr_38317;
})();
var statearr_38318_38344 = state_38297__$1;
(statearr_38318_38344[(2)] = null);

(statearr_38318_38344[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38298 === (8))){
var inst_38256 = (state_38297[(9)]);
var inst_38260 = figwheel.client.reload_file_state_QMARK_.call(null,inst_38256,opts);
var state_38297__$1 = state_38297;
if(cljs.core.truth_(inst_38260)){
var statearr_38319_38345 = state_38297__$1;
(statearr_38319_38345[(1)] = (11));

} else {
var statearr_38320_38346 = state_38297__$1;
(statearr_38320_38346[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__32227__auto___38328,ch))
;
return ((function (switch__32115__auto__,c__32227__auto___38328,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__32116__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__32116__auto____0 = (function (){
var statearr_38324 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_38324[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__32116__auto__);

(statearr_38324[(1)] = (1));

return statearr_38324;
});
var figwheel$client$file_reloader_plugin_$_state_machine__32116__auto____1 = (function (state_38297){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_38297);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e38325){if((e38325 instanceof Object)){
var ex__32119__auto__ = e38325;
var statearr_38326_38347 = state_38297;
(statearr_38326_38347[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38297);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38325;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38348 = state_38297;
state_38297 = G__38348;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__32116__auto__ = function(state_38297){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__32116__auto____1.call(this,state_38297);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloader_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__32116__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__32116__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto___38328,ch))
})();
var state__32229__auto__ = (function (){var statearr_38327 = f__32228__auto__.call(null);
(statearr_38327[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto___38328);

return statearr_38327;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto___38328,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__38349_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__38349_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
figwheel.client.get_ua_product = (function figwheel$client$get_ua_product(){
if(cljs.core.truth_(figwheel.client.utils.node_env_QMARK_.call(null))){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
}
});
var base_path_38352 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_38352){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,opts,result_handler){
try{figwheel.client.enable_repl_print_BANG_.call(null);

var result_value = figwheel.client.utils.eval_helper.call(null,code,opts);
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),result_value], null));
}catch (e38351){if((e38351 instanceof Error)){
var e = e38351;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_38352], null));
} else {
var e = e38351;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}finally {figwheel.client.enable_repl_print_BANG_.call(null);
}});})(base_path_38352))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = {};
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__38353){
var map__38362 = p__38353;
var map__38362__$1 = ((((!((map__38362 == null)))?((((map__38362.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38362.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38362):map__38362);
var opts = map__38362__$1;
var build_id = cljs.core.get.call(null,map__38362__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__38362,map__38362__$1,opts,build_id){
return (function (p__38364){
var vec__38365 = p__38364;
var seq__38366 = cljs.core.seq.call(null,vec__38365);
var first__38367 = cljs.core.first.call(null,seq__38366);
var seq__38366__$1 = cljs.core.next.call(null,seq__38366);
var map__38368 = first__38367;
var map__38368__$1 = ((((!((map__38368 == null)))?((((map__38368.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38368.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38368):map__38368);
var msg = map__38368__$1;
var msg_name = cljs.core.get.call(null,map__38368__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__38366__$1;
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),opts,((function (vec__38365,seq__38366,first__38367,seq__38366__$1,map__38368,map__38368__$1,msg,msg_name,_,map__38362,map__38362__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__38365,seq__38366,first__38367,seq__38366__$1,map__38368,map__38368__$1,msg,msg_name,_,map__38362,map__38362__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__38362,map__38362__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__38376){
var vec__38377 = p__38376;
var seq__38378 = cljs.core.seq.call(null,vec__38377);
var first__38379 = cljs.core.first.call(null,seq__38378);
var seq__38378__$1 = cljs.core.next.call(null,seq__38378);
var map__38380 = first__38379;
var map__38380__$1 = ((((!((map__38380 == null)))?((((map__38380.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38380.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38380):map__38380);
var msg = map__38380__$1;
var msg_name = cljs.core.get.call(null,map__38380__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__38378__$1;
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__38382){
var map__38394 = p__38382;
var map__38394__$1 = ((((!((map__38394 == null)))?((((map__38394.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38394.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38394):map__38394);
var on_compile_warning = cljs.core.get.call(null,map__38394__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
var on_compile_fail = cljs.core.get.call(null,map__38394__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
return ((function (map__38394,map__38394__$1,on_compile_warning,on_compile_fail){
return (function (p__38396){
var vec__38397 = p__38396;
var seq__38398 = cljs.core.seq.call(null,vec__38397);
var first__38399 = cljs.core.first.call(null,seq__38398);
var seq__38398__$1 = cljs.core.next.call(null,seq__38398);
var map__38400 = first__38399;
var map__38400__$1 = ((((!((map__38400 == null)))?((((map__38400.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38400.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38400):map__38400);
var msg = map__38400__$1;
var msg_name = cljs.core.get.call(null,map__38400__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__38398__$1;
var pred__38402 = cljs.core._EQ_;
var expr__38403 = msg_name;
if(cljs.core.truth_(pred__38402.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__38403))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__38402.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__38403))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__38394,map__38394__$1,on_compile_warning,on_compile_fail))
});
figwheel.client.auto_jump_to_error = (function figwheel$client$auto_jump_to_error(opts,error){
if(cljs.core.truth_(new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920).cljs$core$IFn$_invoke$arity$1(opts))){
return figwheel.client.heads_up.auto_notify_source_file_line.call(null,error);
} else {
return null;
}
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__,msg_hist,msg_names,msg){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__,msg_hist,msg_names,msg){
return (function (state_38631){
var state_val_38632 = (state_38631[(1)]);
if((state_val_38632 === (7))){
var inst_38551 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38551)){
var statearr_38633_38683 = state_38631__$1;
(statearr_38633_38683[(1)] = (8));

} else {
var statearr_38634_38684 = state_38631__$1;
(statearr_38634_38684[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (20))){
var inst_38625 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38635_38685 = state_38631__$1;
(statearr_38635_38685[(2)] = inst_38625);

(statearr_38635_38685[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (27))){
var inst_38621 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38636_38686 = state_38631__$1;
(statearr_38636_38686[(2)] = inst_38621);

(statearr_38636_38686[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (1))){
var inst_38544 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38544)){
var statearr_38637_38687 = state_38631__$1;
(statearr_38637_38687[(1)] = (2));

} else {
var statearr_38638_38688 = state_38631__$1;
(statearr_38638_38688[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (24))){
var inst_38623 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38639_38689 = state_38631__$1;
(statearr_38639_38689[(2)] = inst_38623);

(statearr_38639_38689[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (4))){
var inst_38629 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38631__$1,inst_38629);
} else {
if((state_val_38632 === (15))){
var inst_38627 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38640_38690 = state_38631__$1;
(statearr_38640_38690[(2)] = inst_38627);

(statearr_38640_38690[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (21))){
var inst_38580 = (state_38631[(2)]);
var inst_38581 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38582 = figwheel.client.auto_jump_to_error.call(null,opts,inst_38581);
var state_38631__$1 = (function (){var statearr_38641 = state_38631;
(statearr_38641[(7)] = inst_38580);

return statearr_38641;
})();
var statearr_38642_38691 = state_38631__$1;
(statearr_38642_38691[(2)] = inst_38582);

(statearr_38642_38691[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (31))){
var inst_38610 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38610)){
var statearr_38643_38692 = state_38631__$1;
(statearr_38643_38692[(1)] = (34));

} else {
var statearr_38644_38693 = state_38631__$1;
(statearr_38644_38693[(1)] = (35));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (32))){
var inst_38619 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38645_38694 = state_38631__$1;
(statearr_38645_38694[(2)] = inst_38619);

(statearr_38645_38694[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (33))){
var inst_38606 = (state_38631[(2)]);
var inst_38607 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38608 = figwheel.client.auto_jump_to_error.call(null,opts,inst_38607);
var state_38631__$1 = (function (){var statearr_38646 = state_38631;
(statearr_38646[(8)] = inst_38606);

return statearr_38646;
})();
var statearr_38647_38695 = state_38631__$1;
(statearr_38647_38695[(2)] = inst_38608);

(statearr_38647_38695[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (13))){
var inst_38565 = figwheel.client.heads_up.clear.call(null);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(16),inst_38565);
} else {
if((state_val_38632 === (22))){
var inst_38586 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38587 = figwheel.client.heads_up.append_warning_message.call(null,inst_38586);
var state_38631__$1 = state_38631;
var statearr_38648_38696 = state_38631__$1;
(statearr_38648_38696[(2)] = inst_38587);

(statearr_38648_38696[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (36))){
var inst_38617 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38649_38697 = state_38631__$1;
(statearr_38649_38697[(2)] = inst_38617);

(statearr_38649_38697[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (29))){
var inst_38597 = (state_38631[(2)]);
var inst_38598 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38599 = figwheel.client.auto_jump_to_error.call(null,opts,inst_38598);
var state_38631__$1 = (function (){var statearr_38650 = state_38631;
(statearr_38650[(9)] = inst_38597);

return statearr_38650;
})();
var statearr_38651_38698 = state_38631__$1;
(statearr_38651_38698[(2)] = inst_38599);

(statearr_38651_38698[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (6))){
var inst_38546 = (state_38631[(10)]);
var state_38631__$1 = state_38631;
var statearr_38652_38699 = state_38631__$1;
(statearr_38652_38699[(2)] = inst_38546);

(statearr_38652_38699[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (28))){
var inst_38593 = (state_38631[(2)]);
var inst_38594 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38595 = figwheel.client.heads_up.display_warning.call(null,inst_38594);
var state_38631__$1 = (function (){var statearr_38653 = state_38631;
(statearr_38653[(11)] = inst_38593);

return statearr_38653;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(29),inst_38595);
} else {
if((state_val_38632 === (25))){
var inst_38591 = figwheel.client.heads_up.clear.call(null);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(28),inst_38591);
} else {
if((state_val_38632 === (34))){
var inst_38612 = figwheel.client.heads_up.flash_loaded.call(null);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(37),inst_38612);
} else {
if((state_val_38632 === (17))){
var inst_38571 = (state_38631[(2)]);
var inst_38572 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38573 = figwheel.client.auto_jump_to_error.call(null,opts,inst_38572);
var state_38631__$1 = (function (){var statearr_38654 = state_38631;
(statearr_38654[(12)] = inst_38571);

return statearr_38654;
})();
var statearr_38655_38700 = state_38631__$1;
(statearr_38655_38700[(2)] = inst_38573);

(statearr_38655_38700[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (3))){
var inst_38563 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38563)){
var statearr_38656_38701 = state_38631__$1;
(statearr_38656_38701[(1)] = (13));

} else {
var statearr_38657_38702 = state_38631__$1;
(statearr_38657_38702[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (12))){
var inst_38559 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38658_38703 = state_38631__$1;
(statearr_38658_38703[(2)] = inst_38559);

(statearr_38658_38703[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (2))){
var inst_38546 = (state_38631[(10)]);
var inst_38546__$1 = figwheel.client.autoload_QMARK_.call(null);
var state_38631__$1 = (function (){var statearr_38659 = state_38631;
(statearr_38659[(10)] = inst_38546__$1);

return statearr_38659;
})();
if(cljs.core.truth_(inst_38546__$1)){
var statearr_38660_38704 = state_38631__$1;
(statearr_38660_38704[(1)] = (5));

} else {
var statearr_38661_38705 = state_38631__$1;
(statearr_38661_38705[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (23))){
var inst_38589 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38589)){
var statearr_38662_38706 = state_38631__$1;
(statearr_38662_38706[(1)] = (25));

} else {
var statearr_38663_38707 = state_38631__$1;
(statearr_38663_38707[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (35))){
var state_38631__$1 = state_38631;
var statearr_38664_38708 = state_38631__$1;
(statearr_38664_38708[(2)] = null);

(statearr_38664_38708[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (19))){
var inst_38584 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38584)){
var statearr_38665_38709 = state_38631__$1;
(statearr_38665_38709[(1)] = (22));

} else {
var statearr_38666_38710 = state_38631__$1;
(statearr_38666_38710[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (11))){
var inst_38555 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38667_38711 = state_38631__$1;
(statearr_38667_38711[(2)] = inst_38555);

(statearr_38667_38711[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (9))){
var inst_38557 = figwheel.client.heads_up.clear.call(null);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(12),inst_38557);
} else {
if((state_val_38632 === (5))){
var inst_38548 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_38631__$1 = state_38631;
var statearr_38668_38712 = state_38631__$1;
(statearr_38668_38712[(2)] = inst_38548);

(statearr_38668_38712[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (14))){
var inst_38575 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38575)){
var statearr_38669_38713 = state_38631__$1;
(statearr_38669_38713[(1)] = (18));

} else {
var statearr_38670_38714 = state_38631__$1;
(statearr_38670_38714[(1)] = (19));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (26))){
var inst_38601 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_38631__$1 = state_38631;
if(cljs.core.truth_(inst_38601)){
var statearr_38671_38715 = state_38631__$1;
(statearr_38671_38715[(1)] = (30));

} else {
var statearr_38672_38716 = state_38631__$1;
(statearr_38672_38716[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (16))){
var inst_38567 = (state_38631[(2)]);
var inst_38568 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38569 = figwheel.client.heads_up.display_exception.call(null,inst_38568);
var state_38631__$1 = (function (){var statearr_38673 = state_38631;
(statearr_38673[(13)] = inst_38567);

return statearr_38673;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(17),inst_38569);
} else {
if((state_val_38632 === (30))){
var inst_38603 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38604 = figwheel.client.heads_up.display_warning.call(null,inst_38603);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(33),inst_38604);
} else {
if((state_val_38632 === (10))){
var inst_38561 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38674_38717 = state_38631__$1;
(statearr_38674_38717[(2)] = inst_38561);

(statearr_38674_38717[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (18))){
var inst_38577 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_38578 = figwheel.client.heads_up.display_exception.call(null,inst_38577);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(21),inst_38578);
} else {
if((state_val_38632 === (37))){
var inst_38614 = (state_38631[(2)]);
var state_38631__$1 = state_38631;
var statearr_38675_38718 = state_38631__$1;
(statearr_38675_38718[(2)] = inst_38614);

(statearr_38675_38718[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38632 === (8))){
var inst_38553 = figwheel.client.heads_up.flash_loaded.call(null);
var state_38631__$1 = state_38631;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38631__$1,(11),inst_38553);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__32227__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__32115__auto__,c__32227__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto____0 = (function (){
var statearr_38679 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_38679[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto__);

(statearr_38679[(1)] = (1));

return statearr_38679;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto____1 = (function (state_38631){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_38631);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e38680){if((e38680 instanceof Object)){
var ex__32119__auto__ = e38680;
var statearr_38681_38719 = state_38631;
(statearr_38681_38719[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38631);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38680;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38720 = state_38631;
state_38631 = G__38720;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto__ = function(state_38631){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto____1.call(this,state_38631);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__,msg_hist,msg_names,msg))
})();
var state__32229__auto__ = (function (){var statearr_38682 = f__32228__auto__.call(null);
(statearr_38682[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_38682;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__,msg_hist,msg_names,msg))
);

return c__32227__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__32227__auto___38783 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto___38783,ch){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto___38783,ch){
return (function (state_38766){
var state_val_38767 = (state_38766[(1)]);
if((state_val_38767 === (1))){
var state_38766__$1 = state_38766;
var statearr_38768_38784 = state_38766__$1;
(statearr_38768_38784[(2)] = null);

(statearr_38768_38784[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38767 === (2))){
var state_38766__$1 = state_38766;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38766__$1,(4),ch);
} else {
if((state_val_38767 === (3))){
var inst_38764 = (state_38766[(2)]);
var state_38766__$1 = state_38766;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38766__$1,inst_38764);
} else {
if((state_val_38767 === (4))){
var inst_38754 = (state_38766[(7)]);
var inst_38754__$1 = (state_38766[(2)]);
var state_38766__$1 = (function (){var statearr_38769 = state_38766;
(statearr_38769[(7)] = inst_38754__$1);

return statearr_38769;
})();
if(cljs.core.truth_(inst_38754__$1)){
var statearr_38770_38785 = state_38766__$1;
(statearr_38770_38785[(1)] = (5));

} else {
var statearr_38771_38786 = state_38766__$1;
(statearr_38771_38786[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38767 === (5))){
var inst_38754 = (state_38766[(7)]);
var inst_38756 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_38754);
var state_38766__$1 = state_38766;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38766__$1,(8),inst_38756);
} else {
if((state_val_38767 === (6))){
var state_38766__$1 = state_38766;
var statearr_38772_38787 = state_38766__$1;
(statearr_38772_38787[(2)] = null);

(statearr_38772_38787[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38767 === (7))){
var inst_38762 = (state_38766[(2)]);
var state_38766__$1 = state_38766;
var statearr_38773_38788 = state_38766__$1;
(statearr_38773_38788[(2)] = inst_38762);

(statearr_38773_38788[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38767 === (8))){
var inst_38758 = (state_38766[(2)]);
var state_38766__$1 = (function (){var statearr_38774 = state_38766;
(statearr_38774[(8)] = inst_38758);

return statearr_38774;
})();
var statearr_38775_38789 = state_38766__$1;
(statearr_38775_38789[(2)] = null);

(statearr_38775_38789[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__32227__auto___38783,ch))
;
return ((function (switch__32115__auto__,c__32227__auto___38783,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__32116__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__32116__auto____0 = (function (){
var statearr_38779 = [null,null,null,null,null,null,null,null,null];
(statearr_38779[(0)] = figwheel$client$heads_up_plugin_$_state_machine__32116__auto__);

(statearr_38779[(1)] = (1));

return statearr_38779;
});
var figwheel$client$heads_up_plugin_$_state_machine__32116__auto____1 = (function (state_38766){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_38766);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e38780){if((e38780 instanceof Object)){
var ex__32119__auto__ = e38780;
var statearr_38781_38790 = state_38766;
(statearr_38781_38790[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38766);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38780;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38791 = state_38766;
state_38766 = G__38791;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__32116__auto__ = function(state_38766){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__32116__auto____1.call(this,state_38766);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__32116__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__32116__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto___38783,ch))
})();
var state__32229__auto__ = (function (){var statearr_38782 = f__32228__auto__.call(null);
(statearr_38782[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto___38783);

return statearr_38782;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto___38783,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__){
return (function (state_38812){
var state_val_38813 = (state_38812[(1)]);
if((state_val_38813 === (1))){
var inst_38807 = cljs.core.async.timeout.call(null,(3000));
var state_38812__$1 = state_38812;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38812__$1,(2),inst_38807);
} else {
if((state_val_38813 === (2))){
var inst_38809 = (state_38812[(2)]);
var inst_38810 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_38812__$1 = (function (){var statearr_38814 = state_38812;
(statearr_38814[(7)] = inst_38809);

return statearr_38814;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38812__$1,inst_38810);
} else {
return null;
}
}
});})(c__32227__auto__))
;
return ((function (switch__32115__auto__,c__32227__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__32116__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__32116__auto____0 = (function (){
var statearr_38818 = [null,null,null,null,null,null,null,null];
(statearr_38818[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__32116__auto__);

(statearr_38818[(1)] = (1));

return statearr_38818;
});
var figwheel$client$enforce_project_plugin_$_state_machine__32116__auto____1 = (function (state_38812){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_38812);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e38819){if((e38819 instanceof Object)){
var ex__32119__auto__ = e38819;
var statearr_38820_38822 = state_38812;
(statearr_38820_38822[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38812);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38819;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38823 = state_38812;
state_38812 = G__38823;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__32116__auto__ = function(state_38812){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__32116__auto____1.call(this,state_38812);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_project_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__32116__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__32116__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__))
})();
var state__32229__auto__ = (function (){var statearr_38821 = f__32228__auto__.call(null);
(statearr_38821[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_38821;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__))
);

return c__32227__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.enforce_figwheel_version_plugin = (function figwheel$client$enforce_figwheel_version_plugin(opts){
return (function (msg_hist){
var temp__4657__auto__ = new cljs.core.Keyword(null,"figwheel-version","figwheel-version",1409553832).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,msg_hist));
if(cljs.core.truth_(temp__4657__auto__)){
var figwheel_version = temp__4657__auto__;
if(cljs.core.not_EQ_.call(null,figwheel_version,figwheel.client._figwheel_version_)){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different version of Figwheel.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__,figwheel_version,temp__4657__auto__){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__,figwheel_version,temp__4657__auto__){
return (function (state_38846){
var state_val_38847 = (state_38846[(1)]);
if((state_val_38847 === (1))){
var inst_38840 = cljs.core.async.timeout.call(null,(2000));
var state_38846__$1 = state_38846;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38846__$1,(2),inst_38840);
} else {
if((state_val_38847 === (2))){
var inst_38842 = (state_38846[(2)]);
var inst_38843 = [cljs.core.str("Figwheel Client Version <strong>"),cljs.core.str(figwheel.client._figwheel_version_),cljs.core.str("</strong> is not equal to "),cljs.core.str("Figwheel Sidecar Version <strong>"),cljs.core.str(figwheel_version),cljs.core.str("</strong>"),cljs.core.str(".  Shutting down Websocket Connection!"),cljs.core.str("<h4>To fix try:</h4>"),cljs.core.str("<ol><li>Reload this page and make sure you are not getting a cached version of the client.</li>"),cljs.core.str("<li>You may have to clean (delete compiled assets) and rebuild to make sure that the new client code is being used.</li>"),cljs.core.str("<li>Also, make sure you have consistent Figwheel dependencies.</li></ol>")].join('');
var inst_38844 = figwheel.client.heads_up.display_system_warning.call(null,"Figwheel Client and Server have different versions!!",inst_38843);
var state_38846__$1 = (function (){var statearr_38848 = state_38846;
(statearr_38848[(7)] = inst_38842);

return statearr_38848;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38846__$1,inst_38844);
} else {
return null;
}
}
});})(c__32227__auto__,figwheel_version,temp__4657__auto__))
;
return ((function (switch__32115__auto__,c__32227__auto__,figwheel_version,temp__4657__auto__){
return (function() {
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto__ = null;
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto____0 = (function (){
var statearr_38852 = [null,null,null,null,null,null,null,null];
(statearr_38852[(0)] = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto__);

(statearr_38852[(1)] = (1));

return statearr_38852;
});
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto____1 = (function (state_38846){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_38846);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e38853){if((e38853 instanceof Object)){
var ex__32119__auto__ = e38853;
var statearr_38854_38856 = state_38846;
(statearr_38854_38856[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38846);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38853;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38857 = state_38846;
state_38846 = G__38857;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto__ = function(state_38846){
switch(arguments.length){
case 0:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto____1.call(this,state_38846);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto____0;
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto____1;
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__,figwheel_version,temp__4657__auto__))
})();
var state__32229__auto__ = (function (){var statearr_38855 = f__32228__auto__.call(null);
(statearr_38855[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_38855;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__,figwheel_version,temp__4657__auto__))
);

return c__32227__auto__;
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.file_line_column = (function figwheel$client$file_line_column(p__38858){
var map__38862 = p__38858;
var map__38862__$1 = ((((!((map__38862 == null)))?((((map__38862.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38862.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38862):map__38862);
var file = cljs.core.get.call(null,map__38862__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38862__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38862__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var G__38864 = "";
var G__38864__$1 = (cljs.core.truth_(file)?[cljs.core.str(G__38864),cljs.core.str("file "),cljs.core.str(file)].join(''):G__38864);
var G__38864__$2 = (cljs.core.truth_(line)?[cljs.core.str(G__38864__$1),cljs.core.str(" at line "),cljs.core.str(line)].join(''):G__38864__$1);
if(cljs.core.truth_((function (){var and__29909__auto__ = line;
if(cljs.core.truth_(and__29909__auto__)){
return column;
} else {
return and__29909__auto__;
}
})())){
return [cljs.core.str(G__38864__$2),cljs.core.str(", column "),cljs.core.str(column)].join('');
} else {
return G__38864__$2;
}
});
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__38865){
var map__38872 = p__38865;
var map__38872__$1 = ((((!((map__38872 == null)))?((((map__38872.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38872.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38872):map__38872);
var ed = map__38872__$1;
var formatted_exception = cljs.core.get.call(null,map__38872__$1,new cljs.core.Keyword(null,"formatted-exception","formatted-exception",-116489026));
var exception_data = cljs.core.get.call(null,map__38872__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var cause = cljs.core.get.call(null,map__38872__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: Compile Exception");

var seq__38874_38878 = cljs.core.seq.call(null,figwheel.client.format_messages.call(null,exception_data));
var chunk__38875_38879 = null;
var count__38876_38880 = (0);
var i__38877_38881 = (0);
while(true){
if((i__38877_38881 < count__38876_38880)){
var msg_38882 = cljs.core._nth.call(null,chunk__38875_38879,i__38877_38881);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_38882);

var G__38883 = seq__38874_38878;
var G__38884 = chunk__38875_38879;
var G__38885 = count__38876_38880;
var G__38886 = (i__38877_38881 + (1));
seq__38874_38878 = G__38883;
chunk__38875_38879 = G__38884;
count__38876_38880 = G__38885;
i__38877_38881 = G__38886;
continue;
} else {
var temp__4657__auto___38887 = cljs.core.seq.call(null,seq__38874_38878);
if(temp__4657__auto___38887){
var seq__38874_38888__$1 = temp__4657__auto___38887;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__38874_38888__$1)){
var c__30732__auto___38889 = cljs.core.chunk_first.call(null,seq__38874_38888__$1);
var G__38890 = cljs.core.chunk_rest.call(null,seq__38874_38888__$1);
var G__38891 = c__30732__auto___38889;
var G__38892 = cljs.core.count.call(null,c__30732__auto___38889);
var G__38893 = (0);
seq__38874_38878 = G__38890;
chunk__38875_38879 = G__38891;
count__38876_38880 = G__38892;
i__38877_38881 = G__38893;
continue;
} else {
var msg_38894 = cljs.core.first.call(null,seq__38874_38888__$1);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_38894);

var G__38895 = cljs.core.next.call(null,seq__38874_38888__$1);
var G__38896 = null;
var G__38897 = (0);
var G__38898 = (0);
seq__38874_38878 = G__38895;
chunk__38875_38879 = G__38896;
count__38876_38880 = G__38897;
i__38877_38881 = G__38898;
continue;
}
} else {
}
}
break;
}

if(cljs.core.truth_(cause)){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Error on "),cljs.core.str(figwheel.client.file_line_column.call(null,ed))].join(''));
} else {
}

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__38899){
var map__38902 = p__38899;
var map__38902__$1 = ((((!((map__38902 == null)))?((((map__38902.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38902.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38902):map__38902);
var w = map__38902__$1;
var message = cljs.core.get.call(null,map__38902__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),[cljs.core.str("Figwheel: Compile Warning - "),cljs.core.str(new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(message)),cljs.core.str(" in "),cljs.core.str(figwheel.client.file_line_column.call(null,message))].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if(typeof figwheel.client.config_defaults !== 'undefined'){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"eval-fn","eval-fn",-1111644294),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[new cljs.core.Var(function(){return figwheel.client.default_on_compile_warning;},new cljs.core.Symbol("figwheel.client","default-on-compile-warning","figwheel.client/default-on-compile-warning",584144208,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-warning","default-on-compile-warning",-18911586,null),"resources/public/js/figwheel/client.cljs",33,1,357,357,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"message","message",1234475525,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"w","w",1994700528,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_warning)?figwheel.client.default_on_compile_warning.cljs$lang$test:null)])),figwheel.client.default_on_jsload,true,new cljs.core.Var(function(){return figwheel.client.default_on_compile_fail;},new cljs.core.Symbol("figwheel.client","default-on-compile-fail","figwheel.client/default-on-compile-fail",1384826337,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-fail","default-on-compile-fail",-158814813,null),"resources/public/js/figwheel/client.cljs",30,1,349,349,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"formatted-exception","formatted-exception",1524042501,null),new cljs.core.Symbol(null,"exception-data","exception-data",1128056641,null),new cljs.core.Symbol(null,"cause","cause",1872432779,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"ed","ed",2076825751,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_fail)?figwheel.client.default_on_compile_fail.cljs$lang$test:null)])),false,true,[cljs.core.str("ws://"),cljs.core.str((cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),cljs.core.str("/figwheel-ws")].join(''),false,figwheel.client.default_before_load,false,false,(100),true,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.fill_url_template = (function figwheel$client$fill_url_template(config){
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
return cljs.core.update_in.call(null,config,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938)], null),(function (x){
return clojure.string.replace.call(null,clojure.string.replace.call(null,x,"[[client-hostname]]",location.hostname),"[[client-port]]",location.port);
}));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"enforce-figwheel-version-plugin","enforce-figwheel-version-plugin",-1916185220),figwheel.client.enforce_figwheel_version_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = ((cljs.core.not.call(null,figwheel.client.utils.html_env_QMARK_.call(null)))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__29909__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__29909__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__29909__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_message_watch = (function figwheel$client$add_message_watch(key,callback){
return cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,key,(function (_,___$1,___$2,msg_hist){
return callback.call(null,cljs.core.first.call(null,msg_hist));
}));
});
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__38914 = cljs.core.seq.call(null,plugins);
var chunk__38915 = null;
var count__38916 = (0);
var i__38917 = (0);
while(true){
if((i__38917 < count__38916)){
var vec__38918 = cljs.core._nth.call(null,chunk__38915,i__38917);
var k = cljs.core.nth.call(null,vec__38918,(0),null);
var plugin = cljs.core.nth.call(null,vec__38918,(1),null);
if(cljs.core.truth_(plugin)){
var pl_38924 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__38914,chunk__38915,count__38916,i__38917,pl_38924,vec__38918,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_38924.call(null,msg_hist);
});})(seq__38914,chunk__38915,count__38916,i__38917,pl_38924,vec__38918,k,plugin))
);
} else {
}

var G__38925 = seq__38914;
var G__38926 = chunk__38915;
var G__38927 = count__38916;
var G__38928 = (i__38917 + (1));
seq__38914 = G__38925;
chunk__38915 = G__38926;
count__38916 = G__38927;
i__38917 = G__38928;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__38914);
if(temp__4657__auto__){
var seq__38914__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__38914__$1)){
var c__30732__auto__ = cljs.core.chunk_first.call(null,seq__38914__$1);
var G__38929 = cljs.core.chunk_rest.call(null,seq__38914__$1);
var G__38930 = c__30732__auto__;
var G__38931 = cljs.core.count.call(null,c__30732__auto__);
var G__38932 = (0);
seq__38914 = G__38929;
chunk__38915 = G__38930;
count__38916 = G__38931;
i__38917 = G__38932;
continue;
} else {
var vec__38921 = cljs.core.first.call(null,seq__38914__$1);
var k = cljs.core.nth.call(null,vec__38921,(0),null);
var plugin = cljs.core.nth.call(null,vec__38921,(1),null);
if(cljs.core.truth_(plugin)){
var pl_38933 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__38914,chunk__38915,count__38916,i__38917,pl_38933,vec__38921,k,plugin,seq__38914__$1,temp__4657__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_38933.call(null,msg_hist);
});})(seq__38914,chunk__38915,count__38916,i__38917,pl_38933,vec__38921,k,plugin,seq__38914__$1,temp__4657__auto__))
);
} else {
}

var G__38934 = cljs.core.next.call(null,seq__38914__$1);
var G__38935 = null;
var G__38936 = (0);
var G__38937 = (0);
seq__38914 = G__38934;
chunk__38915 = G__38935;
count__38916 = G__38936;
i__38917 = G__38937;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(var_args){
var args38938 = [];
var len__30996__auto___38945 = arguments.length;
var i__30997__auto___38946 = (0);
while(true){
if((i__30997__auto___38946 < len__30996__auto___38945)){
args38938.push((arguments[i__30997__auto___38946]));

var G__38947 = (i__30997__auto___38946 + (1));
i__30997__auto___38946 = G__38947;
continue;
} else {
}
break;
}

var G__38940 = args38938.length;
switch (G__38940) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args38938.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if(typeof figwheel.client.__figwheel_start_once__ !== 'undefined'){
return null;
} else {
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.fill_url_template.call(null,figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370)))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.enable_repl_print_BANG_.call(null);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

var seq__38941_38949 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"initial-messages","initial-messages",2057377771).cljs$core$IFn$_invoke$arity$1(system_options));
var chunk__38942_38950 = null;
var count__38943_38951 = (0);
var i__38944_38952 = (0);
while(true){
if((i__38944_38952 < count__38943_38951)){
var msg_38953 = cljs.core._nth.call(null,chunk__38942_38950,i__38944_38952);
figwheel.client.socket.handle_incoming_message.call(null,msg_38953);

var G__38954 = seq__38941_38949;
var G__38955 = chunk__38942_38950;
var G__38956 = count__38943_38951;
var G__38957 = (i__38944_38952 + (1));
seq__38941_38949 = G__38954;
chunk__38942_38950 = G__38955;
count__38943_38951 = G__38956;
i__38944_38952 = G__38957;
continue;
} else {
var temp__4657__auto___38958 = cljs.core.seq.call(null,seq__38941_38949);
if(temp__4657__auto___38958){
var seq__38941_38959__$1 = temp__4657__auto___38958;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__38941_38959__$1)){
var c__30732__auto___38960 = cljs.core.chunk_first.call(null,seq__38941_38959__$1);
var G__38961 = cljs.core.chunk_rest.call(null,seq__38941_38959__$1);
var G__38962 = c__30732__auto___38960;
var G__38963 = cljs.core.count.call(null,c__30732__auto___38960);
var G__38964 = (0);
seq__38941_38949 = G__38961;
chunk__38942_38950 = G__38962;
count__38943_38951 = G__38963;
i__38944_38952 = G__38964;
continue;
} else {
var msg_38965 = cljs.core.first.call(null,seq__38941_38959__$1);
figwheel.client.socket.handle_incoming_message.call(null,msg_38965);

var G__38966 = cljs.core.next.call(null,seq__38941_38959__$1);
var G__38967 = null;
var G__38968 = (0);
var G__38969 = (0);
seq__38941_38949 = G__38966;
chunk__38942_38950 = G__38967;
count__38943_38951 = G__38968;
i__38944_38952 = G__38969;
continue;
}
} else {
}
}
break;
}

return figwheel.client.socket.open.call(null,system_options);
}));
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;

figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(var_args){
var args__31003__auto__ = [];
var len__30996__auto___38974 = arguments.length;
var i__30997__auto___38975 = (0);
while(true){
if((i__30997__auto___38975 < len__30996__auto___38974)){
args__31003__auto__.push((arguments[i__30997__auto___38975]));

var G__38976 = (i__30997__auto___38975 + (1));
i__30997__auto___38975 = G__38976;
continue;
} else {
}
break;
}

var argseq__31004__auto__ = ((((0) < args__31003__auto__.length))?(new cljs.core.IndexedSeq(args__31003__auto__.slice((0)),(0),null)):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__31004__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__38971){
var map__38972 = p__38971;
var map__38972__$1 = ((((!((map__38972 == null)))?((((map__38972.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38972.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38972):map__38972);
var opts = map__38972__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq38970){
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq38970));
});

figwheel.client.fetch_data_from_env = (function figwheel$client$fetch_data_from_env(){
try{return cljs.reader.read_string.call(null,goog.object.get(window,"FIGWHEEL_CLIENT_CONFIGURATION"));
}catch (e38978){if((e38978 instanceof Error)){
var e = e38978;
cljs.core._STAR_print_err_fn_STAR_.call(null,"Unable to load FIGWHEEL_CLIENT_CONFIGURATION from the environment");

return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"autoload","autoload",-354122500),false], null);
} else {
throw e38978;

}
}});
figwheel.client.console_intro_message = "Figwheel has compiled a temporary helper application to your :output-file.\n\nThe code currently in your configured output file does not\nrepresent the code that you are trying to compile.\n\nThis temporary application is intended to help you continue to get\nfeedback from Figwheel until the build you are working on compiles\ncorrectly.\n\nWhen your ClojureScript source code compiles correctly this helper\napplication will auto-reload and pick up your freshly compiled\nClojureScript program.";
figwheel.client.bad_compile_helper_app = (function figwheel$client$bad_compile_helper_app(){
cljs.core.enable_console_print_BANG_.call(null);

var config = figwheel.client.fetch_data_from_env.call(null);
cljs.core.println.call(null,figwheel.client.console_intro_message);

figwheel.client.heads_up.bad_compile_screen.call(null);

if(cljs.core.truth_(goog.dependencies_)){
} else {
goog.dependencies_ = true;
}

figwheel.client.start.call(null,config);

return figwheel.client.add_message_watch.call(null,new cljs.core.Keyword(null,"listen-for-successful-compile","listen-for-successful-compile",-995277603),((function (config){
return (function (p__38982){
var map__38983 = p__38982;
var map__38983__$1 = ((((!((map__38983 == null)))?((((map__38983.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38983.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38983):map__38983);
var msg_name = cljs.core.get.call(null,map__38983__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))){
return location.href = location.href;
} else {
return null;
}
});})(config))
);
});

//# sourceMappingURL=client.js.map?rel=1494229528845