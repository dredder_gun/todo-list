// Compiled by ClojureScript 1.9.89 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('goog.string');
goog.require('goog.async.Deferred');
goog.require('goog.Uri');
goog.require('goog.net.jsloader');
goog.require('cljs.core.async');
goog.require('goog.object');
goog.require('clojure.set');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
if(typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined'){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.on_cssload_custom_event = (function figwheel$client$file_reloading$on_cssload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.css-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__29921__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string')) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__29921__auto__){
return or__29921__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return (goog.dependencies_.nameToPath[ns]);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return (goog.dependencies_.written[figwheel.client.file_reloading.name__GT_path.call(null,ns)]);
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__29921__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 10, ["svgpan.SvgPan",null,"far.out",null,"cljs.nodejs",null,"testDep.bar",null,"someprotopackage.TestPackageTypes",null,"goog",null,"an.existing.path",null,"cljs.core",null,"ns",null,"dup.base",null], null), null).call(null,name);
if(cljs.core.truth_(or__29921__auto__)){
return or__29921__auto__;
} else {
return cljs.core.some.call(null,cljs.core.partial.call(null,goog.string.startsWith,name),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, ["goog.","cljs.","clojure.","fake.","proto2."], null));
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__36689_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__36689_SHARP_));
}),goog.object.getKeys((goog.dependencies_.requires[figwheel.client.file_reloading.name__GT_path.call(null,ns)]))));
});
if(typeof figwheel.client.file_reloading.dependency_data !== 'undefined'){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([name], true));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([parent_ns], true));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__36694 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__36695 = null;
var count__36696 = (0);
var i__36697 = (0);
while(true){
if((i__36697 < count__36696)){
var n = cljs.core._nth.call(null,chunk__36695,i__36697);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__36698 = seq__36694;
var G__36699 = chunk__36695;
var G__36700 = count__36696;
var G__36701 = (i__36697 + (1));
seq__36694 = G__36698;
chunk__36695 = G__36699;
count__36696 = G__36700;
i__36697 = G__36701;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__36694);
if(temp__4657__auto__){
var seq__36694__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36694__$1)){
var c__30732__auto__ = cljs.core.chunk_first.call(null,seq__36694__$1);
var G__36702 = cljs.core.chunk_rest.call(null,seq__36694__$1);
var G__36703 = c__30732__auto__;
var G__36704 = cljs.core.count.call(null,c__30732__auto__);
var G__36705 = (0);
seq__36694 = G__36702;
chunk__36695 = G__36703;
count__36696 = G__36704;
i__36697 = G__36705;
continue;
} else {
var n = cljs.core.first.call(null,seq__36694__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__36706 = cljs.core.next.call(null,seq__36694__$1);
var G__36707 = null;
var G__36708 = (0);
var G__36709 = (0);
seq__36694 = G__36706;
chunk__36695 = G__36707;
count__36696 = G__36708;
i__36697 = G__36709;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__36760_36771 = cljs.core.seq.call(null,deps);
var chunk__36761_36772 = null;
var count__36762_36773 = (0);
var i__36763_36774 = (0);
while(true){
if((i__36763_36774 < count__36762_36773)){
var dep_36775 = cljs.core._nth.call(null,chunk__36761_36772,i__36763_36774);
topo_sort_helper_STAR_.call(null,dep_36775,(depth + (1)),state);

var G__36776 = seq__36760_36771;
var G__36777 = chunk__36761_36772;
var G__36778 = count__36762_36773;
var G__36779 = (i__36763_36774 + (1));
seq__36760_36771 = G__36776;
chunk__36761_36772 = G__36777;
count__36762_36773 = G__36778;
i__36763_36774 = G__36779;
continue;
} else {
var temp__4657__auto___36780 = cljs.core.seq.call(null,seq__36760_36771);
if(temp__4657__auto___36780){
var seq__36760_36781__$1 = temp__4657__auto___36780;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36760_36781__$1)){
var c__30732__auto___36782 = cljs.core.chunk_first.call(null,seq__36760_36781__$1);
var G__36783 = cljs.core.chunk_rest.call(null,seq__36760_36781__$1);
var G__36784 = c__30732__auto___36782;
var G__36785 = cljs.core.count.call(null,c__30732__auto___36782);
var G__36786 = (0);
seq__36760_36771 = G__36783;
chunk__36761_36772 = G__36784;
count__36762_36773 = G__36785;
i__36763_36774 = G__36786;
continue;
} else {
var dep_36787 = cljs.core.first.call(null,seq__36760_36781__$1);
topo_sort_helper_STAR_.call(null,dep_36787,(depth + (1)),state);

var G__36788 = cljs.core.next.call(null,seq__36760_36781__$1);
var G__36789 = null;
var G__36790 = (0);
var G__36791 = (0);
seq__36760_36771 = G__36788;
chunk__36761_36772 = G__36789;
count__36762_36773 = G__36790;
i__36763_36774 = G__36791;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__36764){
var vec__36768 = p__36764;
var seq__36769 = cljs.core.seq.call(null,vec__36768);
var first__36770 = cljs.core.first.call(null,seq__36769);
var seq__36769__$1 = cljs.core.next.call(null,seq__36769);
var x = first__36770;
var xs = seq__36769__$1;
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__36768,seq__36769,first__36770,seq__36769__$1,x,xs,get_deps__$1){
return (function (p1__36710_SHARP_){
return clojure.set.difference.call(null,p1__36710_SHARP_,x);
});})(vec__36768,seq__36769,first__36770,seq__36769__$1,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,figwheel.client.file_reloading.immutable_ns_QMARK_),cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss)))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str(goog.basePath),cljs.core.str(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str(goog.basePath),cljs.core.str(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__36804 = cljs.core.seq.call(null,provides);
var chunk__36805 = null;
var count__36806 = (0);
var i__36807 = (0);
while(true){
if((i__36807 < count__36806)){
var prov = cljs.core._nth.call(null,chunk__36805,i__36807);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__36808_36816 = cljs.core.seq.call(null,requires);
var chunk__36809_36817 = null;
var count__36810_36818 = (0);
var i__36811_36819 = (0);
while(true){
if((i__36811_36819 < count__36810_36818)){
var req_36820 = cljs.core._nth.call(null,chunk__36809_36817,i__36811_36819);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_36820,prov);

var G__36821 = seq__36808_36816;
var G__36822 = chunk__36809_36817;
var G__36823 = count__36810_36818;
var G__36824 = (i__36811_36819 + (1));
seq__36808_36816 = G__36821;
chunk__36809_36817 = G__36822;
count__36810_36818 = G__36823;
i__36811_36819 = G__36824;
continue;
} else {
var temp__4657__auto___36825 = cljs.core.seq.call(null,seq__36808_36816);
if(temp__4657__auto___36825){
var seq__36808_36826__$1 = temp__4657__auto___36825;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36808_36826__$1)){
var c__30732__auto___36827 = cljs.core.chunk_first.call(null,seq__36808_36826__$1);
var G__36828 = cljs.core.chunk_rest.call(null,seq__36808_36826__$1);
var G__36829 = c__30732__auto___36827;
var G__36830 = cljs.core.count.call(null,c__30732__auto___36827);
var G__36831 = (0);
seq__36808_36816 = G__36828;
chunk__36809_36817 = G__36829;
count__36810_36818 = G__36830;
i__36811_36819 = G__36831;
continue;
} else {
var req_36832 = cljs.core.first.call(null,seq__36808_36826__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_36832,prov);

var G__36833 = cljs.core.next.call(null,seq__36808_36826__$1);
var G__36834 = null;
var G__36835 = (0);
var G__36836 = (0);
seq__36808_36816 = G__36833;
chunk__36809_36817 = G__36834;
count__36810_36818 = G__36835;
i__36811_36819 = G__36836;
continue;
}
} else {
}
}
break;
}

var G__36837 = seq__36804;
var G__36838 = chunk__36805;
var G__36839 = count__36806;
var G__36840 = (i__36807 + (1));
seq__36804 = G__36837;
chunk__36805 = G__36838;
count__36806 = G__36839;
i__36807 = G__36840;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__36804);
if(temp__4657__auto__){
var seq__36804__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36804__$1)){
var c__30732__auto__ = cljs.core.chunk_first.call(null,seq__36804__$1);
var G__36841 = cljs.core.chunk_rest.call(null,seq__36804__$1);
var G__36842 = c__30732__auto__;
var G__36843 = cljs.core.count.call(null,c__30732__auto__);
var G__36844 = (0);
seq__36804 = G__36841;
chunk__36805 = G__36842;
count__36806 = G__36843;
i__36807 = G__36844;
continue;
} else {
var prov = cljs.core.first.call(null,seq__36804__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__36812_36845 = cljs.core.seq.call(null,requires);
var chunk__36813_36846 = null;
var count__36814_36847 = (0);
var i__36815_36848 = (0);
while(true){
if((i__36815_36848 < count__36814_36847)){
var req_36849 = cljs.core._nth.call(null,chunk__36813_36846,i__36815_36848);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_36849,prov);

var G__36850 = seq__36812_36845;
var G__36851 = chunk__36813_36846;
var G__36852 = count__36814_36847;
var G__36853 = (i__36815_36848 + (1));
seq__36812_36845 = G__36850;
chunk__36813_36846 = G__36851;
count__36814_36847 = G__36852;
i__36815_36848 = G__36853;
continue;
} else {
var temp__4657__auto___36854__$1 = cljs.core.seq.call(null,seq__36812_36845);
if(temp__4657__auto___36854__$1){
var seq__36812_36855__$1 = temp__4657__auto___36854__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36812_36855__$1)){
var c__30732__auto___36856 = cljs.core.chunk_first.call(null,seq__36812_36855__$1);
var G__36857 = cljs.core.chunk_rest.call(null,seq__36812_36855__$1);
var G__36858 = c__30732__auto___36856;
var G__36859 = cljs.core.count.call(null,c__30732__auto___36856);
var G__36860 = (0);
seq__36812_36845 = G__36857;
chunk__36813_36846 = G__36858;
count__36814_36847 = G__36859;
i__36815_36848 = G__36860;
continue;
} else {
var req_36861 = cljs.core.first.call(null,seq__36812_36855__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_36861,prov);

var G__36862 = cljs.core.next.call(null,seq__36812_36855__$1);
var G__36863 = null;
var G__36864 = (0);
var G__36865 = (0);
seq__36812_36845 = G__36862;
chunk__36813_36846 = G__36863;
count__36814_36847 = G__36864;
i__36815_36848 = G__36865;
continue;
}
} else {
}
}
break;
}

var G__36866 = cljs.core.next.call(null,seq__36804__$1);
var G__36867 = null;
var G__36868 = (0);
var G__36869 = (0);
seq__36804 = G__36866;
chunk__36805 = G__36867;
count__36806 = G__36868;
i__36807 = G__36869;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel$client$file_reloading$figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__36874_36878 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__36875_36879 = null;
var count__36876_36880 = (0);
var i__36877_36881 = (0);
while(true){
if((i__36877_36881 < count__36876_36880)){
var ns_36882 = cljs.core._nth.call(null,chunk__36875_36879,i__36877_36881);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_36882);

var G__36883 = seq__36874_36878;
var G__36884 = chunk__36875_36879;
var G__36885 = count__36876_36880;
var G__36886 = (i__36877_36881 + (1));
seq__36874_36878 = G__36883;
chunk__36875_36879 = G__36884;
count__36876_36880 = G__36885;
i__36877_36881 = G__36886;
continue;
} else {
var temp__4657__auto___36887 = cljs.core.seq.call(null,seq__36874_36878);
if(temp__4657__auto___36887){
var seq__36874_36888__$1 = temp__4657__auto___36887;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36874_36888__$1)){
var c__30732__auto___36889 = cljs.core.chunk_first.call(null,seq__36874_36888__$1);
var G__36890 = cljs.core.chunk_rest.call(null,seq__36874_36888__$1);
var G__36891 = c__30732__auto___36889;
var G__36892 = cljs.core.count.call(null,c__30732__auto___36889);
var G__36893 = (0);
seq__36874_36878 = G__36890;
chunk__36875_36879 = G__36891;
count__36876_36880 = G__36892;
i__36877_36881 = G__36893;
continue;
} else {
var ns_36894 = cljs.core.first.call(null,seq__36874_36888__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_36894);

var G__36895 = cljs.core.next.call(null,seq__36874_36888__$1);
var G__36896 = null;
var G__36897 = (0);
var G__36898 = (0);
seq__36874_36878 = G__36895;
chunk__36875_36879 = G__36896;
count__36876_36880 = G__36897;
i__36877_36881 = G__36898;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__29921__auto__ = goog.require__;
if(cljs.core.truth_(or__29921__auto__)){
return or__29921__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__36899__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__36899 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__36900__i = 0, G__36900__a = new Array(arguments.length -  0);
while (G__36900__i < G__36900__a.length) {G__36900__a[G__36900__i] = arguments[G__36900__i + 0]; ++G__36900__i;}
  args = new cljs.core.IndexedSeq(G__36900__a,0);
} 
return G__36899__delegate.call(this,args);};
G__36899.cljs$lang$maxFixedArity = 0;
G__36899.cljs$lang$applyTo = (function (arglist__36901){
var args = cljs.core.seq(arglist__36901);
return G__36899__delegate(args);
});
G__36899.cljs$core$IFn$_invoke$arity$variadic = G__36899__delegate;
return G__36899;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if(typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined'){
return null;
} else {
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
;
}
});
figwheel.client.file_reloading.reload_file_in_html_env = (function figwheel$client$file_reloading$reload_file_in_html_env(request_url,callback){

var deferred = goog.net.jsloader.load(figwheel.client.file_reloading.add_cache_buster.call(null,request_url),{"cleanupWhenDone": true});
deferred.addCallback(((function (deferred){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(deferred))
);

return deferred.addErrback(((function (deferred){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(deferred))
);
});
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__36902 = cljs.core._EQ_;
var expr__36903 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__36902.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__36903))){
var node_path_lib = require("path");
var util_pattern = [cljs.core.str(node_path_lib.sep),cljs.core.str(node_path_lib.join("goog","bootstrap","nodejs.js"))].join('');
var util_path = goog.object.findKey(require.cache,((function (node_path_lib,util_pattern,pred__36902,expr__36903){
return (function (v,k,o){
return goog.string.endsWith(k,util_pattern);
});})(node_path_lib,util_pattern,pred__36902,expr__36903))
);
var parts = cljs.core.pop.call(null,cljs.core.pop.call(null,clojure.string.split.call(null,util_path,/[\/\\]/)));
var root_path = clojure.string.join.call(null,node_path_lib.sep,parts);
return ((function (node_path_lib,util_pattern,util_path,parts,root_path,pred__36902,expr__36903){
return (function (request_url,callback){

var cache_path = node_path_lib.resolve(root_path,request_url);
goog.object.remove(require.cache,cache_path);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e36905){if((e36905 instanceof Error)){
var e = e36905;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e36905;

}
}})());
});
;})(node_path_lib,util_pattern,util_path,parts,root_path,pred__36902,expr__36903))
} else {
if(cljs.core.truth_(pred__36902.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__36903))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__36902.call(null,new cljs.core.Keyword(null,"react-native","react-native",-1543085138),expr__36903))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__36902.call(null,new cljs.core.Keyword(null,"worker","worker",938239996),expr__36903))){
return ((function (pred__36902,expr__36903){
return (function (request_url,callback){

return callback.call(null,(function (){try{self.importScripts(figwheel.client.file_reloading.add_cache_buster.call(null,request_url));

return true;
}catch (e36906){if((e36906 instanceof Error)){
var e = e36906;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(request_url)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e36906;

}
}})());
});
;})(pred__36902,expr__36903))
} else {
return ((function (pred__36902,expr__36903){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__36902,expr__36903))
}
}
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__36907,callback){
var map__36910 = p__36907;
var map__36910__$1 = ((((!((map__36910 == null)))?((((map__36910.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36910.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36910):map__36910);
var file_msg = map__36910__$1;
var request_url = cljs.core.get.call(null,map__36910__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Attempting to load "),cljs.core.str(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__36910,map__36910__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Successfully loaded "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__36910,map__36910__$1,file_msg,request_url))
);
});
if(typeof figwheel.client.file_reloading.reload_chan !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if(typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined'){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if(typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined'){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if(typeof figwheel.client.file_reloading.reloader_loop !== 'undefined'){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__){
return (function (state_36934){
var state_val_36935 = (state_36934[(1)]);
if((state_val_36935 === (7))){
var inst_36930 = (state_36934[(2)]);
var state_36934__$1 = state_36934;
var statearr_36936_36956 = state_36934__$1;
(statearr_36936_36956[(2)] = inst_36930);

(statearr_36936_36956[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36935 === (1))){
var state_36934__$1 = state_36934;
var statearr_36937_36957 = state_36934__$1;
(statearr_36937_36957[(2)] = null);

(statearr_36937_36957[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36935 === (4))){
var inst_36914 = (state_36934[(7)]);
var inst_36914__$1 = (state_36934[(2)]);
var state_36934__$1 = (function (){var statearr_36938 = state_36934;
(statearr_36938[(7)] = inst_36914__$1);

return statearr_36938;
})();
if(cljs.core.truth_(inst_36914__$1)){
var statearr_36939_36958 = state_36934__$1;
(statearr_36939_36958[(1)] = (5));

} else {
var statearr_36940_36959 = state_36934__$1;
(statearr_36940_36959[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36935 === (6))){
var state_36934__$1 = state_36934;
var statearr_36941_36960 = state_36934__$1;
(statearr_36941_36960[(2)] = null);

(statearr_36941_36960[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36935 === (3))){
var inst_36932 = (state_36934[(2)]);
var state_36934__$1 = state_36934;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36934__$1,inst_36932);
} else {
if((state_val_36935 === (2))){
var state_36934__$1 = state_36934;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36934__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_36935 === (11))){
var inst_36926 = (state_36934[(2)]);
var state_36934__$1 = (function (){var statearr_36942 = state_36934;
(statearr_36942[(8)] = inst_36926);

return statearr_36942;
})();
var statearr_36943_36961 = state_36934__$1;
(statearr_36943_36961[(2)] = null);

(statearr_36943_36961[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36935 === (9))){
var inst_36920 = (state_36934[(9)]);
var inst_36918 = (state_36934[(10)]);
var inst_36922 = inst_36920.call(null,inst_36918);
var state_36934__$1 = state_36934;
var statearr_36944_36962 = state_36934__$1;
(statearr_36944_36962[(2)] = inst_36922);

(statearr_36944_36962[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36935 === (5))){
var inst_36914 = (state_36934[(7)]);
var inst_36916 = figwheel.client.file_reloading.blocking_load.call(null,inst_36914);
var state_36934__$1 = state_36934;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36934__$1,(8),inst_36916);
} else {
if((state_val_36935 === (10))){
var inst_36918 = (state_36934[(10)]);
var inst_36924 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_36918);
var state_36934__$1 = state_36934;
var statearr_36945_36963 = state_36934__$1;
(statearr_36945_36963[(2)] = inst_36924);

(statearr_36945_36963[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36935 === (8))){
var inst_36914 = (state_36934[(7)]);
var inst_36920 = (state_36934[(9)]);
var inst_36918 = (state_36934[(2)]);
var inst_36919 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_36920__$1 = cljs.core.get.call(null,inst_36919,inst_36914);
var state_36934__$1 = (function (){var statearr_36946 = state_36934;
(statearr_36946[(9)] = inst_36920__$1);

(statearr_36946[(10)] = inst_36918);

return statearr_36946;
})();
if(cljs.core.truth_(inst_36920__$1)){
var statearr_36947_36964 = state_36934__$1;
(statearr_36947_36964[(1)] = (9));

} else {
var statearr_36948_36965 = state_36934__$1;
(statearr_36948_36965[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__32227__auto__))
;
return ((function (switch__32115__auto__,c__32227__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__32116__auto__ = null;
var figwheel$client$file_reloading$state_machine__32116__auto____0 = (function (){
var statearr_36952 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_36952[(0)] = figwheel$client$file_reloading$state_machine__32116__auto__);

(statearr_36952[(1)] = (1));

return statearr_36952;
});
var figwheel$client$file_reloading$state_machine__32116__auto____1 = (function (state_36934){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_36934);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e36953){if((e36953 instanceof Object)){
var ex__32119__auto__ = e36953;
var statearr_36954_36966 = state_36934;
(statearr_36954_36966[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36934);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36953;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36967 = state_36934;
state_36934 = G__36967;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__32116__auto__ = function(state_36934){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__32116__auto____1.call(this,state_36934);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__32116__auto____0;
figwheel$client$file_reloading$state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__32116__auto____1;
return figwheel$client$file_reloading$state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__))
})();
var state__32229__auto__ = (function (){var statearr_36955 = f__32228__auto__.call(null);
(statearr_36955[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_36955;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__))
);

return c__32227__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(url){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,url);
});
figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__36968,callback){
var map__36971 = p__36968;
var map__36971__$1 = ((((!((map__36971 == null)))?((((map__36971.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36971.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36971):map__36971);
var file_msg = map__36971__$1;
var namespace = cljs.core.get.call(null,map__36971__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__36971,map__36971__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__36971,map__36971__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.figwheel_no_load_QMARK_ = (function figwheel$client$file_reloading$figwheel_no_load_QMARK_(p__36973){
var map__36976 = p__36973;
var map__36976__$1 = ((((!((map__36976 == null)))?((((map__36976.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36976.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36976):map__36976);
var file_msg = map__36976__$1;
var namespace = cljs.core.get.call(null,map__36976__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__36978){
var map__36981 = p__36978;
var map__36981__$1 = ((((!((map__36981 == null)))?((((map__36981.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36981.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36981):map__36981);
var file_msg = map__36981__$1;
var namespace = cljs.core.get.call(null,map__36981__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__29909__auto__ = cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,file_msg));
if(and__29909__auto__){
var or__29921__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__29921__auto__)){
return or__29921__auto__;
} else {
var or__29921__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__29921__auto____$1)){
return or__29921__auto____$1;
} else {
return figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
}
}
} else {
return and__29909__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__36983,callback){
var map__36986 = p__36983;
var map__36986__$1 = ((((!((map__36986 == null)))?((((map__36986.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36986.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36986):map__36986);
var file_msg = map__36986__$1;
var request_url = cljs.core.get.call(null,map__36986__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__36986__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Figwheel: Not trying to load file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__32227__auto___37090 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto___37090,out){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto___37090,out){
return (function (state_37072){
var state_val_37073 = (state_37072[(1)]);
if((state_val_37073 === (1))){
var inst_37046 = cljs.core.seq.call(null,files);
var inst_37047 = cljs.core.first.call(null,inst_37046);
var inst_37048 = cljs.core.next.call(null,inst_37046);
var inst_37049 = files;
var state_37072__$1 = (function (){var statearr_37074 = state_37072;
(statearr_37074[(7)] = inst_37049);

(statearr_37074[(8)] = inst_37047);

(statearr_37074[(9)] = inst_37048);

return statearr_37074;
})();
var statearr_37075_37091 = state_37072__$1;
(statearr_37075_37091[(2)] = null);

(statearr_37075_37091[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37073 === (2))){
var inst_37049 = (state_37072[(7)]);
var inst_37055 = (state_37072[(10)]);
var inst_37054 = cljs.core.seq.call(null,inst_37049);
var inst_37055__$1 = cljs.core.first.call(null,inst_37054);
var inst_37056 = cljs.core.next.call(null,inst_37054);
var inst_37057 = (inst_37055__$1 == null);
var inst_37058 = cljs.core.not.call(null,inst_37057);
var state_37072__$1 = (function (){var statearr_37076 = state_37072;
(statearr_37076[(11)] = inst_37056);

(statearr_37076[(10)] = inst_37055__$1);

return statearr_37076;
})();
if(inst_37058){
var statearr_37077_37092 = state_37072__$1;
(statearr_37077_37092[(1)] = (4));

} else {
var statearr_37078_37093 = state_37072__$1;
(statearr_37078_37093[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37073 === (3))){
var inst_37070 = (state_37072[(2)]);
var state_37072__$1 = state_37072;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_37072__$1,inst_37070);
} else {
if((state_val_37073 === (4))){
var inst_37055 = (state_37072[(10)]);
var inst_37060 = figwheel.client.file_reloading.reload_js_file.call(null,inst_37055);
var state_37072__$1 = state_37072;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37072__$1,(7),inst_37060);
} else {
if((state_val_37073 === (5))){
var inst_37066 = cljs.core.async.close_BANG_.call(null,out);
var state_37072__$1 = state_37072;
var statearr_37079_37094 = state_37072__$1;
(statearr_37079_37094[(2)] = inst_37066);

(statearr_37079_37094[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37073 === (6))){
var inst_37068 = (state_37072[(2)]);
var state_37072__$1 = state_37072;
var statearr_37080_37095 = state_37072__$1;
(statearr_37080_37095[(2)] = inst_37068);

(statearr_37080_37095[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37073 === (7))){
var inst_37056 = (state_37072[(11)]);
var inst_37062 = (state_37072[(2)]);
var inst_37063 = cljs.core.async.put_BANG_.call(null,out,inst_37062);
var inst_37049 = inst_37056;
var state_37072__$1 = (function (){var statearr_37081 = state_37072;
(statearr_37081[(12)] = inst_37063);

(statearr_37081[(7)] = inst_37049);

return statearr_37081;
})();
var statearr_37082_37096 = state_37072__$1;
(statearr_37082_37096[(2)] = null);

(statearr_37082_37096[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__32227__auto___37090,out))
;
return ((function (switch__32115__auto__,c__32227__auto___37090,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto____0 = (function (){
var statearr_37086 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_37086[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto__);

(statearr_37086[(1)] = (1));

return statearr_37086;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto____1 = (function (state_37072){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_37072);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e37087){if((e37087 instanceof Object)){
var ex__32119__auto__ = e37087;
var statearr_37088_37097 = state_37072;
(statearr_37088_37097[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_37072);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e37087;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__37098 = state_37072;
state_37072 = G__37098;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto__ = function(state_37072){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto____1.call(this,state_37072);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto___37090,out))
})();
var state__32229__auto__ = (function (){var statearr_37089 = f__32228__auto__.call(null);
(statearr_37089[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto___37090);

return statearr_37089;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto___37090,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__37099,opts){
var map__37103 = p__37099;
var map__37103__$1 = ((((!((map__37103 == null)))?((((map__37103.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37103.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37103):map__37103);
var eval_body__$1 = cljs.core.get.call(null,map__37103__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__37103__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__29909__auto__ = eval_body__$1;
if(cljs.core.truth_(and__29909__auto__)){
return typeof eval_body__$1 === 'string';
} else {
return and__29909__auto__;
}
})())){
var code = eval_body__$1;
try{figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Evaling file "),cljs.core.str(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e37105){var e = e37105;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Unable to evaluate "),cljs.core.str(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,cljs.core.partial.call(null,cljs.core.re_matches,/figwheel\.connect.*/),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__4655__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__37106_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__37106_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__4655__auto__)){
var file_msg = temp__4655__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__37115){
var vec__37116 = p__37115;
var k = cljs.core.nth.call(null,vec__37116,(0),null);
var v = cljs.core.nth.call(null,vec__37116,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__37119){
var vec__37120 = p__37119;
var k = cljs.core.nth.call(null,vec__37120,(0),null);
var v = cljs.core.nth.call(null,vec__37120,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__37126,p__37127){
var map__37374 = p__37126;
var map__37374__$1 = ((((!((map__37374 == null)))?((((map__37374.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37374.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37374):map__37374);
var opts = map__37374__$1;
var before_jsload = cljs.core.get.call(null,map__37374__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__37374__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__37374__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__37375 = p__37127;
var map__37375__$1 = ((((!((map__37375 == null)))?((((map__37375.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37375.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37375):map__37375);
var msg = map__37375__$1;
var files = cljs.core.get.call(null,map__37375__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__37375__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__37375__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_37528){
var state_val_37529 = (state_37528[(1)]);
if((state_val_37529 === (7))){
var inst_37390 = (state_37528[(7)]);
var inst_37391 = (state_37528[(8)]);
var inst_37389 = (state_37528[(9)]);
var inst_37392 = (state_37528[(10)]);
var inst_37397 = cljs.core._nth.call(null,inst_37390,inst_37392);
var inst_37398 = figwheel.client.file_reloading.eval_body.call(null,inst_37397,opts);
var inst_37399 = (inst_37392 + (1));
var tmp37530 = inst_37390;
var tmp37531 = inst_37391;
var tmp37532 = inst_37389;
var inst_37389__$1 = tmp37532;
var inst_37390__$1 = tmp37530;
var inst_37391__$1 = tmp37531;
var inst_37392__$1 = inst_37399;
var state_37528__$1 = (function (){var statearr_37533 = state_37528;
(statearr_37533[(7)] = inst_37390__$1);

(statearr_37533[(8)] = inst_37391__$1);

(statearr_37533[(11)] = inst_37398);

(statearr_37533[(9)] = inst_37389__$1);

(statearr_37533[(10)] = inst_37392__$1);

return statearr_37533;
})();
var statearr_37534_37620 = state_37528__$1;
(statearr_37534_37620[(2)] = null);

(statearr_37534_37620[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (20))){
var inst_37432 = (state_37528[(12)]);
var inst_37440 = figwheel.client.file_reloading.sort_files.call(null,inst_37432);
var state_37528__$1 = state_37528;
var statearr_37535_37621 = state_37528__$1;
(statearr_37535_37621[(2)] = inst_37440);

(statearr_37535_37621[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (27))){
var state_37528__$1 = state_37528;
var statearr_37536_37622 = state_37528__$1;
(statearr_37536_37622[(2)] = null);

(statearr_37536_37622[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (1))){
var inst_37381 = (state_37528[(13)]);
var inst_37378 = before_jsload.call(null,files);
var inst_37379 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_37380 = (function (){return ((function (inst_37381,inst_37378,inst_37379,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__37123_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__37123_SHARP_);
});
;})(inst_37381,inst_37378,inst_37379,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_37381__$1 = cljs.core.filter.call(null,inst_37380,files);
var inst_37382 = cljs.core.not_empty.call(null,inst_37381__$1);
var state_37528__$1 = (function (){var statearr_37537 = state_37528;
(statearr_37537[(14)] = inst_37378);

(statearr_37537[(13)] = inst_37381__$1);

(statearr_37537[(15)] = inst_37379);

return statearr_37537;
})();
if(cljs.core.truth_(inst_37382)){
var statearr_37538_37623 = state_37528__$1;
(statearr_37538_37623[(1)] = (2));

} else {
var statearr_37539_37624 = state_37528__$1;
(statearr_37539_37624[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (24))){
var state_37528__$1 = state_37528;
var statearr_37540_37625 = state_37528__$1;
(statearr_37540_37625[(2)] = null);

(statearr_37540_37625[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (39))){
var inst_37482 = (state_37528[(16)]);
var state_37528__$1 = state_37528;
var statearr_37541_37626 = state_37528__$1;
(statearr_37541_37626[(2)] = inst_37482);

(statearr_37541_37626[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (46))){
var inst_37523 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
var statearr_37542_37627 = state_37528__$1;
(statearr_37542_37627[(2)] = inst_37523);

(statearr_37542_37627[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (4))){
var inst_37426 = (state_37528[(2)]);
var inst_37427 = cljs.core.List.EMPTY;
var inst_37428 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_37427);
var inst_37429 = (function (){return ((function (inst_37426,inst_37427,inst_37428,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__37124_SHARP_){
var and__29909__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__37124_SHARP_);
if(cljs.core.truth_(and__29909__auto__)){
return (cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__37124_SHARP_))) && (cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,p1__37124_SHARP_)));
} else {
return and__29909__auto__;
}
});
;})(inst_37426,inst_37427,inst_37428,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_37430 = cljs.core.filter.call(null,inst_37429,files);
var inst_37431 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_37432 = cljs.core.concat.call(null,inst_37430,inst_37431);
var state_37528__$1 = (function (){var statearr_37543 = state_37528;
(statearr_37543[(17)] = inst_37428);

(statearr_37543[(18)] = inst_37426);

(statearr_37543[(12)] = inst_37432);

return statearr_37543;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_37544_37628 = state_37528__$1;
(statearr_37544_37628[(1)] = (16));

} else {
var statearr_37545_37629 = state_37528__$1;
(statearr_37545_37629[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (15))){
var inst_37416 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
var statearr_37546_37630 = state_37528__$1;
(statearr_37546_37630[(2)] = inst_37416);

(statearr_37546_37630[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (21))){
var inst_37442 = (state_37528[(19)]);
var inst_37442__$1 = (state_37528[(2)]);
var inst_37443 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_37442__$1);
var state_37528__$1 = (function (){var statearr_37547 = state_37528;
(statearr_37547[(19)] = inst_37442__$1);

return statearr_37547;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37528__$1,(22),inst_37443);
} else {
if((state_val_37529 === (31))){
var inst_37526 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_37528__$1,inst_37526);
} else {
if((state_val_37529 === (32))){
var inst_37482 = (state_37528[(16)]);
var inst_37487 = inst_37482.cljs$lang$protocol_mask$partition0$;
var inst_37488 = (inst_37487 & (64));
var inst_37489 = inst_37482.cljs$core$ISeq$;
var inst_37490 = (inst_37488) || (inst_37489);
var state_37528__$1 = state_37528;
if(cljs.core.truth_(inst_37490)){
var statearr_37548_37631 = state_37528__$1;
(statearr_37548_37631[(1)] = (35));

} else {
var statearr_37549_37632 = state_37528__$1;
(statearr_37549_37632[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (40))){
var inst_37503 = (state_37528[(20)]);
var inst_37502 = (state_37528[(2)]);
var inst_37503__$1 = cljs.core.get.call(null,inst_37502,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_37504 = cljs.core.get.call(null,inst_37502,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_37505 = cljs.core.not_empty.call(null,inst_37503__$1);
var state_37528__$1 = (function (){var statearr_37550 = state_37528;
(statearr_37550[(20)] = inst_37503__$1);

(statearr_37550[(21)] = inst_37504);

return statearr_37550;
})();
if(cljs.core.truth_(inst_37505)){
var statearr_37551_37633 = state_37528__$1;
(statearr_37551_37633[(1)] = (41));

} else {
var statearr_37552_37634 = state_37528__$1;
(statearr_37552_37634[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (33))){
var state_37528__$1 = state_37528;
var statearr_37553_37635 = state_37528__$1;
(statearr_37553_37635[(2)] = false);

(statearr_37553_37635[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (13))){
var inst_37402 = (state_37528[(22)]);
var inst_37406 = cljs.core.chunk_first.call(null,inst_37402);
var inst_37407 = cljs.core.chunk_rest.call(null,inst_37402);
var inst_37408 = cljs.core.count.call(null,inst_37406);
var inst_37389 = inst_37407;
var inst_37390 = inst_37406;
var inst_37391 = inst_37408;
var inst_37392 = (0);
var state_37528__$1 = (function (){var statearr_37554 = state_37528;
(statearr_37554[(7)] = inst_37390);

(statearr_37554[(8)] = inst_37391);

(statearr_37554[(9)] = inst_37389);

(statearr_37554[(10)] = inst_37392);

return statearr_37554;
})();
var statearr_37555_37636 = state_37528__$1;
(statearr_37555_37636[(2)] = null);

(statearr_37555_37636[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (22))){
var inst_37445 = (state_37528[(23)]);
var inst_37450 = (state_37528[(24)]);
var inst_37446 = (state_37528[(25)]);
var inst_37442 = (state_37528[(19)]);
var inst_37445__$1 = (state_37528[(2)]);
var inst_37446__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_37445__$1);
var inst_37447 = (function (){var all_files = inst_37442;
var res_SINGLEQUOTE_ = inst_37445__$1;
var res = inst_37446__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_37445,inst_37450,inst_37446,inst_37442,inst_37445__$1,inst_37446__$1,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__37125_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__37125_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_37445,inst_37450,inst_37446,inst_37442,inst_37445__$1,inst_37446__$1,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_37448 = cljs.core.filter.call(null,inst_37447,inst_37445__$1);
var inst_37449 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_37450__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_37449);
var inst_37451 = cljs.core.not_empty.call(null,inst_37450__$1);
var state_37528__$1 = (function (){var statearr_37556 = state_37528;
(statearr_37556[(23)] = inst_37445__$1);

(statearr_37556[(24)] = inst_37450__$1);

(statearr_37556[(25)] = inst_37446__$1);

(statearr_37556[(26)] = inst_37448);

return statearr_37556;
})();
if(cljs.core.truth_(inst_37451)){
var statearr_37557_37637 = state_37528__$1;
(statearr_37557_37637[(1)] = (23));

} else {
var statearr_37558_37638 = state_37528__$1;
(statearr_37558_37638[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (36))){
var state_37528__$1 = state_37528;
var statearr_37559_37639 = state_37528__$1;
(statearr_37559_37639[(2)] = false);

(statearr_37559_37639[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (41))){
var inst_37503 = (state_37528[(20)]);
var inst_37507 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_37508 = cljs.core.map.call(null,inst_37507,inst_37503);
var inst_37509 = cljs.core.pr_str.call(null,inst_37508);
var inst_37510 = [cljs.core.str("figwheel-no-load meta-data: "),cljs.core.str(inst_37509)].join('');
var inst_37511 = figwheel.client.utils.log.call(null,inst_37510);
var state_37528__$1 = state_37528;
var statearr_37560_37640 = state_37528__$1;
(statearr_37560_37640[(2)] = inst_37511);

(statearr_37560_37640[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (43))){
var inst_37504 = (state_37528[(21)]);
var inst_37514 = (state_37528[(2)]);
var inst_37515 = cljs.core.not_empty.call(null,inst_37504);
var state_37528__$1 = (function (){var statearr_37561 = state_37528;
(statearr_37561[(27)] = inst_37514);

return statearr_37561;
})();
if(cljs.core.truth_(inst_37515)){
var statearr_37562_37641 = state_37528__$1;
(statearr_37562_37641[(1)] = (44));

} else {
var statearr_37563_37642 = state_37528__$1;
(statearr_37563_37642[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (29))){
var inst_37445 = (state_37528[(23)]);
var inst_37450 = (state_37528[(24)]);
var inst_37446 = (state_37528[(25)]);
var inst_37442 = (state_37528[(19)]);
var inst_37448 = (state_37528[(26)]);
var inst_37482 = (state_37528[(16)]);
var inst_37478 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_37481 = (function (){var all_files = inst_37442;
var res_SINGLEQUOTE_ = inst_37445;
var res = inst_37446;
var files_not_loaded = inst_37448;
var dependencies_that_loaded = inst_37450;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37482,inst_37478,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__37480){
var map__37564 = p__37480;
var map__37564__$1 = ((((!((map__37564 == null)))?((((map__37564.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37564.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37564):map__37564);
var namespace = cljs.core.get.call(null,map__37564__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37482,inst_37478,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_37482__$1 = cljs.core.group_by.call(null,inst_37481,inst_37448);
var inst_37484 = (inst_37482__$1 == null);
var inst_37485 = cljs.core.not.call(null,inst_37484);
var state_37528__$1 = (function (){var statearr_37566 = state_37528;
(statearr_37566[(28)] = inst_37478);

(statearr_37566[(16)] = inst_37482__$1);

return statearr_37566;
})();
if(inst_37485){
var statearr_37567_37643 = state_37528__$1;
(statearr_37567_37643[(1)] = (32));

} else {
var statearr_37568_37644 = state_37528__$1;
(statearr_37568_37644[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (44))){
var inst_37504 = (state_37528[(21)]);
var inst_37517 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_37504);
var inst_37518 = cljs.core.pr_str.call(null,inst_37517);
var inst_37519 = [cljs.core.str("not required: "),cljs.core.str(inst_37518)].join('');
var inst_37520 = figwheel.client.utils.log.call(null,inst_37519);
var state_37528__$1 = state_37528;
var statearr_37569_37645 = state_37528__$1;
(statearr_37569_37645[(2)] = inst_37520);

(statearr_37569_37645[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (6))){
var inst_37423 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
var statearr_37570_37646 = state_37528__$1;
(statearr_37570_37646[(2)] = inst_37423);

(statearr_37570_37646[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (28))){
var inst_37448 = (state_37528[(26)]);
var inst_37475 = (state_37528[(2)]);
var inst_37476 = cljs.core.not_empty.call(null,inst_37448);
var state_37528__$1 = (function (){var statearr_37571 = state_37528;
(statearr_37571[(29)] = inst_37475);

return statearr_37571;
})();
if(cljs.core.truth_(inst_37476)){
var statearr_37572_37647 = state_37528__$1;
(statearr_37572_37647[(1)] = (29));

} else {
var statearr_37573_37648 = state_37528__$1;
(statearr_37573_37648[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (25))){
var inst_37446 = (state_37528[(25)]);
var inst_37462 = (state_37528[(2)]);
var inst_37463 = cljs.core.not_empty.call(null,inst_37446);
var state_37528__$1 = (function (){var statearr_37574 = state_37528;
(statearr_37574[(30)] = inst_37462);

return statearr_37574;
})();
if(cljs.core.truth_(inst_37463)){
var statearr_37575_37649 = state_37528__$1;
(statearr_37575_37649[(1)] = (26));

} else {
var statearr_37576_37650 = state_37528__$1;
(statearr_37576_37650[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (34))){
var inst_37497 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
if(cljs.core.truth_(inst_37497)){
var statearr_37577_37651 = state_37528__$1;
(statearr_37577_37651[(1)] = (38));

} else {
var statearr_37578_37652 = state_37528__$1;
(statearr_37578_37652[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (17))){
var state_37528__$1 = state_37528;
var statearr_37579_37653 = state_37528__$1;
(statearr_37579_37653[(2)] = recompile_dependents);

(statearr_37579_37653[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (3))){
var state_37528__$1 = state_37528;
var statearr_37580_37654 = state_37528__$1;
(statearr_37580_37654[(2)] = null);

(statearr_37580_37654[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (12))){
var inst_37419 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
var statearr_37581_37655 = state_37528__$1;
(statearr_37581_37655[(2)] = inst_37419);

(statearr_37581_37655[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (2))){
var inst_37381 = (state_37528[(13)]);
var inst_37388 = cljs.core.seq.call(null,inst_37381);
var inst_37389 = inst_37388;
var inst_37390 = null;
var inst_37391 = (0);
var inst_37392 = (0);
var state_37528__$1 = (function (){var statearr_37582 = state_37528;
(statearr_37582[(7)] = inst_37390);

(statearr_37582[(8)] = inst_37391);

(statearr_37582[(9)] = inst_37389);

(statearr_37582[(10)] = inst_37392);

return statearr_37582;
})();
var statearr_37583_37656 = state_37528__$1;
(statearr_37583_37656[(2)] = null);

(statearr_37583_37656[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (23))){
var inst_37445 = (state_37528[(23)]);
var inst_37450 = (state_37528[(24)]);
var inst_37446 = (state_37528[(25)]);
var inst_37442 = (state_37528[(19)]);
var inst_37448 = (state_37528[(26)]);
var inst_37453 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_37455 = (function (){var all_files = inst_37442;
var res_SINGLEQUOTE_ = inst_37445;
var res = inst_37446;
var files_not_loaded = inst_37448;
var dependencies_that_loaded = inst_37450;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37453,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__37454){
var map__37584 = p__37454;
var map__37584__$1 = ((((!((map__37584 == null)))?((((map__37584.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37584.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37584):map__37584);
var request_url = cljs.core.get.call(null,map__37584__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37453,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_37456 = cljs.core.reverse.call(null,inst_37450);
var inst_37457 = cljs.core.map.call(null,inst_37455,inst_37456);
var inst_37458 = cljs.core.pr_str.call(null,inst_37457);
var inst_37459 = figwheel.client.utils.log.call(null,inst_37458);
var state_37528__$1 = (function (){var statearr_37586 = state_37528;
(statearr_37586[(31)] = inst_37453);

return statearr_37586;
})();
var statearr_37587_37657 = state_37528__$1;
(statearr_37587_37657[(2)] = inst_37459);

(statearr_37587_37657[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (35))){
var state_37528__$1 = state_37528;
var statearr_37588_37658 = state_37528__$1;
(statearr_37588_37658[(2)] = true);

(statearr_37588_37658[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (19))){
var inst_37432 = (state_37528[(12)]);
var inst_37438 = figwheel.client.file_reloading.expand_files.call(null,inst_37432);
var state_37528__$1 = state_37528;
var statearr_37589_37659 = state_37528__$1;
(statearr_37589_37659[(2)] = inst_37438);

(statearr_37589_37659[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (11))){
var state_37528__$1 = state_37528;
var statearr_37590_37660 = state_37528__$1;
(statearr_37590_37660[(2)] = null);

(statearr_37590_37660[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (9))){
var inst_37421 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
var statearr_37591_37661 = state_37528__$1;
(statearr_37591_37661[(2)] = inst_37421);

(statearr_37591_37661[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (5))){
var inst_37391 = (state_37528[(8)]);
var inst_37392 = (state_37528[(10)]);
var inst_37394 = (inst_37392 < inst_37391);
var inst_37395 = inst_37394;
var state_37528__$1 = state_37528;
if(cljs.core.truth_(inst_37395)){
var statearr_37592_37662 = state_37528__$1;
(statearr_37592_37662[(1)] = (7));

} else {
var statearr_37593_37663 = state_37528__$1;
(statearr_37593_37663[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (14))){
var inst_37402 = (state_37528[(22)]);
var inst_37411 = cljs.core.first.call(null,inst_37402);
var inst_37412 = figwheel.client.file_reloading.eval_body.call(null,inst_37411,opts);
var inst_37413 = cljs.core.next.call(null,inst_37402);
var inst_37389 = inst_37413;
var inst_37390 = null;
var inst_37391 = (0);
var inst_37392 = (0);
var state_37528__$1 = (function (){var statearr_37594 = state_37528;
(statearr_37594[(7)] = inst_37390);

(statearr_37594[(8)] = inst_37391);

(statearr_37594[(32)] = inst_37412);

(statearr_37594[(9)] = inst_37389);

(statearr_37594[(10)] = inst_37392);

return statearr_37594;
})();
var statearr_37595_37664 = state_37528__$1;
(statearr_37595_37664[(2)] = null);

(statearr_37595_37664[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (45))){
var state_37528__$1 = state_37528;
var statearr_37596_37665 = state_37528__$1;
(statearr_37596_37665[(2)] = null);

(statearr_37596_37665[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (26))){
var inst_37445 = (state_37528[(23)]);
var inst_37450 = (state_37528[(24)]);
var inst_37446 = (state_37528[(25)]);
var inst_37442 = (state_37528[(19)]);
var inst_37448 = (state_37528[(26)]);
var inst_37465 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_37467 = (function (){var all_files = inst_37442;
var res_SINGLEQUOTE_ = inst_37445;
var res = inst_37446;
var files_not_loaded = inst_37448;
var dependencies_that_loaded = inst_37450;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37465,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__37466){
var map__37597 = p__37466;
var map__37597__$1 = ((((!((map__37597 == null)))?((((map__37597.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37597.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37597):map__37597);
var namespace = cljs.core.get.call(null,map__37597__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__37597__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37465,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_37468 = cljs.core.map.call(null,inst_37467,inst_37446);
var inst_37469 = cljs.core.pr_str.call(null,inst_37468);
var inst_37470 = figwheel.client.utils.log.call(null,inst_37469);
var inst_37471 = (function (){var all_files = inst_37442;
var res_SINGLEQUOTE_ = inst_37445;
var res = inst_37446;
var files_not_loaded = inst_37448;
var dependencies_that_loaded = inst_37450;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37465,inst_37467,inst_37468,inst_37469,inst_37470,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_37445,inst_37450,inst_37446,inst_37442,inst_37448,inst_37465,inst_37467,inst_37468,inst_37469,inst_37470,state_val_37529,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_37472 = setTimeout(inst_37471,(10));
var state_37528__$1 = (function (){var statearr_37599 = state_37528;
(statearr_37599[(33)] = inst_37465);

(statearr_37599[(34)] = inst_37470);

return statearr_37599;
})();
var statearr_37600_37666 = state_37528__$1;
(statearr_37600_37666[(2)] = inst_37472);

(statearr_37600_37666[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (16))){
var state_37528__$1 = state_37528;
var statearr_37601_37667 = state_37528__$1;
(statearr_37601_37667[(2)] = reload_dependents);

(statearr_37601_37667[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (38))){
var inst_37482 = (state_37528[(16)]);
var inst_37499 = cljs.core.apply.call(null,cljs.core.hash_map,inst_37482);
var state_37528__$1 = state_37528;
var statearr_37602_37668 = state_37528__$1;
(statearr_37602_37668[(2)] = inst_37499);

(statearr_37602_37668[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (30))){
var state_37528__$1 = state_37528;
var statearr_37603_37669 = state_37528__$1;
(statearr_37603_37669[(2)] = null);

(statearr_37603_37669[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (10))){
var inst_37402 = (state_37528[(22)]);
var inst_37404 = cljs.core.chunked_seq_QMARK_.call(null,inst_37402);
var state_37528__$1 = state_37528;
if(inst_37404){
var statearr_37604_37670 = state_37528__$1;
(statearr_37604_37670[(1)] = (13));

} else {
var statearr_37605_37671 = state_37528__$1;
(statearr_37605_37671[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (18))){
var inst_37436 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
if(cljs.core.truth_(inst_37436)){
var statearr_37606_37672 = state_37528__$1;
(statearr_37606_37672[(1)] = (19));

} else {
var statearr_37607_37673 = state_37528__$1;
(statearr_37607_37673[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (42))){
var state_37528__$1 = state_37528;
var statearr_37608_37674 = state_37528__$1;
(statearr_37608_37674[(2)] = null);

(statearr_37608_37674[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (37))){
var inst_37494 = (state_37528[(2)]);
var state_37528__$1 = state_37528;
var statearr_37609_37675 = state_37528__$1;
(statearr_37609_37675[(2)] = inst_37494);

(statearr_37609_37675[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37529 === (8))){
var inst_37402 = (state_37528[(22)]);
var inst_37389 = (state_37528[(9)]);
var inst_37402__$1 = cljs.core.seq.call(null,inst_37389);
var state_37528__$1 = (function (){var statearr_37610 = state_37528;
(statearr_37610[(22)] = inst_37402__$1);

return statearr_37610;
})();
if(inst_37402__$1){
var statearr_37611_37676 = state_37528__$1;
(statearr_37611_37676[(1)] = (10));

} else {
var statearr_37612_37677 = state_37528__$1;
(statearr_37612_37677[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__32115__auto__,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto____0 = (function (){
var statearr_37616 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_37616[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto__);

(statearr_37616[(1)] = (1));

return statearr_37616;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto____1 = (function (state_37528){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_37528);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e37617){if((e37617 instanceof Object)){
var ex__32119__auto__ = e37617;
var statearr_37618_37678 = state_37528;
(statearr_37618_37678[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_37528);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e37617;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__37679 = state_37528;
state_37528 = G__37679;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto__ = function(state_37528){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto____1.call(this,state_37528);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__32229__auto__ = (function (){var statearr_37619 = f__32228__auto__.call(null);
(statearr_37619[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_37619;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__,map__37374,map__37374__$1,opts,before_jsload,on_jsload,reload_dependents,map__37375,map__37375__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__32227__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str(location.protocol),cljs.core.str("//")].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__37682,link){
var map__37685 = p__37682;
var map__37685__$1 = ((((!((map__37685 == null)))?((((map__37685.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37685.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37685):map__37685);
var file = cljs.core.get.call(null,map__37685__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4657__auto__ = link.href;
if(cljs.core.truth_(temp__4657__auto__)){
var link_href = temp__4657__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__4657__auto__,map__37685,map__37685__$1,file){
return (function (p1__37680_SHARP_,p2__37681_SHARP_){
if(cljs.core._EQ_.call(null,p1__37680_SHARP_,p2__37681_SHARP_)){
return p1__37680_SHARP_;
} else {
return false;
}
});})(link_href,temp__4657__auto__,map__37685,map__37685__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__4657__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__37691){
var map__37692 = p__37691;
var map__37692__$1 = ((((!((map__37692 == null)))?((((map__37692.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37692.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37692):map__37692);
var match_length = cljs.core.get.call(null,map__37692__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__37692__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__37687_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__37687_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__4657__auto__)){
var res = temp__4657__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.distinctify = (function figwheel$client$file_reloading$distinctify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__37694_SHARP_,p2__37695_SHARP_){
return cljs.core.assoc.call(null,p1__37694_SHARP_,cljs.core.get.call(null,p2__37695_SHARP_,key),p2__37695_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.add_link_to_document = (function figwheel$client$file_reloading$add_link_to_document(orig_link,klone,finished_fn){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
parent.removeChild(orig_link);

return finished_fn.call(null);
});})(parent))
,(300));
});
if(typeof figwheel.client.file_reloading.reload_css_deferred_chain !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_css_deferred_chain = cljs.core.atom.call(null,goog.async.Deferred.succeed());
}
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(f_data,fin){
var temp__4655__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__4655__auto__)){
var link = temp__4655__auto__;
return figwheel.client.file_reloading.add_link_to_document.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href),((function (link,temp__4655__auto__){
return (function (){
return fin.call(null,cljs.core.assoc.call(null,f_data,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),true));
});})(link,temp__4655__auto__))
);
} else {
return fin.call(null,f_data);
}
});
figwheel.client.file_reloading.reload_css_files_STAR_ = (function figwheel$client$file_reloading$reload_css_files_STAR_(deferred,f_datas,on_cssload){
return figwheel.client.utils.liftContD.call(null,figwheel.client.utils.mapConcatD.call(null,deferred,figwheel.client.file_reloading.reload_css_file,f_datas),(function (f_datas_SINGLEQUOTE_,fin){
var loaded_f_datas_37696 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),f_datas_SINGLEQUOTE_);
figwheel.client.file_reloading.on_cssload_custom_event.call(null,loaded_f_datas_37696);

if(cljs.core.fn_QMARK_.call(null,on_cssload)){
on_cssload.call(null,loaded_f_datas_37696);
} else {
}

return fin.call(null);
}));
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__37697,p__37698){
var map__37703 = p__37697;
var map__37703__$1 = ((((!((map__37703 == null)))?((((map__37703.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37703.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37703):map__37703);
var on_cssload = cljs.core.get.call(null,map__37703__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
var map__37704 = p__37698;
var map__37704__$1 = ((((!((map__37704 == null)))?((((map__37704.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37704.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37704):map__37704);
var files_msg = map__37704__$1;
var files = cljs.core.get.call(null,map__37704__$1,new cljs.core.Keyword(null,"files","files",-472457450));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var temp__4657__auto__ = cljs.core.not_empty.call(null,figwheel.client.file_reloading.distinctify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files));
if(cljs.core.truth_(temp__4657__auto__)){
var f_datas = temp__4657__auto__;
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.reload_css_deferred_chain,figwheel.client.file_reloading.reload_css_files_STAR_,f_datas,on_cssload);
} else {
return null;
}
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1494229528008