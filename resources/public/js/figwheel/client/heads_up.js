// Compiled by ClojureScript 1.9.89 {}
goog.provide('figwheel.client.heads_up');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('goog.dom.dataset');
goog.require('goog.string');
goog.require('cljs.core.async');
goog.require('goog.object');
goog.require('figwheel.client.socket');
goog.require('cljs.pprint');
goog.require('clojure.string');
goog.require('figwheel.client.utils');

figwheel.client.heads_up.node = (function figwheel$client$heads_up$node(var_args){
var args__31003__auto__ = [];
var len__30996__auto___37849 = arguments.length;
var i__30997__auto___37850 = (0);
while(true){
if((i__30997__auto___37850 < len__30996__auto___37849)){
args__31003__auto__.push((arguments[i__30997__auto___37850]));

var G__37851 = (i__30997__auto___37850 + (1));
i__30997__auto___37850 = G__37851;
continue;
} else {
}
break;
}

var argseq__31004__auto__ = ((((2) < args__31003__auto__.length))?(new cljs.core.IndexedSeq(args__31003__auto__.slice((2)),(0),null)):null);
return figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__31004__auto__);
});

figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic = (function (t,attrs,children){
var e = document.createElement(cljs.core.name.call(null,t));
var seq__37841_37852 = cljs.core.seq.call(null,cljs.core.keys.call(null,attrs));
var chunk__37842_37853 = null;
var count__37843_37854 = (0);
var i__37844_37855 = (0);
while(true){
if((i__37844_37855 < count__37843_37854)){
var k_37856 = cljs.core._nth.call(null,chunk__37842_37853,i__37844_37855);
e.setAttribute(cljs.core.name.call(null,k_37856),cljs.core.get.call(null,attrs,k_37856));

var G__37857 = seq__37841_37852;
var G__37858 = chunk__37842_37853;
var G__37859 = count__37843_37854;
var G__37860 = (i__37844_37855 + (1));
seq__37841_37852 = G__37857;
chunk__37842_37853 = G__37858;
count__37843_37854 = G__37859;
i__37844_37855 = G__37860;
continue;
} else {
var temp__4657__auto___37861 = cljs.core.seq.call(null,seq__37841_37852);
if(temp__4657__auto___37861){
var seq__37841_37862__$1 = temp__4657__auto___37861;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37841_37862__$1)){
var c__30732__auto___37863 = cljs.core.chunk_first.call(null,seq__37841_37862__$1);
var G__37864 = cljs.core.chunk_rest.call(null,seq__37841_37862__$1);
var G__37865 = c__30732__auto___37863;
var G__37866 = cljs.core.count.call(null,c__30732__auto___37863);
var G__37867 = (0);
seq__37841_37852 = G__37864;
chunk__37842_37853 = G__37865;
count__37843_37854 = G__37866;
i__37844_37855 = G__37867;
continue;
} else {
var k_37868 = cljs.core.first.call(null,seq__37841_37862__$1);
e.setAttribute(cljs.core.name.call(null,k_37868),cljs.core.get.call(null,attrs,k_37868));

var G__37869 = cljs.core.next.call(null,seq__37841_37862__$1);
var G__37870 = null;
var G__37871 = (0);
var G__37872 = (0);
seq__37841_37852 = G__37869;
chunk__37842_37853 = G__37870;
count__37843_37854 = G__37871;
i__37844_37855 = G__37872;
continue;
}
} else {
}
}
break;
}

var seq__37845_37873 = cljs.core.seq.call(null,children);
var chunk__37846_37874 = null;
var count__37847_37875 = (0);
var i__37848_37876 = (0);
while(true){
if((i__37848_37876 < count__37847_37875)){
var ch_37877 = cljs.core._nth.call(null,chunk__37846_37874,i__37848_37876);
e.appendChild(ch_37877);

var G__37878 = seq__37845_37873;
var G__37879 = chunk__37846_37874;
var G__37880 = count__37847_37875;
var G__37881 = (i__37848_37876 + (1));
seq__37845_37873 = G__37878;
chunk__37846_37874 = G__37879;
count__37847_37875 = G__37880;
i__37848_37876 = G__37881;
continue;
} else {
var temp__4657__auto___37882 = cljs.core.seq.call(null,seq__37845_37873);
if(temp__4657__auto___37882){
var seq__37845_37883__$1 = temp__4657__auto___37882;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37845_37883__$1)){
var c__30732__auto___37884 = cljs.core.chunk_first.call(null,seq__37845_37883__$1);
var G__37885 = cljs.core.chunk_rest.call(null,seq__37845_37883__$1);
var G__37886 = c__30732__auto___37884;
var G__37887 = cljs.core.count.call(null,c__30732__auto___37884);
var G__37888 = (0);
seq__37845_37873 = G__37885;
chunk__37846_37874 = G__37886;
count__37847_37875 = G__37887;
i__37848_37876 = G__37888;
continue;
} else {
var ch_37889 = cljs.core.first.call(null,seq__37845_37883__$1);
e.appendChild(ch_37889);

var G__37890 = cljs.core.next.call(null,seq__37845_37883__$1);
var G__37891 = null;
var G__37892 = (0);
var G__37893 = (0);
seq__37845_37873 = G__37890;
chunk__37846_37874 = G__37891;
count__37847_37875 = G__37892;
i__37848_37876 = G__37893;
continue;
}
} else {
}
}
break;
}

return e;
});

figwheel.client.heads_up.node.cljs$lang$maxFixedArity = (2);

figwheel.client.heads_up.node.cljs$lang$applyTo = (function (seq37838){
var G__37839 = cljs.core.first.call(null,seq37838);
var seq37838__$1 = cljs.core.next.call(null,seq37838);
var G__37840 = cljs.core.first.call(null,seq37838__$1);
var seq37838__$2 = cljs.core.next.call(null,seq37838__$1);
return figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic(G__37839,G__37840,seq37838__$2);
});

if(typeof figwheel.client.heads_up.heads_up_event_dispatch !== 'undefined'){
} else {
figwheel.client.heads_up.heads_up_event_dispatch = (function (){var method_table__30846__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__30847__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__30848__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__30849__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__30850__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"figwheel.client.heads-up","heads-up-event-dispatch"),((function (method_table__30846__auto__,prefer_table__30847__auto__,method_cache__30848__auto__,cached_hierarchy__30849__auto__,hierarchy__30850__auto__){
return (function (dataset){
return dataset.figwheelEvent;
});})(method_table__30846__auto__,prefer_table__30847__auto__,method_cache__30848__auto__,cached_hierarchy__30849__auto__,hierarchy__30850__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__30850__auto__,method_table__30846__auto__,prefer_table__30847__auto__,method_cache__30848__auto__,cached_hierarchy__30849__auto__));
})();
}
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,new cljs.core.Keyword(null,"default","default",-1987822328),(function (_){
return cljs.core.PersistentArrayMap.EMPTY;
}));
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,"file-selected",(function (dataset){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"file-selected",new cljs.core.Keyword(null,"file-name","file-name",-1654217259),dataset.fileName,new cljs.core.Keyword(null,"file-line","file-line",-1228823138),dataset.fileLine,new cljs.core.Keyword(null,"file-column","file-column",1543934780),dataset.fileColumn], null));
}));
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,"close-heads-up",(function (dataset){
return figwheel.client.heads_up.clear.call(null);
}));
figwheel.client.heads_up.ancestor_nodes = (function figwheel$client$heads_up$ancestor_nodes(el){
return cljs.core.iterate.call(null,(function (e){
return e.parentNode;
}),el);
});
figwheel.client.heads_up.get_dataset = (function figwheel$client$heads_up$get_dataset(el){
return cljs.core.first.call(null,cljs.core.keep.call(null,(function (x){
if(cljs.core.truth_(x.dataset.figwheelEvent)){
return x.dataset;
} else {
return null;
}
}),cljs.core.take.call(null,(4),figwheel.client.heads_up.ancestor_nodes.call(null,el))));
});
figwheel.client.heads_up.heads_up_onclick_handler = (function figwheel$client$heads_up$heads_up_onclick_handler(event){
var dataset = figwheel.client.heads_up.get_dataset.call(null,event.target);
event.preventDefault();

if(cljs.core.truth_(dataset)){
return figwheel.client.heads_up.heads_up_event_dispatch.call(null,dataset);
} else {
return null;
}
});
figwheel.client.heads_up.ensure_container = (function figwheel$client$heads_up$ensure_container(){
var cont_id = "figwheel-heads-up-container";
var content_id = "figwheel-heads-up-content-area";
if(cljs.core.not.call(null,document.querySelector([cljs.core.str("#"),cljs.core.str(cont_id)].join('')))){
var el_37894 = figwheel.client.heads_up.node.call(null,new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),cont_id,new cljs.core.Keyword(null,"style","style",-496642736),[cljs.core.str("-webkit-transition: all 0.2s ease-in-out;"),cljs.core.str("-moz-transition: all 0.2s ease-in-out;"),cljs.core.str("-o-transition: all 0.2s ease-in-out;"),cljs.core.str("transition: all 0.2s ease-in-out;"),cljs.core.str("font-size: 13px;"),cljs.core.str("border-top: 1px solid #f5f5f5;"),cljs.core.str("box-shadow: 0px 0px 1px #aaaaaa;"),cljs.core.str("line-height: 18px;"),cljs.core.str("color: #333;"),cljs.core.str("font-family: monospace;"),cljs.core.str("padding: 0px 10px 0px 70px;"),cljs.core.str("position: fixed;"),cljs.core.str("bottom: 0px;"),cljs.core.str("left: 0px;"),cljs.core.str("height: 0px;"),cljs.core.str("opacity: 0.0;"),cljs.core.str("box-sizing: border-box;"),cljs.core.str("z-index: 10000;"),cljs.core.str("text-align: left;")].join('')], null));
el_37894.onclick = figwheel.client.heads_up.heads_up_onclick_handler;

el_37894.innerHTML = figwheel.client.heads_up.cljs_logo_svg;

el_37894.appendChild(figwheel.client.heads_up.node.call(null,new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),content_id], null)));

document.body.appendChild(el_37894);
} else {
}

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"container-el","container-el",109664205),document.getElementById(cont_id),new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187),document.getElementById(content_id)], null);
});
figwheel.client.heads_up.set_style_BANG_ = (function figwheel$client$heads_up$set_style_BANG_(p__37895,st_map){
var map__37902 = p__37895;
var map__37902__$1 = ((((!((map__37902 == null)))?((((map__37902.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37902.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37902):map__37902);
var container_el = cljs.core.get.call(null,map__37902__$1,new cljs.core.Keyword(null,"container-el","container-el",109664205));
return cljs.core.mapv.call(null,((function (map__37902,map__37902__$1,container_el){
return (function (p__37904){
var vec__37905 = p__37904;
var k = cljs.core.nth.call(null,vec__37905,(0),null);
var v = cljs.core.nth.call(null,vec__37905,(1),null);
return (container_el.style[cljs.core.name.call(null,k)] = v);
});})(map__37902,map__37902__$1,container_el))
,st_map);
});
figwheel.client.heads_up.set_content_BANG_ = (function figwheel$client$heads_up$set_content_BANG_(p__37908,dom_str){
var map__37911 = p__37908;
var map__37911__$1 = ((((!((map__37911 == null)))?((((map__37911.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37911.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37911):map__37911);
var c = map__37911__$1;
var content_area_el = cljs.core.get.call(null,map__37911__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
return content_area_el.innerHTML = dom_str;
});
figwheel.client.heads_up.get_content = (function figwheel$client$heads_up$get_content(p__37913){
var map__37916 = p__37913;
var map__37916__$1 = ((((!((map__37916 == null)))?((((map__37916.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37916.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37916):map__37916);
var content_area_el = cljs.core.get.call(null,map__37916__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
return content_area_el.innerHTML;
});
figwheel.client.heads_up.close_link = (function figwheel$client$heads_up$close_link(){
return [cljs.core.str("<a style=\""),cljs.core.str("float: right;"),cljs.core.str("font-size: 18px;"),cljs.core.str("text-decoration: none;"),cljs.core.str("text-align: right;"),cljs.core.str("width: 30px;"),cljs.core.str("height: 30px;"),cljs.core.str("color: rgba(84,84,84, 0.5);"),cljs.core.str("\" href=\"#\"  data-figwheel-event=\"close-heads-up\">"),cljs.core.str("x"),cljs.core.str("</a>")].join('');
});
figwheel.client.heads_up.display_heads_up = (function figwheel$client$heads_up$display_heads_up(style,msg){
var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__){
return (function (state_37959){
var state_val_37960 = (state_37959[(1)]);
if((state_val_37960 === (1))){
var inst_37944 = (state_37959[(7)]);
var inst_37944__$1 = figwheel.client.heads_up.ensure_container.call(null);
var inst_37945 = [new cljs.core.Keyword(null,"paddingTop","paddingTop",-1088692345),new cljs.core.Keyword(null,"paddingBottom","paddingBottom",-916694489),new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"minHeight","minHeight",-1635998980),new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_37946 = ["10px","10px","100%","68px","1.0"];
var inst_37947 = cljs.core.PersistentHashMap.fromArrays(inst_37945,inst_37946);
var inst_37948 = cljs.core.merge.call(null,inst_37947,style);
var inst_37949 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_37944__$1,inst_37948);
var inst_37950 = figwheel.client.heads_up.set_content_BANG_.call(null,inst_37944__$1,msg);
var inst_37951 = cljs.core.async.timeout.call(null,(300));
var state_37959__$1 = (function (){var statearr_37961 = state_37959;
(statearr_37961[(8)] = inst_37949);

(statearr_37961[(7)] = inst_37944__$1);

(statearr_37961[(9)] = inst_37950);

return statearr_37961;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37959__$1,(2),inst_37951);
} else {
if((state_val_37960 === (2))){
var inst_37944 = (state_37959[(7)]);
var inst_37953 = (state_37959[(2)]);
var inst_37954 = [new cljs.core.Keyword(null,"height","height",1025178622)];
var inst_37955 = ["auto"];
var inst_37956 = cljs.core.PersistentHashMap.fromArrays(inst_37954,inst_37955);
var inst_37957 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_37944,inst_37956);
var state_37959__$1 = (function (){var statearr_37962 = state_37959;
(statearr_37962[(10)] = inst_37953);

return statearr_37962;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_37959__$1,inst_37957);
} else {
return null;
}
}
});})(c__32227__auto__))
;
return ((function (switch__32115__auto__,c__32227__auto__){
return (function() {
var figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto__ = null;
var figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto____0 = (function (){
var statearr_37966 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_37966[(0)] = figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto__);

(statearr_37966[(1)] = (1));

return statearr_37966;
});
var figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto____1 = (function (state_37959){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_37959);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e37967){if((e37967 instanceof Object)){
var ex__32119__auto__ = e37967;
var statearr_37968_37970 = state_37959;
(statearr_37968_37970[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_37959);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e37967;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__37971 = state_37959;
state_37959 = G__37971;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto__ = function(state_37959){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto____1.call(this,state_37959);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto____0;
figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto____1;
return figwheel$client$heads_up$display_heads_up_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__))
})();
var state__32229__auto__ = (function (){var statearr_37969 = f__32228__auto__.call(null);
(statearr_37969[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_37969;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__))
);

return c__32227__auto__;
});
figwheel.client.heads_up.heading = (function figwheel$client$heads_up$heading(var_args){
var args37972 = [];
var len__30996__auto___37975 = arguments.length;
var i__30997__auto___37976 = (0);
while(true){
if((i__30997__auto___37976 < len__30996__auto___37975)){
args37972.push((arguments[i__30997__auto___37976]));

var G__37977 = (i__30997__auto___37976 + (1));
i__30997__auto___37976 = G__37977;
continue;
} else {
}
break;
}

var G__37974 = args37972.length;
switch (G__37974) {
case 1:
return figwheel.client.heads_up.heading.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return figwheel.client.heads_up.heading.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args37972.length)].join('')));

}
});

figwheel.client.heads_up.heading.cljs$core$IFn$_invoke$arity$1 = (function (s){
return figwheel.client.heads_up.heading.call(null,s,"");
});

figwheel.client.heads_up.heading.cljs$core$IFn$_invoke$arity$2 = (function (s,sub_head){
return [cljs.core.str("<div style=\""),cljs.core.str("font-size: 26px;"),cljs.core.str("line-height: 26px;"),cljs.core.str("margin-bottom: 2px;"),cljs.core.str("padding-top: 1px;"),cljs.core.str("\">"),cljs.core.str(s),cljs.core.str(" <span style=\""),cljs.core.str("display: inline-block;"),cljs.core.str("font-size: 13px;"),cljs.core.str("\">"),cljs.core.str(sub_head),cljs.core.str("</span></div>")].join('');
});

figwheel.client.heads_up.heading.cljs$lang$maxFixedArity = 2;

figwheel.client.heads_up.file_selector_div = (function figwheel$client$heads_up$file_selector_div(file_name,line_number,column_number,msg){
return [cljs.core.str("<div style=\"cursor: pointer;\" data-figwheel-event=\"file-selected\" data-file-name=\""),cljs.core.str(file_name),cljs.core.str("\" data-file-line=\""),cljs.core.str(line_number),cljs.core.str("\" data-file-column=\""),cljs.core.str(column_number),cljs.core.str("\">"),cljs.core.str(msg),cljs.core.str("</div>")].join('');
});
figwheel.client.heads_up.format_line = (function figwheel$client$heads_up$format_line(msg,p__37979){
var map__37982 = p__37979;
var map__37982__$1 = ((((!((map__37982 == null)))?((((map__37982.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37982.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37982):map__37982);
var file = cljs.core.get.call(null,map__37982__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__37982__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__37982__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var msg__$1 = goog.string.htmlEscape(msg);
if(cljs.core.truth_((function (){var or__29921__auto__ = file;
if(cljs.core.truth_(or__29921__auto__)){
return or__29921__auto__;
} else {
return line;
}
})())){
return figwheel.client.heads_up.file_selector_div.call(null,file,line,column,msg__$1);
} else {
return [cljs.core.str("<div>"),cljs.core.str(msg__$1),cljs.core.str("</div>")].join('');
}
});
figwheel.client.heads_up.escape = (function figwheel$client$heads_up$escape(x){
return goog.string.htmlEscape(x);
});
figwheel.client.heads_up.pad_line_number = (function figwheel$client$heads_up$pad_line_number(n,line_number){
var len = cljs.core.count.call(null,cljs.core.fnil.call(null,cljs.core.str,"").call(null,line_number));
return [cljs.core.str((((len < n))?cljs.core.apply.call(null,cljs.core.str,cljs.core.repeat.call(null,(n - len)," ")):"")),cljs.core.str(line_number)].join('');
});
figwheel.client.heads_up.inline_error_line = (function figwheel$client$heads_up$inline_error_line(style,line_number,line){
return [cljs.core.str("<span style='"),cljs.core.str(style),cljs.core.str("'>"),cljs.core.str("<span style='color: #757575;'>"),cljs.core.str(line_number),cljs.core.str("  </span>"),cljs.core.str(figwheel.client.heads_up.escape.call(null,line)),cljs.core.str("</span>")].join('');
});
figwheel.client.heads_up.format_inline_error_line = (function figwheel$client$heads_up$format_inline_error_line(p__37984){
var vec__37991 = p__37984;
var typ = cljs.core.nth.call(null,vec__37991,(0),null);
var line_number = cljs.core.nth.call(null,vec__37991,(1),null);
var line = cljs.core.nth.call(null,vec__37991,(2),null);
var pred__37994 = cljs.core._EQ_;
var expr__37995 = typ;
if(cljs.core.truth_(pred__37994.call(null,new cljs.core.Keyword(null,"code-line","code-line",-2138627853),expr__37995))){
return figwheel.client.heads_up.inline_error_line.call(null,"color: #999;",line_number,line);
} else {
if(cljs.core.truth_(pred__37994.call(null,new cljs.core.Keyword(null,"error-in-code","error-in-code",-1661931357),expr__37995))){
return figwheel.client.heads_up.inline_error_line.call(null,"color: #ccc; font-weight: bold;",line_number,line);
} else {
if(cljs.core.truth_(pred__37994.call(null,new cljs.core.Keyword(null,"error-message","error-message",1756021561),expr__37995))){
return figwheel.client.heads_up.inline_error_line.call(null,"color: #D07D7D;",line_number,line);
} else {
return figwheel.client.heads_up.inline_error_line.call(null,"color: #666;",line_number,line);
}
}
}
});
figwheel.client.heads_up.pad_line_numbers = (function figwheel$client$heads_up$pad_line_numbers(inline_error){
var max_line_number_length = cljs.core.count.call(null,[cljs.core.str(cljs.core.reduce.call(null,cljs.core.max,cljs.core.map.call(null,cljs.core.second,inline_error)))].join(''));
return cljs.core.map.call(null,((function (max_line_number_length){
return (function (p1__37997_SHARP_){
return cljs.core.update_in.call(null,p1__37997_SHARP_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(1)], null),cljs.core.partial.call(null,figwheel.client.heads_up.pad_line_number,max_line_number_length));
});})(max_line_number_length))
,inline_error);
});
figwheel.client.heads_up.format_inline_error = (function figwheel$client$heads_up$format_inline_error(inline_error){
var lines = cljs.core.map.call(null,figwheel.client.heads_up.format_inline_error_line,figwheel.client.heads_up.pad_line_numbers.call(null,inline_error));
return [cljs.core.str("<pre style='whitespace:pre; overflow-x: scroll; display:block; font-family:monospace; font-size:0.8em; border-radius: 3px;"),cljs.core.str(" line-height: 1.1em; padding: 10px; background-color: rgb(24,26,38); margin-right: 5px'>"),cljs.core.str(clojure.string.join.call(null,"\n",lines)),cljs.core.str("</pre>")].join('');
});
figwheel.client.heads_up.flatten_exception = (function figwheel$client$heads_up$flatten_exception(p1__37998_SHARP_){
return cljs.core.take_while.call(null,cljs.core.some_QMARK_,cljs.core.iterate.call(null,new cljs.core.Keyword(null,"cause","cause",231901252),p1__37998_SHARP_));
});
figwheel.client.heads_up.exception__GT_display_data = (function figwheel$client$heads_up$exception__GT_display_data(p__38001){
var map__38004 = p__38001;
var map__38004__$1 = ((((!((map__38004 == null)))?((((map__38004.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38004.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38004):map__38004);
var exception = map__38004__$1;
var message = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var failed_loading_clj_file = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"failed-loading-clj-file","failed-loading-clj-file",-1682536481));
var reader_exception = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"reader-exception","reader-exception",-1938323098));
var file = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var column = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var failed_compiling = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"failed-compiling","failed-compiling",1768639503));
var error_inline = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"error-inline","error-inline",1073987185));
var line = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"line","line",212345235));
var class$ = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var analysis_exception = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"analysis-exception","analysis-exception",591623285));
var display_ex_data = cljs.core.get.call(null,map__38004__$1,new cljs.core.Keyword(null,"display-ex-data","display-ex-data",-1611558730));
var last_message = (cljs.core.truth_((function (){var and__29909__auto__ = file;
if(cljs.core.truth_(and__29909__auto__)){
return line;
} else {
return and__29909__auto__;
}
})())?[cljs.core.str("Please see line "),cljs.core.str(line),cljs.core.str(" of file "),cljs.core.str(file)].join(''):(cljs.core.truth_(file)?[cljs.core.str("Please see "),cljs.core.str(file)].join(''):null
));
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"head","head",-771383919),(cljs.core.truth_(failed_loading_clj_file)?"Couldn't load Clojure file":(cljs.core.truth_(analysis_exception)?"Could not Analyze":(cljs.core.truth_(reader_exception)?"Could not Read":(cljs.core.truth_(failed_compiling)?"Could not Compile":"Compile Exception"
)))),new cljs.core.Keyword(null,"sub-head","sub-head",1930649117),file,new cljs.core.Keyword(null,"messages","messages",345434482),cljs.core.concat.call(null,cljs.core.map.call(null,((function (last_message,map__38004,map__38004__$1,exception,message,failed_loading_clj_file,reader_exception,file,column,failed_compiling,error_inline,line,class$,analysis_exception,display_ex_data){
return (function (p1__37999_SHARP_){
return [cljs.core.str("<div>"),cljs.core.str(p1__37999_SHARP_),cljs.core.str("</div>")].join('');
});})(last_message,map__38004,map__38004__$1,exception,message,failed_loading_clj_file,reader_exception,file,column,failed_compiling,error_inline,line,class$,analysis_exception,display_ex_data))
,(cljs.core.truth_(message)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [[cljs.core.str((cljs.core.truth_(class$)?[cljs.core.str(figwheel.client.heads_up.escape.call(null,class$)),cljs.core.str(": ")].join(''):"")),cljs.core.str("<span style=\"font-weight:bold;\">"),cljs.core.str(figwheel.client.heads_up.escape.call(null,message)),cljs.core.str("</span>")].join(''),(cljs.core.truth_(display_ex_data)?[cljs.core.str("<pre>"),cljs.core.str(figwheel.client.utils.pprint_to_string.call(null,display_ex_data)),cljs.core.str("</pre>")].join(''):null),(((cljs.core.count.call(null,error_inline) > (0)))?figwheel.client.heads_up.format_inline_error.call(null,error_inline):null)], null):cljs.core.map.call(null,((function (last_message,map__38004,map__38004__$1,exception,message,failed_loading_clj_file,reader_exception,file,column,failed_compiling,error_inline,line,class$,analysis_exception,display_ex_data){
return (function (p1__38000_SHARP_){
return [cljs.core.str(figwheel.client.heads_up.escape.call(null,new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(p1__38000_SHARP_))),cljs.core.str(": "),cljs.core.str(figwheel.client.heads_up.escape.call(null,new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(p1__38000_SHARP_)))].join('');
});})(last_message,map__38004,map__38004__$1,exception,message,failed_loading_clj_file,reader_exception,file,column,failed_compiling,error_inline,line,class$,analysis_exception,display_ex_data))
,figwheel.client.heads_up.flatten_exception.call(null,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(exception))))),(cljs.core.truth_(last_message)?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [[cljs.core.str("<div style=\"color: #AD4F4F; padding-top: 3px;\">"),cljs.core.str(figwheel.client.heads_up.escape.call(null,last_message)),cljs.core.str("</div>")].join('')], null):null)),new cljs.core.Keyword(null,"file","file",-1269645878),file,new cljs.core.Keyword(null,"line","line",212345235),line,new cljs.core.Keyword(null,"column","column",2078222095),column], null);
});
figwheel.client.heads_up.auto_notify_source_file_line = (function figwheel$client$heads_up$auto_notify_source_file_line(p__38006){
var map__38009 = p__38006;
var map__38009__$1 = ((((!((map__38009 == null)))?((((map__38009.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38009.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38009):map__38009);
var file = cljs.core.get.call(null,map__38009__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38009__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38009__$1,new cljs.core.Keyword(null,"column","column",2078222095));
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"file-selected",new cljs.core.Keyword(null,"file-name","file-name",-1654217259),[cljs.core.str(file)].join(''),new cljs.core.Keyword(null,"file-line","file-line",-1228823138),[cljs.core.str(line)].join(''),new cljs.core.Keyword(null,"file-column","file-column",1543934780),[cljs.core.str(column)].join('')], null));
});
figwheel.client.heads_up.display_exception = (function figwheel$client$heads_up$display_exception(exception_data){
var map__38014 = figwheel.client.heads_up.exception__GT_display_data.call(null,exception_data);
var map__38014__$1 = ((((!((map__38014 == null)))?((((map__38014.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38014.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38014):map__38014);
var head = cljs.core.get.call(null,map__38014__$1,new cljs.core.Keyword(null,"head","head",-771383919));
var sub_head = cljs.core.get.call(null,map__38014__$1,new cljs.core.Keyword(null,"sub-head","sub-head",1930649117));
var messages = cljs.core.get.call(null,map__38014__$1,new cljs.core.Keyword(null,"messages","messages",345434482));
var last_message = cljs.core.get.call(null,map__38014__$1,new cljs.core.Keyword(null,"last-message","last-message",-2087778135));
var file = cljs.core.get.call(null,map__38014__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38014__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38014__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var msg = cljs.core.apply.call(null,cljs.core.str,messages);
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(255, 161, 161, 0.95)"], null),[cljs.core.str(figwheel.client.heads_up.close_link.call(null)),cljs.core.str(figwheel.client.heads_up.heading.call(null,head,sub_head)),cljs.core.str(figwheel.client.heads_up.file_selector_div.call(null,file,line,column,msg))].join(''));
});
figwheel.client.heads_up.warning_data__GT_display_data = (function figwheel$client$heads_up$warning_data__GT_display_data(p__38017){
var map__38020 = p__38017;
var map__38020__$1 = ((((!((map__38020 == null)))?((((map__38020.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38020.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38020):map__38020);
var warning_data = map__38020__$1;
var file = cljs.core.get.call(null,map__38020__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38020__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38020__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var message = cljs.core.get.call(null,map__38020__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var error_inline = cljs.core.get.call(null,map__38020__$1,new cljs.core.Keyword(null,"error-inline","error-inline",1073987185));
var last_message = (cljs.core.truth_((function (){var and__29909__auto__ = file;
if(cljs.core.truth_(and__29909__auto__)){
return line;
} else {
return and__29909__auto__;
}
})())?[cljs.core.str("Please see line "),cljs.core.str(line),cljs.core.str(" of file "),cljs.core.str(file)].join(''):(cljs.core.truth_(file)?[cljs.core.str("Please see "),cljs.core.str(file)].join(''):null
));
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"head","head",-771383919),"Compile Warning",new cljs.core.Keyword(null,"sub-head","sub-head",1930649117),file,new cljs.core.Keyword(null,"messages","messages",345434482),cljs.core.concat.call(null,cljs.core.map.call(null,((function (last_message,map__38020,map__38020__$1,warning_data,file,line,column,message,error_inline){
return (function (p1__38016_SHARP_){
return [cljs.core.str("<div>"),cljs.core.str(p1__38016_SHARP_),cljs.core.str("</div>")].join('');
});})(last_message,map__38020,map__38020__$1,warning_data,file,line,column,message,error_inline))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(cljs.core.truth_(message)?[cljs.core.str("<span style=\"font-weight:bold;\">"),cljs.core.str(figwheel.client.heads_up.escape.call(null,message)),cljs.core.str("</span>")].join(''):null),(((cljs.core.count.call(null,error_inline) > (0)))?figwheel.client.heads_up.format_inline_error.call(null,error_inline):null)], null)),(cljs.core.truth_(last_message)?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [[cljs.core.str("<div style=\"color: #AD4F4F; padding-top: 3px; margin-bottom: 10px;\">"),cljs.core.str(figwheel.client.heads_up.escape.call(null,last_message)),cljs.core.str("</div>")].join('')], null):null)),new cljs.core.Keyword(null,"file","file",-1269645878),file,new cljs.core.Keyword(null,"line","line",212345235),line,new cljs.core.Keyword(null,"column","column",2078222095),column], null);
});
figwheel.client.heads_up.display_system_warning = (function figwheel$client$heads_up$display_system_warning(header,msg){
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(255, 220, 110, 0.95)"], null),[cljs.core.str(figwheel.client.heads_up.close_link.call(null)),cljs.core.str(figwheel.client.heads_up.heading.call(null,header)),cljs.core.str("<div>"),cljs.core.str(msg),cljs.core.str("</div>")].join(''));
});
figwheel.client.heads_up.display_warning = (function figwheel$client$heads_up$display_warning(warning_data){
var map__38024 = figwheel.client.heads_up.warning_data__GT_display_data.call(null,warning_data);
var map__38024__$1 = ((((!((map__38024 == null)))?((((map__38024.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38024.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38024):map__38024);
var head = cljs.core.get.call(null,map__38024__$1,new cljs.core.Keyword(null,"head","head",-771383919));
var sub_head = cljs.core.get.call(null,map__38024__$1,new cljs.core.Keyword(null,"sub-head","sub-head",1930649117));
var messages = cljs.core.get.call(null,map__38024__$1,new cljs.core.Keyword(null,"messages","messages",345434482));
var last_message = cljs.core.get.call(null,map__38024__$1,new cljs.core.Keyword(null,"last-message","last-message",-2087778135));
var file = cljs.core.get.call(null,map__38024__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38024__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38024__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var msg = cljs.core.apply.call(null,cljs.core.str,messages);
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(255, 220, 110, 0.95)"], null),[cljs.core.str(figwheel.client.heads_up.close_link.call(null)),cljs.core.str(figwheel.client.heads_up.heading.call(null,head,sub_head)),cljs.core.str(figwheel.client.heads_up.file_selector_div.call(null,file,line,column,msg))].join(''));
});
figwheel.client.heads_up.format_warning_message = (function figwheel$client$heads_up$format_warning_message(p__38026){
var map__38030 = p__38026;
var map__38030__$1 = ((((!((map__38030 == null)))?((((map__38030.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38030.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38030):map__38030);
var warning_data = map__38030__$1;
var message = cljs.core.get.call(null,map__38030__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var file = cljs.core.get.call(null,map__38030__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38030__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38030__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var G__38032 = message;
var G__38032__$1 = (cljs.core.truth_(line)?[cljs.core.str(G__38032),cljs.core.str(" at line "),cljs.core.str(line)].join(''):G__38032);
var G__38032__$2 = (cljs.core.truth_((function (){var and__29909__auto__ = line;
if(cljs.core.truth_(and__29909__auto__)){
return column;
} else {
return and__29909__auto__;
}
})())?[cljs.core.str(G__38032__$1),cljs.core.str(", column "),cljs.core.str(column)].join(''):G__38032__$1);
if(cljs.core.truth_(file)){
return [cljs.core.str(G__38032__$2),cljs.core.str(" in file "),cljs.core.str(file)].join('');
} else {
return G__38032__$2;
}
});
figwheel.client.heads_up.append_warning_message = (function figwheel$client$heads_up$append_warning_message(p__38033){
var map__38038 = p__38033;
var map__38038__$1 = ((((!((map__38038 == null)))?((((map__38038.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38038.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38038):map__38038);
var warning_data = map__38038__$1;
var message = cljs.core.get.call(null,map__38038__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var file = cljs.core.get.call(null,map__38038__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38038__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38038__$1,new cljs.core.Keyword(null,"column","column",2078222095));
if(cljs.core.truth_(message)){
var map__38040 = figwheel.client.heads_up.ensure_container.call(null);
var map__38040__$1 = ((((!((map__38040 == null)))?((((map__38040.cljs$lang$protocol_mask$partition0$ & (64))) || (map__38040.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38040):map__38040);
var content_area_el = cljs.core.get.call(null,map__38040__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
var el = goog.dom.createElement("div");
var child_count = goog.dom.getChildren(content_area_el).length;
if((child_count < (6))){
el.innerHTML = figwheel.client.heads_up.format_line.call(null,figwheel.client.heads_up.format_warning_message.call(null,warning_data),warning_data);

return goog.dom.append(content_area_el,el);
} else {
var temp__4657__auto__ = goog.dom.getLastElementChild(content_area_el);
if(cljs.core.truth_(temp__4657__auto__)){
var last_child = temp__4657__auto__;
var temp__4655__auto__ = goog.dom.dataset.get(last_child,"figwheel_count");
if(cljs.core.truth_(temp__4655__auto__)){
var message_count = temp__4655__auto__;
var message_count__$1 = (parseInt(message_count) + (1));
goog.dom.dataset.set(last_child,"figwheel_count",message_count__$1);

return last_child.innerHTML = [cljs.core.str(message_count__$1),cljs.core.str(" more warnings have not been displayed ...")].join('');
} else {
return goog.dom.append(content_area_el,goog.dom.createDom("div",{"data-figwheel_count": (1), "style": "margin-top: 3px; font-weight: bold"},"1 more warning that has not been displayed ..."));
}
} else {
return null;
}
}
} else {
return null;
}
});
figwheel.client.heads_up.clear = (function figwheel$client$heads_up$clear(){
var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__){
return (function (state_38088){
var state_val_38089 = (state_38088[(1)]);
if((state_val_38089 === (1))){
var inst_38071 = (state_38088[(7)]);
var inst_38071__$1 = figwheel.client.heads_up.ensure_container.call(null);
var inst_38072 = [new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_38073 = ["0.0"];
var inst_38074 = cljs.core.PersistentHashMap.fromArrays(inst_38072,inst_38073);
var inst_38075 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_38071__$1,inst_38074);
var inst_38076 = cljs.core.async.timeout.call(null,(300));
var state_38088__$1 = (function (){var statearr_38090 = state_38088;
(statearr_38090[(7)] = inst_38071__$1);

(statearr_38090[(8)] = inst_38075);

return statearr_38090;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38088__$1,(2),inst_38076);
} else {
if((state_val_38089 === (2))){
var inst_38071 = (state_38088[(7)]);
var inst_38078 = (state_38088[(2)]);
var inst_38079 = [new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"height","height",1025178622),new cljs.core.Keyword(null,"minHeight","minHeight",-1635998980),new cljs.core.Keyword(null,"padding","padding",1660304693),new cljs.core.Keyword(null,"borderRadius","borderRadius",-1505621083),new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491)];
var inst_38080 = ["auto","0px","0px","0px 10px 0px 70px","0px","transparent"];
var inst_38081 = cljs.core.PersistentHashMap.fromArrays(inst_38079,inst_38080);
var inst_38082 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_38071,inst_38081);
var inst_38083 = cljs.core.async.timeout.call(null,(200));
var state_38088__$1 = (function (){var statearr_38091 = state_38088;
(statearr_38091[(9)] = inst_38082);

(statearr_38091[(10)] = inst_38078);

return statearr_38091;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38088__$1,(3),inst_38083);
} else {
if((state_val_38089 === (3))){
var inst_38071 = (state_38088[(7)]);
var inst_38085 = (state_38088[(2)]);
var inst_38086 = figwheel.client.heads_up.set_content_BANG_.call(null,inst_38071,"");
var state_38088__$1 = (function (){var statearr_38092 = state_38088;
(statearr_38092[(11)] = inst_38085);

return statearr_38092;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38088__$1,inst_38086);
} else {
return null;
}
}
}
});})(c__32227__auto__))
;
return ((function (switch__32115__auto__,c__32227__auto__){
return (function() {
var figwheel$client$heads_up$clear_$_state_machine__32116__auto__ = null;
var figwheel$client$heads_up$clear_$_state_machine__32116__auto____0 = (function (){
var statearr_38096 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_38096[(0)] = figwheel$client$heads_up$clear_$_state_machine__32116__auto__);

(statearr_38096[(1)] = (1));

return statearr_38096;
});
var figwheel$client$heads_up$clear_$_state_machine__32116__auto____1 = (function (state_38088){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_38088);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e38097){if((e38097 instanceof Object)){
var ex__32119__auto__ = e38097;
var statearr_38098_38100 = state_38088;
(statearr_38098_38100[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38088);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38097;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38101 = state_38088;
state_38088 = G__38101;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$heads_up$clear_$_state_machine__32116__auto__ = function(state_38088){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$clear_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$heads_up$clear_$_state_machine__32116__auto____1.call(this,state_38088);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$clear_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$clear_$_state_machine__32116__auto____0;
figwheel$client$heads_up$clear_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$clear_$_state_machine__32116__auto____1;
return figwheel$client$heads_up$clear_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__))
})();
var state__32229__auto__ = (function (){var statearr_38099 = f__32228__auto__.call(null);
(statearr_38099[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_38099;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__))
);

return c__32227__auto__;
});
figwheel.client.heads_up.display_loaded_start = (function figwheel$client$heads_up$display_loaded_start(){
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(211,234,172,1.0)",new cljs.core.Keyword(null,"width","width",-384071477),"68px",new cljs.core.Keyword(null,"height","height",1025178622),"68px",new cljs.core.Keyword(null,"paddingLeft","paddingLeft",262720813),"0px",new cljs.core.Keyword(null,"paddingRight","paddingRight",-1642313463),"0px",new cljs.core.Keyword(null,"borderRadius","borderRadius",-1505621083),"35px"], null),"");
});
figwheel.client.heads_up.flash_loaded = (function figwheel$client$heads_up$flash_loaded(){
var c__32227__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__32227__auto__){
return (function (){
var f__32228__auto__ = (function (){var switch__32115__auto__ = ((function (c__32227__auto__){
return (function (state_38133){
var state_val_38134 = (state_38133[(1)]);
if((state_val_38134 === (1))){
var inst_38123 = figwheel.client.heads_up.display_loaded_start.call(null);
var state_38133__$1 = state_38133;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38133__$1,(2),inst_38123);
} else {
if((state_val_38134 === (2))){
var inst_38125 = (state_38133[(2)]);
var inst_38126 = cljs.core.async.timeout.call(null,(400));
var state_38133__$1 = (function (){var statearr_38135 = state_38133;
(statearr_38135[(7)] = inst_38125);

return statearr_38135;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38133__$1,(3),inst_38126);
} else {
if((state_val_38134 === (3))){
var inst_38128 = (state_38133[(2)]);
var inst_38129 = figwheel.client.heads_up.clear.call(null);
var state_38133__$1 = (function (){var statearr_38136 = state_38133;
(statearr_38136[(8)] = inst_38128);

return statearr_38136;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38133__$1,(4),inst_38129);
} else {
if((state_val_38134 === (4))){
var inst_38131 = (state_38133[(2)]);
var state_38133__$1 = state_38133;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38133__$1,inst_38131);
} else {
return null;
}
}
}
}
});})(c__32227__auto__))
;
return ((function (switch__32115__auto__,c__32227__auto__){
return (function() {
var figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto__ = null;
var figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto____0 = (function (){
var statearr_38140 = [null,null,null,null,null,null,null,null,null];
(statearr_38140[(0)] = figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto__);

(statearr_38140[(1)] = (1));

return statearr_38140;
});
var figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto____1 = (function (state_38133){
while(true){
var ret_value__32117__auto__ = (function (){try{while(true){
var result__32118__auto__ = switch__32115__auto__.call(null,state_38133);
if(cljs.core.keyword_identical_QMARK_.call(null,result__32118__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__32118__auto__;
}
break;
}
}catch (e38141){if((e38141 instanceof Object)){
var ex__32119__auto__ = e38141;
var statearr_38142_38144 = state_38133;
(statearr_38142_38144[(5)] = ex__32119__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38133);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38141;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__32117__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38145 = state_38133;
state_38133 = G__38145;
continue;
} else {
return ret_value__32117__auto__;
}
break;
}
});
figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto__ = function(state_38133){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto____0.call(this);
case 1:
return figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto____1.call(this,state_38133);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto____0;
figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto____1;
return figwheel$client$heads_up$flash_loaded_$_state_machine__32116__auto__;
})()
;})(switch__32115__auto__,c__32227__auto__))
})();
var state__32229__auto__ = (function (){var statearr_38143 = f__32228__auto__.call(null);
(statearr_38143[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__32227__auto__);

return statearr_38143;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__32229__auto__);
});})(c__32227__auto__))
);

return c__32227__auto__;
});
figwheel.client.heads_up.cljs_logo_svg = "<?xml version='1.0' encoding='utf-8'?>\n<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>\n<svg width='49px' height='49px' style='position:absolute; top:9px; left: 10px;' version='1.1'\n  xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'\n  viewBox='0 0 428 428' enable-background='new 0 0 428 428' xml:space='preserve'>\n<circle fill='#fff' cx='213' cy='214' r='213' />\n<g>\n<path fill='#96CA4B' d='M122,266.6c-12.7,0-22.3-3.7-28.9-11.1c-6.6-7.4-9.9-18-9.9-31.8c0-14.1,3.4-24.9,10.3-32.5\n  s16.8-11.4,29.9-11.4c8.8,0,16.8,1.6,23.8,4.9l-5.4,14.3c-7.5-2.9-13.7-4.4-18.6-4.4c-14.5,0-21.7,9.6-21.7,28.8\n  c0,9.4,1.8,16.4,5.4,21.2c3.6,4.7,8.9,7.1,15.9,7.1c7.9,0,15.4-2,22.5-5.9v15.5c-3.2,1.9-6.6,3.2-10.2,4\n  C131.5,266.2,127.1,266.6,122,266.6z'/>\n<path fill='#96CA4B' d='M194.4,265.1h-17.8V147.3h17.8V265.1z'/>\n<path fill='#5F7FBF' d='M222.9,302.3c-5.3,0-9.8-0.6-13.3-1.9v-14.1c3.4,0.9,6.9,1.4,10.5,1.4c7.6,0,11.4-4.3,11.4-12.9v-93.5h17.8\n  v94.7c0,8.6-2.3,15.2-6.8,19.6C237.9,300.1,231.4,302.3,222.9,302.3z M230.4,159.2c0-3.2,0.9-5.6,2.6-7.3c1.7-1.7,4.2-2.6,7.5-2.6\n  c3.1,0,5.6,0.9,7.3,2.6c1.7,1.7,2.6,4.2,2.6,7.3c0,3-0.9,5.4-2.6,7.2c-1.7,1.7-4.2,2.6-7.3,2.6c-3.2,0-5.7-0.9-7.5-2.6\n  C231.2,164.6,230.4,162.2,230.4,159.2z'/>\n<path fill='#5F7FBF' d='M342.5,241.3c0,8.2-3,14.4-8.9,18.8c-6,4.4-14.5,6.5-25.6,6.5c-11.2,0-20.1-1.7-26.9-5.1v-15.4\n  c9.8,4.5,19,6.8,27.5,6.8c10.9,0,16.4-3.3,16.4-9.9c0-2.1-0.6-3.9-1.8-5.3c-1.2-1.4-3.2-2.9-6-4.4c-2.8-1.5-6.6-3.2-11.6-5.1\n  c-9.6-3.7-16.2-7.5-19.6-11.2c-3.4-3.7-5.1-8.6-5.1-14.5c0-7.2,2.9-12.7,8.7-16.7c5.8-4,13.6-5.9,23.6-5.9c9.8,0,19.1,2,27.9,6\n  l-5.8,13.4c-9-3.7-16.6-5.6-22.8-5.6c-9.4,0-14.1,2.7-14.1,8c0,2.6,1.2,4.8,3.7,6.7c2.4,1.8,7.8,4.3,16,7.5\n  c6.9,2.7,11.9,5.1,15.1,7.3c3.1,2.2,5.4,4.8,7,7.7C341.7,233.7,342.5,237.2,342.5,241.3z'/>\n</g>\n<path fill='#96CA4B' stroke='#96CA4B' stroke-width='6' stroke-miterlimit='10' d='M197,392.7c-91.2-8.1-163-85-163-178.3\n  S105.8,44.3,197,36.2V16.1c-102.3,8.2-183,94-183,198.4s80.7,190.2,183,198.4V392.7z'/>\n<path fill='#5F7FBF' stroke='#5F7FBF' stroke-width='6' stroke-miterlimit='10' d='M229,16.1v20.1c91.2,8.1,163,85,163,178.3\n  s-71.8,170.2-163,178.3v20.1c102.3-8.2,183-94,183-198.4S331.3,24.3,229,16.1z'/>\n</svg>";
figwheel.client.heads_up.close_bad_compile_screen = (function figwheel$client$heads_up$close_bad_compile_screen(){
var temp__4657__auto__ = document.getElementById("figwheelFailScreen");
if(cljs.core.truth_(temp__4657__auto__)){
var el = temp__4657__auto__;
return goog.dom.removeNode(el);
} else {
return null;
}
});
figwheel.client.heads_up.bad_compile_screen = (function figwheel$client$heads_up$bad_compile_screen(){
var body = (goog.dom.getElementsByTagNameAndClass("body")[(0)]);
figwheel.client.heads_up.close_bad_compile_screen.call(null);

return goog.dom.append(body,goog.dom.createDom("div",{"id": "figwheelFailScreen", "style": [cljs.core.str("background-color: rgba(24, 26, 38, 0.95);"),cljs.core.str("position: absolute;"),cljs.core.str("z-index: 9000;"),cljs.core.str("width: 100vw;"),cljs.core.str("height: 100vh;"),cljs.core.str("top: 0px; left: 0px;"),cljs.core.str("font-family: monospace")].join('')},goog.dom.createDom("div",{"class": "message", "style": [cljs.core.str("color: #FFF5DB;"),cljs.core.str("width: 100vw;"),cljs.core.str("margin: auto;"),cljs.core.str("margin-top: 10px;"),cljs.core.str("text-align: center; "),cljs.core.str("padding: 2px 0px;"),cljs.core.str("font-size: 13px;"),cljs.core.str("position: relative")].join('')},goog.dom.createDom("a",{"onclick": ((function (body){
return (function (e){
e.preventDefault();

return figwheel.client.heads_up.close_bad_compile_screen.call(null);
});})(body))
, "href": "javascript:", "style": "position: absolute; right: 10px; top: 10px; color: #666"},"X"),goog.dom.createDom("h2",{"style": "color: #FFF5DB"},"Figwheel Says: Your code didn't compile."),goog.dom.createDom("div",{"style": "font-size: 12px"},goog.dom.createDom("p",{"style": "color: #D07D7D;"},"Keep trying. This page will auto-refresh when your code compiles successfully.")))));
});

//# sourceMappingURL=heads_up.js.map?rel=1494229528396