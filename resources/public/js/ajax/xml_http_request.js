// Compiled by ClojureScript 1.9.89 {}
goog.provide('ajax.xml_http_request');
goog.require('cljs.core');
goog.require('ajax.protocols');
ajax.xml_http_request.ready_state = (function ajax$xml_http_request$ready_state(e){
return new cljs.core.PersistentArrayMap(null, 5, [(0),new cljs.core.Keyword(null,"not-initialized","not-initialized",-1937378906),(1),new cljs.core.Keyword(null,"connection-established","connection-established",-1403749733),(2),new cljs.core.Keyword(null,"request-received","request-received",2110590540),(3),new cljs.core.Keyword(null,"processing-request","processing-request",-264947221),(4),new cljs.core.Keyword(null,"response-ready","response-ready",245208276)], null).call(null,e.target.readyState);
});
XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3 = (function (this$,p__12445,handler){
var map__12446 = p__12445;
var map__12446__$1 = ((((!((map__12446 == null)))?((((map__12446.cljs$lang$protocol_mask$partition0$ & (64))) || (map__12446.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__12446):map__12446);
var uri = cljs.core.get.call(null,map__12446__$1,new cljs.core.Keyword(null,"uri","uri",-774711847));
var method = cljs.core.get.call(null,map__12446__$1,new cljs.core.Keyword(null,"method","method",55703592));
var body = cljs.core.get.call(null,map__12446__$1,new cljs.core.Keyword(null,"body","body",-2049205669));
var headers = cljs.core.get.call(null,map__12446__$1,new cljs.core.Keyword(null,"headers","headers",-835030129));
var timeout = cljs.core.get.call(null,map__12446__$1,new cljs.core.Keyword(null,"timeout","timeout",-318625318),(0));
var with_credentials = cljs.core.get.call(null,map__12446__$1,new cljs.core.Keyword(null,"with-credentials","with-credentials",-1163127235),false);
var response_format = cljs.core.get.call(null,map__12446__$1,new cljs.core.Keyword(null,"response-format","response-format",1664465322));
var this$__$1 = this;
this$__$1.withCredentials = with_credentials;

this$__$1.onreadystatechange = ((function (this$__$1,map__12446,map__12446__$1,uri,method,body,headers,timeout,with_credentials,response_format){
return (function (p1__12444_SHARP_){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"response-ready","response-ready",245208276),ajax.xml_http_request.ready_state.call(null,p1__12444_SHARP_))){
return handler.call(null,this$__$1);
} else {
return null;
}
});})(this$__$1,map__12446,map__12446__$1,uri,method,body,headers,timeout,with_credentials,response_format))
;

this$__$1.open(method,uri,true);

this$__$1.timeout = timeout;

var temp__4657__auto___12458 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(response_format);
if(cljs.core.truth_(temp__4657__auto___12458)){
var response_type_12459 = temp__4657__auto___12458;
this$__$1.responseType = cljs.core.name.call(null,response_type_12459);
} else {
}

var seq__12448_12460 = cljs.core.seq.call(null,headers);
var chunk__12449_12461 = null;
var count__12450_12462 = (0);
var i__12451_12463 = (0);
while(true){
if((i__12451_12463 < count__12450_12462)){
var vec__12452_12464 = cljs.core._nth.call(null,chunk__12449_12461,i__12451_12463);
var k_12465 = cljs.core.nth.call(null,vec__12452_12464,(0),null);
var v_12466 = cljs.core.nth.call(null,vec__12452_12464,(1),null);
this$__$1.setRequestHeader(k_12465,v_12466);

var G__12467 = seq__12448_12460;
var G__12468 = chunk__12449_12461;
var G__12469 = count__12450_12462;
var G__12470 = (i__12451_12463 + (1));
seq__12448_12460 = G__12467;
chunk__12449_12461 = G__12468;
count__12450_12462 = G__12469;
i__12451_12463 = G__12470;
continue;
} else {
var temp__4657__auto___12471 = cljs.core.seq.call(null,seq__12448_12460);
if(temp__4657__auto___12471){
var seq__12448_12472__$1 = temp__4657__auto___12471;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12448_12472__$1)){
var c__6413__auto___12473 = cljs.core.chunk_first.call(null,seq__12448_12472__$1);
var G__12474 = cljs.core.chunk_rest.call(null,seq__12448_12472__$1);
var G__12475 = c__6413__auto___12473;
var G__12476 = cljs.core.count.call(null,c__6413__auto___12473);
var G__12477 = (0);
seq__12448_12460 = G__12474;
chunk__12449_12461 = G__12475;
count__12450_12462 = G__12476;
i__12451_12463 = G__12477;
continue;
} else {
var vec__12455_12478 = cljs.core.first.call(null,seq__12448_12472__$1);
var k_12479 = cljs.core.nth.call(null,vec__12455_12478,(0),null);
var v_12480 = cljs.core.nth.call(null,vec__12455_12478,(1),null);
this$__$1.setRequestHeader(k_12479,v_12480);

var G__12481 = cljs.core.next.call(null,seq__12448_12472__$1);
var G__12482 = null;
var G__12483 = (0);
var G__12484 = (0);
seq__12448_12460 = G__12481;
chunk__12449_12461 = G__12482;
count__12450_12462 = G__12483;
i__12451_12463 = G__12484;
continue;
}
} else {
}
}
break;
}

this$__$1.send((function (){var or__5602__auto__ = body;
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
return "";
}
})());

return this$__$1;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$_abort$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.abort();
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_body$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.response;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.status;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status_text$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.statusText;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_get_response_header$arity$2 = (function (this$,header){
var this$__$1 = this;
return this$__$1.getResponseHeader(header);
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_was_aborted$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core._EQ_.call(null,(0),this$__$1.readyState);
});

//# sourceMappingURL=xml_http_request.js.map