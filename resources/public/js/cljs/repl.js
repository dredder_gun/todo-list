// Compiled by ClojureScript 1.9.89 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__37709){
var map__37734 = p__37709;
var map__37734__$1 = ((((!((map__37734 == null)))?((((map__37734.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37734.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37734):map__37734);
var m = map__37734__$1;
var n = cljs.core.get.call(null,map__37734__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__37734__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4657__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4657__auto__)){
var ns = temp__4657__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__37736_37758 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__37737_37759 = null;
var count__37738_37760 = (0);
var i__37739_37761 = (0);
while(true){
if((i__37739_37761 < count__37738_37760)){
var f_37762 = cljs.core._nth.call(null,chunk__37737_37759,i__37739_37761);
cljs.core.println.call(null,"  ",f_37762);

var G__37763 = seq__37736_37758;
var G__37764 = chunk__37737_37759;
var G__37765 = count__37738_37760;
var G__37766 = (i__37739_37761 + (1));
seq__37736_37758 = G__37763;
chunk__37737_37759 = G__37764;
count__37738_37760 = G__37765;
i__37739_37761 = G__37766;
continue;
} else {
var temp__4657__auto___37767 = cljs.core.seq.call(null,seq__37736_37758);
if(temp__4657__auto___37767){
var seq__37736_37768__$1 = temp__4657__auto___37767;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37736_37768__$1)){
var c__30732__auto___37769 = cljs.core.chunk_first.call(null,seq__37736_37768__$1);
var G__37770 = cljs.core.chunk_rest.call(null,seq__37736_37768__$1);
var G__37771 = c__30732__auto___37769;
var G__37772 = cljs.core.count.call(null,c__30732__auto___37769);
var G__37773 = (0);
seq__37736_37758 = G__37770;
chunk__37737_37759 = G__37771;
count__37738_37760 = G__37772;
i__37739_37761 = G__37773;
continue;
} else {
var f_37774 = cljs.core.first.call(null,seq__37736_37768__$1);
cljs.core.println.call(null,"  ",f_37774);

var G__37775 = cljs.core.next.call(null,seq__37736_37768__$1);
var G__37776 = null;
var G__37777 = (0);
var G__37778 = (0);
seq__37736_37758 = G__37775;
chunk__37737_37759 = G__37776;
count__37738_37760 = G__37777;
i__37739_37761 = G__37778;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_37779 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__29921__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__29921__auto__)){
return or__29921__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_37779);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_37779)))?cljs.core.second.call(null,arglists_37779):arglists_37779));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__37740_37780 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__37741_37781 = null;
var count__37742_37782 = (0);
var i__37743_37783 = (0);
while(true){
if((i__37743_37783 < count__37742_37782)){
var vec__37744_37784 = cljs.core._nth.call(null,chunk__37741_37781,i__37743_37783);
var name_37785 = cljs.core.nth.call(null,vec__37744_37784,(0),null);
var map__37747_37786 = cljs.core.nth.call(null,vec__37744_37784,(1),null);
var map__37747_37787__$1 = ((((!((map__37747_37786 == null)))?((((map__37747_37786.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37747_37786.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37747_37786):map__37747_37786);
var doc_37788 = cljs.core.get.call(null,map__37747_37787__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_37789 = cljs.core.get.call(null,map__37747_37787__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_37785);

cljs.core.println.call(null," ",arglists_37789);

if(cljs.core.truth_(doc_37788)){
cljs.core.println.call(null," ",doc_37788);
} else {
}

var G__37790 = seq__37740_37780;
var G__37791 = chunk__37741_37781;
var G__37792 = count__37742_37782;
var G__37793 = (i__37743_37783 + (1));
seq__37740_37780 = G__37790;
chunk__37741_37781 = G__37791;
count__37742_37782 = G__37792;
i__37743_37783 = G__37793;
continue;
} else {
var temp__4657__auto___37794 = cljs.core.seq.call(null,seq__37740_37780);
if(temp__4657__auto___37794){
var seq__37740_37795__$1 = temp__4657__auto___37794;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37740_37795__$1)){
var c__30732__auto___37796 = cljs.core.chunk_first.call(null,seq__37740_37795__$1);
var G__37797 = cljs.core.chunk_rest.call(null,seq__37740_37795__$1);
var G__37798 = c__30732__auto___37796;
var G__37799 = cljs.core.count.call(null,c__30732__auto___37796);
var G__37800 = (0);
seq__37740_37780 = G__37797;
chunk__37741_37781 = G__37798;
count__37742_37782 = G__37799;
i__37743_37783 = G__37800;
continue;
} else {
var vec__37749_37801 = cljs.core.first.call(null,seq__37740_37795__$1);
var name_37802 = cljs.core.nth.call(null,vec__37749_37801,(0),null);
var map__37752_37803 = cljs.core.nth.call(null,vec__37749_37801,(1),null);
var map__37752_37804__$1 = ((((!((map__37752_37803 == null)))?((((map__37752_37803.cljs$lang$protocol_mask$partition0$ & (64))) || (map__37752_37803.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37752_37803):map__37752_37803);
var doc_37805 = cljs.core.get.call(null,map__37752_37804__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_37806 = cljs.core.get.call(null,map__37752_37804__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_37802);

cljs.core.println.call(null," ",arglists_37806);

if(cljs.core.truth_(doc_37805)){
cljs.core.println.call(null," ",doc_37805);
} else {
}

var G__37807 = cljs.core.next.call(null,seq__37740_37795__$1);
var G__37808 = null;
var G__37809 = (0);
var G__37810 = (0);
seq__37740_37780 = G__37807;
chunk__37741_37781 = G__37808;
count__37742_37782 = G__37809;
i__37743_37783 = G__37810;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__4657__auto__ = cljs.spec.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__4657__auto__)){
var fnspec = temp__4657__auto__;
cljs.core.print.call(null,"Spec");

var seq__37754 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__37755 = null;
var count__37756 = (0);
var i__37757 = (0);
while(true){
if((i__37757 < count__37756)){
var role = cljs.core._nth.call(null,chunk__37755,i__37757);
var temp__4657__auto___37811__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__4657__auto___37811__$1)){
var spec_37812 = temp__4657__auto___37811__$1;
cljs.core.print.call(null,[cljs.core.str("\n "),cljs.core.str(cljs.core.name.call(null,role)),cljs.core.str(":")].join(''),cljs.spec.describe.call(null,spec_37812));
} else {
}

var G__37813 = seq__37754;
var G__37814 = chunk__37755;
var G__37815 = count__37756;
var G__37816 = (i__37757 + (1));
seq__37754 = G__37813;
chunk__37755 = G__37814;
count__37756 = G__37815;
i__37757 = G__37816;
continue;
} else {
var temp__4657__auto____$1 = cljs.core.seq.call(null,seq__37754);
if(temp__4657__auto____$1){
var seq__37754__$1 = temp__4657__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37754__$1)){
var c__30732__auto__ = cljs.core.chunk_first.call(null,seq__37754__$1);
var G__37817 = cljs.core.chunk_rest.call(null,seq__37754__$1);
var G__37818 = c__30732__auto__;
var G__37819 = cljs.core.count.call(null,c__30732__auto__);
var G__37820 = (0);
seq__37754 = G__37817;
chunk__37755 = G__37818;
count__37756 = G__37819;
i__37757 = G__37820;
continue;
} else {
var role = cljs.core.first.call(null,seq__37754__$1);
var temp__4657__auto___37821__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__4657__auto___37821__$2)){
var spec_37822 = temp__4657__auto___37821__$2;
cljs.core.print.call(null,[cljs.core.str("\n "),cljs.core.str(cljs.core.name.call(null,role)),cljs.core.str(":")].join(''),cljs.spec.describe.call(null,spec_37822));
} else {
}

var G__37823 = cljs.core.next.call(null,seq__37754__$1);
var G__37824 = null;
var G__37825 = (0);
var G__37826 = (0);
seq__37754 = G__37823;
chunk__37755 = G__37824;
count__37756 = G__37825;
i__37757 = G__37826;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map?rel=1494229528101