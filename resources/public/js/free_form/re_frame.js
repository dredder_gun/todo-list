// Compiled by ClojureScript 1.9.89 {}
goog.provide('free_form.re_frame');
goog.require('cljs.core');
goog.require('free_form.core');
goog.require('re_frame.core');
free_form.re_frame.form = (function free_form$re_frame$form(var_args){
var args__6684__auto__ = [];
var len__6677__auto___19873 = arguments.length;
var i__6678__auto___19874 = (0);
while(true){
if((i__6678__auto___19874 < len__6677__auto___19873)){
args__6684__auto__.push((arguments[i__6678__auto___19874]));

var G__19875 = (i__6678__auto___19874 + (1));
i__6678__auto___19874 = G__19875;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return free_form.re_frame.form.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

free_form.re_frame.form.cljs$core$IFn$_invoke$arity$variadic = (function (args){
var event = cljs.core.nth.call(null,args,(2));
var re_frame_event_generator = ((function (event){
return (function (keys,value){
var event_v = ((cljs.core.fn_QMARK_.call(null,event))?event.call(null,keys,value):((cljs.core.vector_QMARK_.call(null,event))?cljs.core.conj.call(null,event,keys,value):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [event,keys,value], null)
));
return re_frame.core.dispatch.call(null,event_v);
});})(event))
;
var args__$1 = cljs.core.assoc.call(null,cljs.core.vec.call(null,args),(2),re_frame_event_generator);
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [free_form.core.form], null),args__$1);
});

free_form.re_frame.form.cljs$lang$maxFixedArity = (0);

free_form.re_frame.form.cljs$lang$applyTo = (function (seq19872){
return free_form.re_frame.form.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq19872));
});


//# sourceMappingURL=re_frame.js.map