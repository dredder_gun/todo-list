// Compiled by ClojureScript 1.9.89 {}
goog.provide('free_form.core');
goog.require('cljs.core');
goog.require('free_form.bootstrap_3');
goog.require('free_form.debug');
goog.require('clojure.string');
goog.require('clojure.walk');
goog.require('free_form.extension');
goog.require('free_form.util');
free_form.core.extract_attributes = (function free_form$core$extract_attributes(node,key){
var attributes = cljs.core.get.call(null,node,free_form.util.attributes_index);
var re_attributes = key.call(null,attributes);
var attributes__$1 = cljs.core.dissoc.call(null,attributes,key);
var keys = (function (){var or__5602__auto__ = new cljs.core.Keyword(null,"keys","keys",1068423698).cljs$core$IFn$_invoke$arity$1(re_attributes);
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(re_attributes)], null);
}
})();
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [attributes__$1,re_attributes,keys], null);
});
free_form.core.input_QMARK_ = (function free_form$core$input_QMARK_(node){
return (cljs.core.coll_QMARK_.call(null,node)) && (cljs.core.contains_QMARK_.call(null,cljs.core.second.call(null,node),new cljs.core.Keyword("free-form","input","free-form/input",228451328)));
});
free_form.core.js_event_value = (function free_form$core$js_event_value(event){
return event.target.value;
});
free_form.core.bind_input = (function free_form$core$bind_input(values,on_change,node){
if(cljs.core.not.call(null,free_form.core.input_QMARK_.call(null,node))){
return node;
} else {
var vec__19065 = free_form.core.extract_attributes.call(null,node,new cljs.core.Keyword("free-form","input","free-form/input",228451328));
var attributes = cljs.core.nth.call(null,vec__19065,(0),null);
var _ = cljs.core.nth.call(null,vec__19065,(1),null);
var keys = cljs.core.nth.call(null,vec__19065,(2),null);
return cljs.core.assoc.call(null,node,free_form.util.attributes_index,cljs.core.assoc.call(null,attributes,new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__5602__auto__ = cljs.core.get_in.call(null,values,keys);
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
return "";
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (vec__19065,attributes,_,keys){
return (function (p1__19061_SHARP_){
return on_change.call(null,keys,free_form.core.js_event_value.call(null,p1__19061_SHARP_));
});})(vec__19065,attributes,_,keys))
));
}
});
/**
 * Tests whether the node should be marked with an error class should the field have an associated error.
 */
free_form.core.error_class_QMARK_ = (function free_form$core$error_class_QMARK_(node){
return (cljs.core.coll_QMARK_.call(null,node)) && (cljs.core.contains_QMARK_.call(null,cljs.core.second.call(null,node),new cljs.core.Keyword("free-form","error-class","free-form/error-class",159754118)));
});
free_form.core.bind_error_class = (function free_form$core$bind_error_class(errors,node){
if(cljs.core.not.call(null,free_form.core.error_class_QMARK_.call(null,node))){
return node;
} else {
var vec__19073 = free_form.core.extract_attributes.call(null,node,new cljs.core.Keyword("free-form","error-class","free-form/error-class",159754118));
var attributes = cljs.core.nth.call(null,vec__19073,(0),null);
var re_attributes = cljs.core.nth.call(null,vec__19073,(1),null);
var keys = cljs.core.nth.call(null,vec__19073,(2),null);
return cljs.core.assoc.call(null,node,free_form.util.attributes_index,((cljs.core.not_any_QMARK_.call(null,((function (vec__19073,attributes,re_attributes,keys){
return (function (p1__19068_SHARP_){
return cljs.core.get_in.call(null,errors,p1__19068_SHARP_);
});})(vec__19073,attributes,re_attributes,keys))
,cljs.core.conj.call(null,new cljs.core.Keyword(null,"extra-keys","extra-keys",-1845607319).cljs$core$IFn$_invoke$arity$1(re_attributes),keys)))?attributes:cljs.core.update.call(null,attributes,new cljs.core.Keyword(null,"class","class",-2030961996),((function (vec__19073,attributes,re_attributes,keys){
return (function (p1__19069_SHARP_){
return [cljs.core.str((function (){var or__5602__auto__ = new cljs.core.Keyword(null,"error","error",-978969032).cljs$core$IFn$_invoke$arity$1(re_attributes);
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
return "error";
}
})()),cljs.core.str(p1__19069_SHARP_)].join('');
});})(vec__19073,attributes,re_attributes,keys))
)));
}
});
free_form.core.error_messages_QMARK_ = (function free_form$core$error_messages_QMARK_(node){
return (cljs.core.coll_QMARK_.call(null,node)) && (cljs.core.contains_QMARK_.call(null,cljs.core.second.call(null,node),new cljs.core.Keyword("free-form","error-message","free-form/error-message",-1957745210)));
});
free_form.core.bind_error_messages = (function free_form$core$bind_error_messages(errors,node){
if(cljs.core.not.call(null,free_form.core.error_messages_QMARK_.call(null,node))){
return node;
} else {
var vec__19080 = free_form.core.extract_attributes.call(null,node,new cljs.core.Keyword("free-form","error-message","free-form/error-message",-1957745210));
var attributes = cljs.core.nth.call(null,vec__19080,(0),null);
var _ = cljs.core.nth.call(null,vec__19080,(1),null);
var keys = cljs.core.nth.call(null,vec__19080,(2),null);
var temp__4655__auto__ = cljs.core.get_in.call(null,errors,keys);
if(cljs.core.truth_(temp__4655__auto__)){
var errors__$1 = temp__4655__auto__;
return cljs.core.vec.call(null,cljs.core.concat.call(null,cljs.core.drop_last.call(null,cljs.core.assoc.call(null,node,free_form.util.attributes_index,attributes)),cljs.core.map.call(null,((function (errors__$1,temp__4655__auto__,vec__19080,attributes,_,keys){
return (function (p1__19076_SHARP_){
return cljs.core.conj.call(null,cljs.core.get.call(null,node,(2)),p1__19076_SHARP_);
});})(errors__$1,temp__4655__auto__,vec__19080,attributes,_,keys))
,errors__$1)));
} else {
return null;
}
}
});
free_form.core.warn_of_leftovers = (function free_form$core$warn_of_leftovers(node){
var attrs_19084 = cljs.core.get.call(null,node,free_form.util.attributes_index);
if(cljs.core.truth_((function (){var and__5590__auto__ = cljs.core.map_QMARK_.call(null,attrs_19084);
if(and__5590__auto__){
return cljs.core.some.call(null,((function (and__5590__auto__,attrs_19084){
return (function (p1__19083_SHARP_){
return cljs.core._EQ_.call(null,"free-form",cljs.core.namespace.call(null,p1__19083_SHARP_));
});})(and__5590__auto__,attrs_19084))
,cljs.core.keys.call(null,attrs_19084));
} else {
return and__5590__auto__;
}
})())){
console.error("There are free-form-looking leftovers on",cljs.core.pr_str.call(null,node));
} else {
}

return node;
});
free_form.core.form = (function free_form$core$form(var_args){
var args19091 = [];
var len__6677__auto___19094 = arguments.length;
var i__6678__auto___19095 = (0);
while(true){
if((i__6678__auto___19095 < len__6677__auto___19094)){
args19091.push((arguments[i__6678__auto___19095]));

var G__19096 = (i__6678__auto___19095 + (1));
i__6678__auto___19095 = G__19096;
continue;
} else {
}
break;
}

var G__19093 = args19091.length;
switch (G__19093) {
case 4:
return free_form.core.form.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return free_form.core.form.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args19091.length)].join('')));

}
});

free_form.core.form.cljs$core$IFn$_invoke$arity$4 = (function (values,errors,on_change,html){
return free_form.core.form.call(null,values,errors,on_change,cljs.core.PersistentVector.EMPTY,html);
});

free_form.core.form.cljs$core$IFn$_invoke$arity$5 = (function (values,errors,on_change,extensions,html){
var errors__$1 = (function (){var or__5602__auto__ = errors;
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
})();
var extensions__$1 = ((cljs.core.sequential_QMARK_.call(null,extensions))?extensions:new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [extensions], null));
var inner_fn = ((function (errors__$1,extensions__$1){
return (function (html__$1){
return clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1){
return (function (p1__19087_SHARP_){
return free_form.core.bind_error_messages.call(null,errors__$1,p1__19087_SHARP_);
});})(errors__$1,extensions__$1))
,clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1){
return (function (p1__19086_SHARP_){
return free_form.core.bind_error_class.call(null,errors__$1,p1__19086_SHARP_);
});})(errors__$1,extensions__$1))
,clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1){
return (function (p1__19085_SHARP_){
return free_form.core.bind_input.call(null,values,on_change,p1__19085_SHARP_);
});})(errors__$1,extensions__$1))
,html__$1)));
});})(errors__$1,extensions__$1))
;
return clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1,inner_fn){
return (function (p1__19088_SHARP_){
return free_form.core.warn_of_leftovers.call(null,p1__19088_SHARP_);
});})(errors__$1,extensions__$1,inner_fn))
,cljs.core.reduce.call(null,((function (errors__$1,extensions__$1,inner_fn){
return (function (p1__19090_SHARP_,p2__19089_SHARP_){
return free_form.extension.extension.call(null,p2__19089_SHARP_,p1__19090_SHARP_);
});})(errors__$1,extensions__$1,inner_fn))
,inner_fn,extensions__$1).call(null,html));
});

free_form.core.form.cljs$lang$maxFixedArity = 5;


//# sourceMappingURL=core.js.map