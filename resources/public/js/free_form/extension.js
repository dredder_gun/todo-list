// Compiled by ClojureScript 1.9.89 {}
goog.provide('free_form.extension');
goog.require('cljs.core');
if(typeof free_form.extension.extension !== 'undefined'){
} else {
free_form.extension.extension = (function (){var method_table__6527__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__6528__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__6529__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__6530__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__6531__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"free-form.extension","extension"),((function (method_table__6527__auto__,prefer_table__6528__auto__,method_cache__6529__auto__,cached_hierarchy__6530__auto__,hierarchy__6531__auto__){
return (function (extension_name,_inner_fn){
return extension_name;
});})(method_table__6527__auto__,prefer_table__6528__auto__,method_cache__6529__auto__,cached_hierarchy__6530__auto__,hierarchy__6531__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__6531__auto__,method_table__6527__auto__,prefer_table__6528__auto__,method_cache__6529__auto__,cached_hierarchy__6530__auto__));
})();
}

//# sourceMappingURL=extension.js.map