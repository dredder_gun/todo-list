// Compiled by ClojureScript 1.9.89 {}
goog.provide('devtools.formatters.templating');
goog.require('cljs.core');
goog.require('cljs.pprint');
goog.require('clojure.walk');
goog.require('devtools.util');
goog.require('devtools.protocols');
goog.require('devtools.formatters.helpers');
goog.require('devtools.formatters.state');
goog.require('clojure.string');
devtools.formatters.templating.mark_as_group_BANG_ = (function devtools$formatters$templating$mark_as_group_BANG_(value){
var x11966_11967 = value;
x11966_11967.devtools$protocols$IGroup$ = true;


return value;
});
devtools.formatters.templating.group_QMARK_ = (function devtools$formatters$templating$group_QMARK_(value){
if(!((value == null))){
if((false) || (value.devtools$protocols$IGroup$)){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.IGroup,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.IGroup,value);
}
});
devtools.formatters.templating.mark_as_template_BANG_ = (function devtools$formatters$templating$mark_as_template_BANG_(value){
var x11971_11972 = value;
x11971_11972.devtools$protocols$ITemplate$ = true;


return value;
});
devtools.formatters.templating.template_QMARK_ = (function devtools$formatters$templating$template_QMARK_(value){
if(!((value == null))){
if((false) || (value.devtools$protocols$ITemplate$)){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ITemplate,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ITemplate,value);
}
});
devtools.formatters.templating.mark_as_surrogate_BANG_ = (function devtools$formatters$templating$mark_as_surrogate_BANG_(value){
var x11976_11977 = value;
x11976_11977.devtools$protocols$ISurrogate$ = true;


return value;
});
devtools.formatters.templating.surrogate_QMARK_ = (function devtools$formatters$templating$surrogate_QMARK_(value){
if(!((value == null))){
if((false) || (value.devtools$protocols$ISurrogate$)){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ISurrogate,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ISurrogate,value);
}
});
devtools.formatters.templating.reference_QMARK_ = (function devtools$formatters$templating$reference_QMARK_(value){
var and__5590__auto__ = devtools.formatters.templating.group_QMARK_.call(null,value);
if(cljs.core.truth_(and__5590__auto__)){
return cljs.core._EQ_.call(null,(value[(0)]),"object");
} else {
return and__5590__auto__;
}
});
devtools.formatters.templating.make_group = (function devtools$formatters$templating$make_group(var_args){
var args__6684__auto__ = [];
var len__6677__auto___11985 = arguments.length;
var i__6678__auto___11986 = (0);
while(true){
if((i__6678__auto___11986 < len__6677__auto___11985)){
args__6684__auto__.push((arguments[i__6678__auto___11986]));

var G__11987 = (i__6678__auto___11986 + (1));
i__6678__auto___11986 = G__11987;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic = (function (items){
var group = devtools.formatters.templating.mark_as_group_BANG_.call(null,[]);
var seq__11981_11988 = cljs.core.seq.call(null,items);
var chunk__11982_11989 = null;
var count__11983_11990 = (0);
var i__11984_11991 = (0);
while(true){
if((i__11984_11991 < count__11983_11990)){
var item_11992 = cljs.core._nth.call(null,chunk__11982_11989,i__11984_11991);
if(cljs.core.some_QMARK_.call(null,item_11992)){
if(cljs.core.coll_QMARK_.call(null,item_11992)){
(group["push"]).apply(group,devtools.formatters.templating.mark_as_group_BANG_.call(null,cljs.core.into_array.call(null,item_11992)));
} else {
group.push(devtools.formatters.helpers.pref.call(null,item_11992));
}
} else {
}

var G__11993 = seq__11981_11988;
var G__11994 = chunk__11982_11989;
var G__11995 = count__11983_11990;
var G__11996 = (i__11984_11991 + (1));
seq__11981_11988 = G__11993;
chunk__11982_11989 = G__11994;
count__11983_11990 = G__11995;
i__11984_11991 = G__11996;
continue;
} else {
var temp__4657__auto___11997 = cljs.core.seq.call(null,seq__11981_11988);
if(temp__4657__auto___11997){
var seq__11981_11998__$1 = temp__4657__auto___11997;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11981_11998__$1)){
var c__6413__auto___11999 = cljs.core.chunk_first.call(null,seq__11981_11998__$1);
var G__12000 = cljs.core.chunk_rest.call(null,seq__11981_11998__$1);
var G__12001 = c__6413__auto___11999;
var G__12002 = cljs.core.count.call(null,c__6413__auto___11999);
var G__12003 = (0);
seq__11981_11988 = G__12000;
chunk__11982_11989 = G__12001;
count__11983_11990 = G__12002;
i__11984_11991 = G__12003;
continue;
} else {
var item_12004 = cljs.core.first.call(null,seq__11981_11998__$1);
if(cljs.core.some_QMARK_.call(null,item_12004)){
if(cljs.core.coll_QMARK_.call(null,item_12004)){
(group["push"]).apply(group,devtools.formatters.templating.mark_as_group_BANG_.call(null,cljs.core.into_array.call(null,item_12004)));
} else {
group.push(devtools.formatters.helpers.pref.call(null,item_12004));
}
} else {
}

var G__12005 = cljs.core.next.call(null,seq__11981_11998__$1);
var G__12006 = null;
var G__12007 = (0);
var G__12008 = (0);
seq__11981_11988 = G__12005;
chunk__11982_11989 = G__12006;
count__11983_11990 = G__12007;
i__11984_11991 = G__12008;
continue;
}
} else {
}
}
break;
}

return group;
});

devtools.formatters.templating.make_group.cljs$lang$maxFixedArity = (0);

devtools.formatters.templating.make_group.cljs$lang$applyTo = (function (seq11980){
return devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq11980));
});

devtools.formatters.templating.make_template = (function devtools$formatters$templating$make_template(var_args){
var args__6684__auto__ = [];
var len__6677__auto___12016 = arguments.length;
var i__6678__auto___12017 = (0);
while(true){
if((i__6678__auto___12017 < len__6677__auto___12016)){
args__6684__auto__.push((arguments[i__6678__auto___12017]));

var G__12018 = (i__6678__auto___12017 + (1));
i__6678__auto___12017 = G__12018;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((2) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((2)),(0),null)):null);
return devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__6685__auto__);
});

devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic = (function (tag,style,children){
var tag__$1 = devtools.formatters.helpers.pref.call(null,tag);
var style__$1 = devtools.formatters.helpers.pref.call(null,style);
var template = devtools.formatters.templating.mark_as_template_BANG_.call(null,[tag__$1,((cljs.core.empty_QMARK_.call(null,style__$1))?{}:{"style": style__$1})]);
var seq__12012_12019 = cljs.core.seq.call(null,children);
var chunk__12013_12020 = null;
var count__12014_12021 = (0);
var i__12015_12022 = (0);
while(true){
if((i__12015_12022 < count__12014_12021)){
var child_12023 = cljs.core._nth.call(null,chunk__12013_12020,i__12015_12022);
if(cljs.core.some_QMARK_.call(null,child_12023)){
if(cljs.core.coll_QMARK_.call(null,child_12023)){
(template["push"]).apply(template,devtools.formatters.templating.mark_as_template_BANG_.call(null,cljs.core.into_array.call(null,cljs.core.keep.call(null,devtools.formatters.helpers.pref,child_12023))));
} else {
var temp__4655__auto___12024 = devtools.formatters.helpers.pref.call(null,child_12023);
if(cljs.core.truth_(temp__4655__auto___12024)){
var child_value_12025 = temp__4655__auto___12024;
template.push(child_value_12025);
} else {
}
}
} else {
}

var G__12026 = seq__12012_12019;
var G__12027 = chunk__12013_12020;
var G__12028 = count__12014_12021;
var G__12029 = (i__12015_12022 + (1));
seq__12012_12019 = G__12026;
chunk__12013_12020 = G__12027;
count__12014_12021 = G__12028;
i__12015_12022 = G__12029;
continue;
} else {
var temp__4657__auto___12030 = cljs.core.seq.call(null,seq__12012_12019);
if(temp__4657__auto___12030){
var seq__12012_12031__$1 = temp__4657__auto___12030;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12012_12031__$1)){
var c__6413__auto___12032 = cljs.core.chunk_first.call(null,seq__12012_12031__$1);
var G__12033 = cljs.core.chunk_rest.call(null,seq__12012_12031__$1);
var G__12034 = c__6413__auto___12032;
var G__12035 = cljs.core.count.call(null,c__6413__auto___12032);
var G__12036 = (0);
seq__12012_12019 = G__12033;
chunk__12013_12020 = G__12034;
count__12014_12021 = G__12035;
i__12015_12022 = G__12036;
continue;
} else {
var child_12037 = cljs.core.first.call(null,seq__12012_12031__$1);
if(cljs.core.some_QMARK_.call(null,child_12037)){
if(cljs.core.coll_QMARK_.call(null,child_12037)){
(template["push"]).apply(template,devtools.formatters.templating.mark_as_template_BANG_.call(null,cljs.core.into_array.call(null,cljs.core.keep.call(null,devtools.formatters.helpers.pref,child_12037))));
} else {
var temp__4655__auto___12038 = devtools.formatters.helpers.pref.call(null,child_12037);
if(cljs.core.truth_(temp__4655__auto___12038)){
var child_value_12039 = temp__4655__auto___12038;
template.push(child_value_12039);
} else {
}
}
} else {
}

var G__12040 = cljs.core.next.call(null,seq__12012_12031__$1);
var G__12041 = null;
var G__12042 = (0);
var G__12043 = (0);
seq__12012_12019 = G__12040;
chunk__12013_12020 = G__12041;
count__12014_12021 = G__12042;
i__12015_12022 = G__12043;
continue;
}
} else {
}
}
break;
}

return template;
});

devtools.formatters.templating.make_template.cljs$lang$maxFixedArity = (2);

devtools.formatters.templating.make_template.cljs$lang$applyTo = (function (seq12009){
var G__12010 = cljs.core.first.call(null,seq12009);
var seq12009__$1 = cljs.core.next.call(null,seq12009);
var G__12011 = cljs.core.first.call(null,seq12009__$1);
var seq12009__$2 = cljs.core.next.call(null,seq12009__$1);
return devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic(G__12010,G__12011,seq12009__$2);
});

devtools.formatters.templating.concat_templates_BANG_ = (function devtools$formatters$templating$concat_templates_BANG_(var_args){
var args__6684__auto__ = [];
var len__6677__auto___12046 = arguments.length;
var i__6678__auto___12047 = (0);
while(true){
if((i__6678__auto___12047 < len__6677__auto___12046)){
args__6684__auto__.push((arguments[i__6678__auto___12047]));

var G__12048 = (i__6678__auto___12047 + (1));
i__6678__auto___12047 = G__12048;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (template,templates){
return devtools.formatters.templating.mark_as_template_BANG_.call(null,goog.object.get(template,"concat").apply(template,cljs.core.into_array.call(null,cljs.core.map.call(null,cljs.core.into_array,cljs.core.keep.call(null,devtools.formatters.helpers.pref,templates)))));
});

devtools.formatters.templating.concat_templates_BANG_.cljs$lang$maxFixedArity = (1);

devtools.formatters.templating.concat_templates_BANG_.cljs$lang$applyTo = (function (seq12044){
var G__12045 = cljs.core.first.call(null,seq12044);
var seq12044__$1 = cljs.core.next.call(null,seq12044);
return devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__12045,seq12044__$1);
});

devtools.formatters.templating.extend_template_BANG_ = (function devtools$formatters$templating$extend_template_BANG_(var_args){
var args__6684__auto__ = [];
var len__6677__auto___12051 = arguments.length;
var i__6678__auto___12052 = (0);
while(true){
if((i__6678__auto___12052 < len__6677__auto___12051)){
args__6684__auto__.push((arguments[i__6678__auto___12052]));

var G__12053 = (i__6678__auto___12052 + (1));
i__6678__auto___12052 = G__12053;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (template,args){
return devtools.formatters.templating.concat_templates_BANG_.call(null,template,args);
});

devtools.formatters.templating.extend_template_BANG_.cljs$lang$maxFixedArity = (1);

devtools.formatters.templating.extend_template_BANG_.cljs$lang$applyTo = (function (seq12049){
var G__12050 = cljs.core.first.call(null,seq12049);
var seq12049__$1 = cljs.core.next.call(null,seq12049);
return devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__12050,seq12049__$1);
});

devtools.formatters.templating.make_surrogate = (function devtools$formatters$templating$make_surrogate(var_args){
var args12054 = [];
var len__6677__auto___12059 = arguments.length;
var i__6678__auto___12060 = (0);
while(true){
if((i__6678__auto___12060 < len__6677__auto___12059)){
args12054.push((arguments[i__6678__auto___12060]));

var G__12061 = (i__6678__auto___12060 + (1));
i__6678__auto___12060 = G__12061;
continue;
} else {
}
break;
}

var G__12056 = args12054.length;
switch (G__12056) {
case 1:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args12054.length)].join('')));

}
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$1 = (function (object){
return devtools.formatters.templating.make_surrogate.call(null,object,null);
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$2 = (function (object,header){
return devtools.formatters.templating.make_surrogate.call(null,object,header,null);
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$3 = (function (object,header,body){
return devtools.formatters.templating.make_surrogate.call(null,object,header,body,(0));
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$4 = (function (object,header,body,start_index){
return devtools.formatters.templating.mark_as_surrogate_BANG_.call(null,(function (){var obj12058 = {"target":object,"header":header,"body":body,"startIndex":(function (){var or__5602__auto__ = start_index;
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
return (0);
}
})()};
return obj12058;
})());
});

devtools.formatters.templating.make_surrogate.cljs$lang$maxFixedArity = 4;

devtools.formatters.templating.get_surrogate_target = (function devtools$formatters$templating$get_surrogate_target(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"target");
});
devtools.formatters.templating.get_surrogate_header = (function devtools$formatters$templating$get_surrogate_header(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"header");
});
devtools.formatters.templating.get_surrogate_body = (function devtools$formatters$templating$get_surrogate_body(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"body");
});
devtools.formatters.templating.get_surrogate_start_index = (function devtools$formatters$templating$get_surrogate_start_index(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"startIndex");
});
devtools.formatters.templating.make_reference = (function devtools$formatters$templating$make_reference(var_args){
var args__6684__auto__ = [];
var len__6677__auto___12069 = arguments.length;
var i__6678__auto___12070 = (0);
while(true){
if((i__6678__auto___12070 < len__6677__auto___12069)){
args__6684__auto__.push((arguments[i__6678__auto___12070]));

var G__12071 = (i__6678__auto___12070 + (1));
i__6678__auto___12070 = G__12071;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic = (function (object,p__12065){
var vec__12066 = p__12065;
var state_override_fn = cljs.core.nth.call(null,vec__12066,(0),null);
if(((state_override_fn == null)) || (cljs.core.fn_QMARK_.call(null,state_override_fn))){
} else {
throw (new Error("Assert failed: (or (nil? state-override-fn) (fn? state-override-fn))"));
}

if((object == null)){
return devtools.formatters.templating.make_template.call(null,new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.Keyword(null,"nil-style","nil-style",-1505044832),new cljs.core.Keyword(null,"nil-label","nil-label",-587789203));
} else {
var sub_state = ((cljs.core.some_QMARK_.call(null,state_override_fn))?state_override_fn.call(null,devtools.formatters.state.get_current_state.call(null)):devtools.formatters.state.get_current_state.call(null));
return devtools.formatters.templating.make_group.call(null,"object",{"object": object, "config": sub_state});
}
});

devtools.formatters.templating.make_reference.cljs$lang$maxFixedArity = (1);

devtools.formatters.templating.make_reference.cljs$lang$applyTo = (function (seq12063){
var G__12064 = cljs.core.first.call(null,seq12063);
var seq12063__$1 = cljs.core.next.call(null,seq12063);
return devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic(G__12064,seq12063__$1);
});

devtools.formatters.templating._STAR_current_render_stack_STAR_ = cljs.core.PersistentVector.EMPTY;
devtools.formatters.templating._STAR_current_render_path_STAR_ = cljs.core.PersistentVector.EMPTY;
devtools.formatters.templating.pprint_str = (function devtools$formatters$templating$pprint_str(markup){
var sb__6588__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR_12075_12078 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR_12076_12079 = cljs.core._STAR_print_fn_STAR_;
cljs.core._STAR_print_newline_STAR_ = true;

cljs.core._STAR_print_fn_STAR_ = ((function (_STAR_print_newline_STAR_12075_12078,_STAR_print_fn_STAR_12076_12079,sb__6588__auto__){
return (function (x__6589__auto__){
return sb__6588__auto__.append(x__6589__auto__);
});})(_STAR_print_newline_STAR_12075_12078,_STAR_print_fn_STAR_12076_12079,sb__6588__auto__))
;

try{var _STAR_print_level_STAR_12077_12080 = cljs.core._STAR_print_level_STAR_;
cljs.core._STAR_print_level_STAR_ = (300);

try{cljs.pprint.pprint.call(null,markup);
}finally {cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR_12077_12080;
}}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR_12076_12079;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR_12075_12078;
}
return [cljs.core.str(sb__6588__auto__)].join('');
});
devtools.formatters.templating.print_preview = (function devtools$formatters$templating$print_preview(markup){
var _STAR_print_level_STAR_12082 = cljs.core._STAR_print_level_STAR_;
cljs.core._STAR_print_level_STAR_ = (1);

try{return cljs.core.pr_str.call(null,markup);
}finally {cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR_12082;
}});
devtools.formatters.templating.add_stack_separators = (function devtools$formatters$templating$add_stack_separators(stack){
return cljs.core.interpose.call(null,"-------------",stack);
});
devtools.formatters.templating.replace_fns_with_markers = (function devtools$formatters$templating$replace_fns_with_markers(stack){
var f = (function (v){
if(cljs.core.fn_QMARK_.call(null,v)){
return "##fn##";
} else {
return v;
}
});
return clojure.walk.prewalk.call(null,f,stack);
});
devtools.formatters.templating.pprint_render_calls = (function devtools$formatters$templating$pprint_render_calls(stack){
return cljs.core.map.call(null,devtools.formatters.templating.pprint_str,stack);
});
devtools.formatters.templating.pprint_render_stack = (function devtools$formatters$templating$pprint_render_stack(stack){
return clojure.string.join.call(null,"\n",devtools.formatters.templating.add_stack_separators.call(null,devtools.formatters.templating.pprint_render_calls.call(null,devtools.formatters.templating.replace_fns_with_markers.call(null,cljs.core.reverse.call(null,stack)))));
});
devtools.formatters.templating.pprint_render_path = (function devtools$formatters$templating$pprint_render_path(path){
return devtools.formatters.templating.pprint_str.call(null,path);
});
devtools.formatters.templating.assert_markup_error = (function devtools$formatters$templating$assert_markup_error(msg){
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str(msg),cljs.core.str("\n"),cljs.core.str("Render path: "),cljs.core.str(devtools.formatters.templating.pprint_render_path.call(null,devtools.formatters.templating._STAR_current_render_path_STAR_)),cljs.core.str("\n"),cljs.core.str("Render stack:\n"),cljs.core.str(devtools.formatters.templating.pprint_render_stack.call(null,devtools.formatters.templating._STAR_current_render_stack_STAR_))].join('')),cljs.core.str("\n"),cljs.core.str("false")].join('')));

});
devtools.formatters.templating.surrogate_markup_QMARK_ = (function devtools$formatters$templating$surrogate_markup_QMARK_(markup){
return (cljs.core.sequential_QMARK_.call(null,markup)) && (cljs.core._EQ_.call(null,cljs.core.first.call(null,markup),"surrogate"));
});
devtools.formatters.templating.render_special = (function devtools$formatters$templating$render_special(name,args){
var G__12084 = name;
switch (G__12084) {
case "surrogate":
var obj = cljs.core.first.call(null,args);
var converted_args = cljs.core.map.call(null,devtools.formatters.templating.render_json_ml_STAR_,cljs.core.rest.call(null,args));
return cljs.core.apply.call(null,devtools.formatters.templating.make_surrogate,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [obj], null),converted_args));

break;
case "reference":
var obj = cljs.core.first.call(null,args);
var converted_obj = (cljs.core.truth_(devtools.formatters.templating.surrogate_markup_QMARK_.call(null,obj))?devtools.formatters.templating.render_json_ml_STAR_.call(null,obj):obj);
return cljs.core.apply.call(null,devtools.formatters.templating.make_reference,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [converted_obj], null),cljs.core.rest.call(null,args)));

break;
default:
return devtools.formatters.templating.assert_markup_error.call(null,[cljs.core.str("no matching special tag name: '"),cljs.core.str(name),cljs.core.str("'")].join(''));

}
});
devtools.formatters.templating.emptyish_QMARK_ = (function devtools$formatters$templating$emptyish_QMARK_(v){
if((cljs.core.seqable_QMARK_.call(null,v)) || (cljs.core.array_QMARK_.call(null,v)) || (typeof v === 'string')){
return cljs.core.empty_QMARK_.call(null,v);
} else {
return false;
}
});
devtools.formatters.templating.render_subtree = (function devtools$formatters$templating$render_subtree(tag,children){
var vec__12089 = tag;
var html_tag = cljs.core.nth.call(null,vec__12089,(0),null);
var style = cljs.core.nth.call(null,vec__12089,(1),null);
return cljs.core.apply.call(null,devtools.formatters.templating.make_template,html_tag,style,cljs.core.map.call(null,devtools.formatters.templating.render_json_ml_STAR_,cljs.core.remove.call(null,devtools.formatters.templating.emptyish_QMARK_,cljs.core.map.call(null,devtools.formatters.helpers.pref,children))));
});
devtools.formatters.templating.render_json_ml_STAR_ = (function devtools$formatters$templating$render_json_ml_STAR_(markup){
if(!(cljs.core.sequential_QMARK_.call(null,markup))){
return markup;
} else {
var _STAR_current_render_path_STAR_12093 = devtools.formatters.templating._STAR_current_render_path_STAR_;
devtools.formatters.templating._STAR_current_render_path_STAR_ = cljs.core.conj.call(null,devtools.formatters.templating._STAR_current_render_path_STAR_,cljs.core.first.call(null,markup));

try{var tag = devtools.formatters.helpers.pref.call(null,cljs.core.first.call(null,markup));
if(typeof tag === 'string'){
return devtools.formatters.templating.render_special.call(null,tag,cljs.core.rest.call(null,markup));
} else {
if(cljs.core.sequential_QMARK_.call(null,tag)){
return devtools.formatters.templating.render_subtree.call(null,tag,cljs.core.rest.call(null,markup));
} else {
return devtools.formatters.templating.assert_markup_error.call(null,[cljs.core.str("invalid json-ml markup at "),cljs.core.str(devtools.formatters.templating.print_preview.call(null,markup)),cljs.core.str(":")].join(''));

}
}
}finally {devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR_12093;
}}
});
devtools.formatters.templating.render_json_ml = (function devtools$formatters$templating$render_json_ml(markup){
var _STAR_current_render_stack_STAR_12096 = devtools.formatters.templating._STAR_current_render_stack_STAR_;
var _STAR_current_render_path_STAR_12097 = devtools.formatters.templating._STAR_current_render_path_STAR_;
devtools.formatters.templating._STAR_current_render_stack_STAR_ = cljs.core.conj.call(null,devtools.formatters.templating._STAR_current_render_stack_STAR_,markup);

devtools.formatters.templating._STAR_current_render_path_STAR_ = cljs.core.conj.call(null,devtools.formatters.templating._STAR_current_render_path_STAR_,"<render-json-ml>");

try{return devtools.formatters.templating.render_json_ml_STAR_.call(null,markup);
}finally {devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR_12097;

devtools.formatters.templating._STAR_current_render_stack_STAR_ = _STAR_current_render_stack_STAR_12096;
}});
devtools.formatters.templating.assert_failed_markup_rendering = (function devtools$formatters$templating$assert_failed_markup_rendering(initial_value,value){
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str("result of markup rendering must be a template,\n"),cljs.core.str("resolved to "),cljs.core.str(devtools.formatters.templating.pprint_str.call(null,value)),cljs.core.str("initial value: "),cljs.core.str(devtools.formatters.templating.pprint_str.call(null,initial_value))].join('')),cljs.core.str("\n"),cljs.core.str("false")].join('')));

});
devtools.formatters.templating.render_markup_STAR_ = (function devtools$formatters$templating$render_markup_STAR_(initial_value,value){
while(true){
if(cljs.core.fn_QMARK_.call(null,value)){
var G__12098 = initial_value;
var G__12099 = value.call(null);
initial_value = G__12098;
value = G__12099;
continue;
} else {
if((value instanceof cljs.core.Keyword)){
var G__12100 = initial_value;
var G__12101 = devtools.formatters.helpers.pref.call(null,value);
initial_value = G__12100;
value = G__12101;
continue;
} else {
if(cljs.core.sequential_QMARK_.call(null,value)){
var G__12102 = initial_value;
var G__12103 = devtools.formatters.templating.render_json_ml.call(null,value);
initial_value = G__12102;
value = G__12103;
continue;
} else {
if(cljs.core.truth_(devtools.formatters.templating.template_QMARK_.call(null,value))){
return value;
} else {
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,value))){
return value;
} else {
if(cljs.core.truth_(devtools.formatters.templating.reference_QMARK_.call(null,value))){
return value;
} else {
return devtools.formatters.templating.assert_failed_markup_rendering.call(null,initial_value,value);

}
}
}
}
}
}
break;
}
});
devtools.formatters.templating.render_markup = (function devtools$formatters$templating$render_markup(value){
return devtools.formatters.templating.render_markup_STAR_.call(null,value,value);
});

//# sourceMappingURL=templating.js.map