// Compiled by ClojureScript 1.9.89 {}
goog.provide('devtools.format');
goog.require('cljs.core');

/**
 * @interface
 */
devtools.format.IDevtoolsFormat = function(){};

devtools.format._header = (function devtools$format$_header(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_header$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_header$arity$1(value);
} else {
var x__6265__auto__ = (((value == null))?null:value);
var m__6266__auto__ = (devtools.format._header[goog.typeOf(x__6265__auto__)]);
if(!((m__6266__auto__ == null))){
return m__6266__auto__.call(null,value);
} else {
var m__6266__auto____$1 = (devtools.format._header["_"]);
if(!((m__6266__auto____$1 == null))){
return m__6266__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-header",value);
}
}
}
});

devtools.format._has_body = (function devtools$format$_has_body(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_has_body$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_has_body$arity$1(value);
} else {
var x__6265__auto__ = (((value == null))?null:value);
var m__6266__auto__ = (devtools.format._has_body[goog.typeOf(x__6265__auto__)]);
if(!((m__6266__auto__ == null))){
return m__6266__auto__.call(null,value);
} else {
var m__6266__auto____$1 = (devtools.format._has_body["_"]);
if(!((m__6266__auto____$1 == null))){
return m__6266__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-has-body",value);
}
}
}
});

devtools.format._body = (function devtools$format$_body(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_body$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_body$arity$1(value);
} else {
var x__6265__auto__ = (((value == null))?null:value);
var m__6266__auto__ = (devtools.format._body[goog.typeOf(x__6265__auto__)]);
if(!((m__6266__auto__ == null))){
return m__6266__auto__.call(null,value);
} else {
var m__6266__auto____$1 = (devtools.format._body["_"]);
if(!((m__6266__auto____$1 == null))){
return m__6266__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-body",value);
}
}
}
});

devtools.format.setup_BANG_ = (function devtools$format$setup_BANG_(){
if(cljs.core.truth_(devtools.format._STAR_setup_done_STAR_)){
return null;
} else {
devtools.format._STAR_setup_done_STAR_ = true;

devtools.format.make_template_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__9514__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__9514__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__9514__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__9514__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__9513__auto__ = temp__4657__auto____$2;
return goog.object.get(o__9513__auto__,"make_template");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_group_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__9514__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__9514__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__9514__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__9514__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__9513__auto__ = temp__4657__auto____$2;
return goog.object.get(o__9513__auto__,"make_group");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_reference_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__9514__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__9514__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__9514__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__9514__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__9513__auto__ = temp__4657__auto____$2;
return goog.object.get(o__9513__auto__,"make_reference");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_surrogate_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__9514__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__9514__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__9514__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__9514__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__9513__auto__ = temp__4657__auto____$2;
return goog.object.get(o__9513__auto__,"make_surrogate");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.render_markup_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__9514__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__9514__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__9514__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__9514__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__9513__auto__ = temp__4657__auto____$2;
return goog.object.get(o__9513__auto__,"render_markup");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format._LT_header_GT__fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__9514__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__9514__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__9514__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__9514__auto____$1,"markup");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__9513__auto__ = temp__4657__auto____$2;
return goog.object.get(o__9513__auto__,"_LT_header_GT_");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format._LT_standard_body_GT__fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__9514__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__9514__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__9514__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__9514__auto____$1,"markup");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__9513__auto__ = temp__4657__auto____$2;
return goog.object.get(o__9513__auto__,"_LT_standard_body_GT_");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

if(cljs.core.truth_(devtools.format.make_template_fn)){
} else {
throw (new Error("Assert failed: make-template-fn"));
}

if(cljs.core.truth_(devtools.format.make_group_fn)){
} else {
throw (new Error("Assert failed: make-group-fn"));
}

if(cljs.core.truth_(devtools.format.make_reference_fn)){
} else {
throw (new Error("Assert failed: make-reference-fn"));
}

if(cljs.core.truth_(devtools.format.make_surrogate_fn)){
} else {
throw (new Error("Assert failed: make-surrogate-fn"));
}

if(cljs.core.truth_(devtools.format.render_markup_fn)){
} else {
throw (new Error("Assert failed: render-markup-fn"));
}

if(cljs.core.truth_(devtools.format._LT_header_GT__fn)){
} else {
throw (new Error("Assert failed: <header>-fn"));
}

if(cljs.core.truth_(devtools.format._LT_standard_body_GT__fn)){
return null;
} else {
throw (new Error("Assert failed: <standard-body>-fn"));
}
}
});
devtools.format.render_markup = (function devtools$format$render_markup(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9535 = arguments.length;
var i__6678__auto___9536 = (0);
while(true){
if((i__6678__auto___9536 < len__6677__auto___9535)){
args__6684__auto__.push((arguments[i__6678__auto___9536]));

var G__9537 = (i__6678__auto___9536 + (1));
i__6678__auto___9536 = G__9537;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.render_markup_fn,args);
});

devtools.format.render_markup.cljs$lang$maxFixedArity = (0);

devtools.format.render_markup.cljs$lang$applyTo = (function (seq9534){
return devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9534));
});

devtools.format.make_template = (function devtools$format$make_template(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9539 = arguments.length;
var i__6678__auto___9540 = (0);
while(true){
if((i__6678__auto___9540 < len__6677__auto___9539)){
args__6684__auto__.push((arguments[i__6678__auto___9540]));

var G__9541 = (i__6678__auto___9540 + (1));
i__6678__auto___9540 = G__9541;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_template_fn,args);
});

devtools.format.make_template.cljs$lang$maxFixedArity = (0);

devtools.format.make_template.cljs$lang$applyTo = (function (seq9538){
return devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9538));
});

devtools.format.make_group = (function devtools$format$make_group(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9543 = arguments.length;
var i__6678__auto___9544 = (0);
while(true){
if((i__6678__auto___9544 < len__6677__auto___9543)){
args__6684__auto__.push((arguments[i__6678__auto___9544]));

var G__9545 = (i__6678__auto___9544 + (1));
i__6678__auto___9544 = G__9545;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_group_fn,args);
});

devtools.format.make_group.cljs$lang$maxFixedArity = (0);

devtools.format.make_group.cljs$lang$applyTo = (function (seq9542){
return devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9542));
});

devtools.format.make_surrogate = (function devtools$format$make_surrogate(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9547 = arguments.length;
var i__6678__auto___9548 = (0);
while(true){
if((i__6678__auto___9548 < len__6677__auto___9547)){
args__6684__auto__.push((arguments[i__6678__auto___9548]));

var G__9549 = (i__6678__auto___9548 + (1));
i__6678__auto___9548 = G__9549;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_surrogate_fn,args);
});

devtools.format.make_surrogate.cljs$lang$maxFixedArity = (0);

devtools.format.make_surrogate.cljs$lang$applyTo = (function (seq9546){
return devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9546));
});

devtools.format.template = (function devtools$format$template(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9551 = arguments.length;
var i__6678__auto___9552 = (0);
while(true){
if((i__6678__auto___9552 < len__6677__auto___9551)){
args__6684__auto__.push((arguments[i__6678__auto___9552]));

var G__9553 = (i__6678__auto___9552 + (1));
i__6678__auto___9552 = G__9553;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.template.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.template.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_template_fn,args);
});

devtools.format.template.cljs$lang$maxFixedArity = (0);

devtools.format.template.cljs$lang$applyTo = (function (seq9550){
return devtools.format.template.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9550));
});

devtools.format.group = (function devtools$format$group(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9555 = arguments.length;
var i__6678__auto___9556 = (0);
while(true){
if((i__6678__auto___9556 < len__6677__auto___9555)){
args__6684__auto__.push((arguments[i__6678__auto___9556]));

var G__9557 = (i__6678__auto___9556 + (1));
i__6678__auto___9556 = G__9557;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.group.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.group.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_group_fn,args);
});

devtools.format.group.cljs$lang$maxFixedArity = (0);

devtools.format.group.cljs$lang$applyTo = (function (seq9554){
return devtools.format.group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9554));
});

devtools.format.surrogate = (function devtools$format$surrogate(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9559 = arguments.length;
var i__6678__auto___9560 = (0);
while(true){
if((i__6678__auto___9560 < len__6677__auto___9559)){
args__6684__auto__.push((arguments[i__6678__auto___9560]));

var G__9561 = (i__6678__auto___9560 + (1));
i__6678__auto___9560 = G__9561;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_surrogate_fn,args);
});

devtools.format.surrogate.cljs$lang$maxFixedArity = (0);

devtools.format.surrogate.cljs$lang$applyTo = (function (seq9558){
return devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9558));
});

devtools.format.reference = (function devtools$format$reference(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9569 = arguments.length;
var i__6678__auto___9570 = (0);
while(true){
if((i__6678__auto___9570 < len__6677__auto___9569)){
args__6684__auto__.push((arguments[i__6678__auto___9570]));

var G__9571 = (i__6678__auto___9570 + (1));
i__6678__auto___9570 = G__9571;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic = (function (object,p__9565){
var vec__9566 = p__9565;
var state_override = cljs.core.nth.call(null,vec__9566,(0),null);
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_reference_fn,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [object,((function (vec__9566,state_override){
return (function (p1__9562_SHARP_){
return cljs.core.merge.call(null,p1__9562_SHARP_,state_override);
});})(vec__9566,state_override))
], null));
});

devtools.format.reference.cljs$lang$maxFixedArity = (1);

devtools.format.reference.cljs$lang$applyTo = (function (seq9563){
var G__9564 = cljs.core.first.call(null,seq9563);
var seq9563__$1 = cljs.core.next.call(null,seq9563);
return devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic(G__9564,seq9563__$1);
});

devtools.format.standard_reference = (function devtools$format$standard_reference(target){
devtools.format.setup_BANG_.call(null);

return devtools.format.make_template_fn.call(null,new cljs.core.Keyword(null,"ol","ol",932524051),new cljs.core.Keyword(null,"standard-ol-style","standard-ol-style",2143825615),devtools.format.make_template_fn.call(null,new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.Keyword(null,"standard-li-style","standard-li-style",413442955),devtools.format.make_reference_fn.call(null,target)));
});
devtools.format.build_header = (function devtools$format$build_header(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9573 = arguments.length;
var i__6678__auto___9574 = (0);
while(true){
if((i__6678__auto___9574 < len__6677__auto___9573)){
args__6684__auto__.push((arguments[i__6678__auto___9574]));

var G__9575 = (i__6678__auto___9574 + (1));
i__6678__auto___9574 = G__9575;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((0) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((0)),(0),null)):null);
return devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic(argseq__6685__auto__);
});

devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return devtools.format.render_markup.call(null,cljs.core.apply.call(null,devtools.format._LT_header_GT__fn,args));
});

devtools.format.build_header.cljs$lang$maxFixedArity = (0);

devtools.format.build_header.cljs$lang$applyTo = (function (seq9572){
return devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq9572));
});

devtools.format.standard_body_template = (function devtools$format$standard_body_template(var_args){
var args__6684__auto__ = [];
var len__6677__auto___9578 = arguments.length;
var i__6678__auto___9579 = (0);
while(true){
if((i__6678__auto___9579 < len__6677__auto___9578)){
args__6684__auto__.push((arguments[i__6678__auto___9579]));

var G__9580 = (i__6678__auto___9579 + (1));
i__6678__auto___9579 = G__9580;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic = (function (lines,rest){
devtools.format.setup_BANG_.call(null);

var args = cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map.call(null,(function (x){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [x], null);
}),lines)], null),rest);
return devtools.format.render_markup.call(null,cljs.core.apply.call(null,devtools.format._LT_standard_body_GT__fn,args));
});

devtools.format.standard_body_template.cljs$lang$maxFixedArity = (1);

devtools.format.standard_body_template.cljs$lang$applyTo = (function (seq9576){
var G__9577 = cljs.core.first.call(null,seq9576);
var seq9576__$1 = cljs.core.next.call(null,seq9576);
return devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic(G__9577,seq9576__$1);
});


//# sourceMappingURL=format.js.map