// Compiled by ClojureScript 1.9.89 {}
goog.provide('re_frame.fx');
goog.require('cljs.core');
goog.require('re_frame.router');
goog.require('re_frame.db');
goog.require('re_frame.interceptor');
goog.require('re_frame.interop');
goog.require('re_frame.events');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_(re_frame.registrar.kinds.call(null,re_frame.fx.kind))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
re_frame.fx.register = cljs.core.partial.call(null,re_frame.registrar.register_handler,re_frame.fx.kind);
/**
 * An interceptor which actions a `context's` (side) `:effects`.
 * 
 *   For each key in the `:effects` map, call the `effects handler` previously
 *   registered using `reg-fx`.
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 *   call the registered effects handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.call(null,new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
var seq__19330 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__19331 = null;
var count__19332 = (0);
var i__19333 = (0);
while(true){
if((i__19333 < count__19332)){
var vec__19334 = cljs.core._nth.call(null,chunk__19331,i__19333);
var effect_k = cljs.core.nth.call(null,vec__19334,(0),null);
var value = cljs.core.nth.call(null,vec__19334,(1),null);
var temp__4655__auto___19340 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_k,true);
if(cljs.core.truth_(temp__4655__auto___19340)){
var effect_fn_19341 = temp__4655__auto___19340;
effect_fn_19341.call(null,value);
} else {
}

var G__19342 = seq__19330;
var G__19343 = chunk__19331;
var G__19344 = count__19332;
var G__19345 = (i__19333 + (1));
seq__19330 = G__19342;
chunk__19331 = G__19343;
count__19332 = G__19344;
i__19333 = G__19345;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__19330);
if(temp__4657__auto__){
var seq__19330__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__19330__$1)){
var c__6413__auto__ = cljs.core.chunk_first.call(null,seq__19330__$1);
var G__19346 = cljs.core.chunk_rest.call(null,seq__19330__$1);
var G__19347 = c__6413__auto__;
var G__19348 = cljs.core.count.call(null,c__6413__auto__);
var G__19349 = (0);
seq__19330 = G__19346;
chunk__19331 = G__19347;
count__19332 = G__19348;
i__19333 = G__19349;
continue;
} else {
var vec__19337 = cljs.core.first.call(null,seq__19330__$1);
var effect_k = cljs.core.nth.call(null,vec__19337,(0),null);
var value = cljs.core.nth.call(null,vec__19337,(1),null);
var temp__4655__auto___19350 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_k,true);
if(cljs.core.truth_(temp__4655__auto___19350)){
var effect_fn_19351 = temp__4655__auto___19350;
effect_fn_19351.call(null,value);
} else {
}

var G__19352 = cljs.core.next.call(null,seq__19330__$1);
var G__19353 = null;
var G__19354 = (0);
var G__19355 = (0);
seq__19330 = G__19352;
chunk__19331 = G__19353;
count__19332 = G__19354;
i__19333 = G__19355;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
var seq__19356 = cljs.core.seq.call(null,value);
var chunk__19357 = null;
var count__19358 = (0);
var i__19359 = (0);
while(true){
if((i__19359 < count__19358)){
var map__19360 = cljs.core._nth.call(null,chunk__19357,i__19359);
var map__19360__$1 = ((((!((map__19360 == null)))?((((map__19360.cljs$lang$protocol_mask$partition0$ & (64))) || (map__19360.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__19360):map__19360);
var effect = map__19360__$1;
var ms = cljs.core.get.call(null,map__19360__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__19360__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number'))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__19356,chunk__19357,count__19358,i__19359,map__19360,map__19360__$1,effect,ms,dispatch){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__19356,chunk__19357,count__19358,i__19359,map__19360,map__19360__$1,effect,ms,dispatch))
,ms);
}

var G__19364 = seq__19356;
var G__19365 = chunk__19357;
var G__19366 = count__19358;
var G__19367 = (i__19359 + (1));
seq__19356 = G__19364;
chunk__19357 = G__19365;
count__19358 = G__19366;
i__19359 = G__19367;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__19356);
if(temp__4657__auto__){
var seq__19356__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__19356__$1)){
var c__6413__auto__ = cljs.core.chunk_first.call(null,seq__19356__$1);
var G__19368 = cljs.core.chunk_rest.call(null,seq__19356__$1);
var G__19369 = c__6413__auto__;
var G__19370 = cljs.core.count.call(null,c__6413__auto__);
var G__19371 = (0);
seq__19356 = G__19368;
chunk__19357 = G__19369;
count__19358 = G__19370;
i__19359 = G__19371;
continue;
} else {
var map__19362 = cljs.core.first.call(null,seq__19356__$1);
var map__19362__$1 = ((((!((map__19362 == null)))?((((map__19362.cljs$lang$protocol_mask$partition0$ & (64))) || (map__19362.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__19362):map__19362);
var effect = map__19362__$1;
var ms = cljs.core.get.call(null,map__19362__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__19362__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number'))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__19356,chunk__19357,count__19358,i__19359,map__19362,map__19362__$1,effect,ms,dispatch,seq__19356__$1,temp__4657__auto__){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__19356,chunk__19357,count__19358,i__19359,map__19362,map__19362__$1,effect,ms,dispatch,seq__19356__$1,temp__4657__auto__))
,ms);
}

var G__19372 = cljs.core.next.call(null,seq__19356__$1);
var G__19373 = null;
var G__19374 = (0);
var G__19375 = (0);
seq__19356 = G__19372;
chunk__19357 = G__19373;
count__19358 = G__19374;
i__19359 = G__19375;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if(!(cljs.core.vector_QMARK_.call(null,value))){
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value);
} else {
return re_frame.router.dispatch.call(null,value);
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if(!(cljs.core.sequential_QMARK_.call(null,value))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-n value. Expected a collection, got got:",value);
} else {
}

var seq__19376 = cljs.core.seq.call(null,value);
var chunk__19377 = null;
var count__19378 = (0);
var i__19379 = (0);
while(true){
if((i__19379 < count__19378)){
var event = cljs.core._nth.call(null,chunk__19377,i__19379);
re_frame.router.dispatch.call(null,event);

var G__19380 = seq__19376;
var G__19381 = chunk__19377;
var G__19382 = count__19378;
var G__19383 = (i__19379 + (1));
seq__19376 = G__19380;
chunk__19377 = G__19381;
count__19378 = G__19382;
i__19379 = G__19383;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__19376);
if(temp__4657__auto__){
var seq__19376__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__19376__$1)){
var c__6413__auto__ = cljs.core.chunk_first.call(null,seq__19376__$1);
var G__19384 = cljs.core.chunk_rest.call(null,seq__19376__$1);
var G__19385 = c__6413__auto__;
var G__19386 = cljs.core.count.call(null,c__6413__auto__);
var G__19387 = (0);
seq__19376 = G__19384;
chunk__19377 = G__19385;
count__19378 = G__19386;
i__19379 = G__19387;
continue;
} else {
var event = cljs.core.first.call(null,seq__19376__$1);
re_frame.router.dispatch.call(null,event);

var G__19388 = cljs.core.next.call(null,seq__19376__$1);
var G__19389 = null;
var G__19390 = (0);
var G__19391 = (0);
seq__19376 = G__19388;
chunk__19377 = G__19389;
count__19378 = G__19390;
i__19379 = G__19391;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.call(null,re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_.call(null,value)){
var seq__19392 = cljs.core.seq.call(null,((cljs.core.sequential_QMARK_.call(null,value))?value:new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [value], null)));
var chunk__19393 = null;
var count__19394 = (0);
var i__19395 = (0);
while(true){
if((i__19395 < count__19394)){
var event = cljs.core._nth.call(null,chunk__19393,i__19395);
clear_event.call(null,event);

var G__19396 = seq__19392;
var G__19397 = chunk__19393;
var G__19398 = count__19394;
var G__19399 = (i__19395 + (1));
seq__19392 = G__19396;
chunk__19393 = G__19397;
count__19394 = G__19398;
i__19395 = G__19399;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__19392);
if(temp__4657__auto__){
var seq__19392__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__19392__$1)){
var c__6413__auto__ = cljs.core.chunk_first.call(null,seq__19392__$1);
var G__19400 = cljs.core.chunk_rest.call(null,seq__19392__$1);
var G__19401 = c__6413__auto__;
var G__19402 = cljs.core.count.call(null,c__6413__auto__);
var G__19403 = (0);
seq__19392 = G__19400;
chunk__19393 = G__19401;
count__19394 = G__19402;
i__19395 = G__19403;
continue;
} else {
var event = cljs.core.first.call(null,seq__19392__$1);
clear_event.call(null,event);

var G__19404 = cljs.core.next.call(null,seq__19392__$1);
var G__19405 = null;
var G__19406 = (0);
var G__19407 = (0);
seq__19392 = G__19404;
chunk__19393 = G__19405;
count__19394 = G__19406;
i__19395 = G__19407;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"db","db",993250759),(function (value){
return cljs.core.reset_BANG_.call(null,re_frame.db.app_db,value);
}));

//# sourceMappingURL=fx.js.map