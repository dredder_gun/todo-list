// Compiled by ClojureScript 1.9.89 {}
goog.provide('re_frame.events');
goog.require('cljs.core');
goog.require('re_frame.db');
goog.require('re_frame.utils');
goog.require('re_frame.interop');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
goog.require('re_frame.interceptor');
goog.require('re_frame.trace');
re_frame.events.kind = new cljs.core.Keyword(null,"event","event",301435442);
if(cljs.core.truth_(re_frame.registrar.kinds.call(null,re_frame.events.kind))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
/**
 * `interceptors` might have nested collections, and contain nil elements.
 *   return a flat collection, with all nils removed.
 *   This function is 9/10 about giving good error messages
 */
re_frame.events.flatten_and_remove_nils = (function re_frame$events$flatten_and_remove_nils(id,interceptors){
var make_chain = (function (p1__19186_SHARP_){
return cljs.core.remove.call(null,cljs.core.nil_QMARK_,cljs.core.flatten.call(null,p1__19186_SHARP_));
});
if(!(re_frame.interop.debug_enabled_QMARK_)){
return make_chain.call(null,interceptors);
} else {
if(cljs.core.coll_QMARK_.call(null,interceptors)){
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("re-frame: when registering "),cljs.core.str(id),cljs.core.str(", expected a collection of interceptors, got:")].join(''),interceptors);
}

var chain = make_chain.call(null,interceptors);
if(cljs.core.empty_QMARK_.call(null,chain)){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("re-frame: when registering"),cljs.core.str(id),cljs.core.str(", given an empty interceptor chain")].join(''));
} else {
}

var temp__4657__auto___19187 = cljs.core.first.call(null,cljs.core.remove.call(null,re_frame.interceptor.interceptor_QMARK_,chain));
if(cljs.core.truth_(temp__4657__auto___19187)){
var not_i_19188 = temp__4657__auto___19187;
if(cljs.core.fn_QMARK_.call(null,not_i_19188)){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("re-frame: when registering "),cljs.core.str(id),cljs.core.str(", got a function instead of an interceptor. Did you provide old style middleware by mistake? Got:")].join(''),not_i_19188);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("re-frame: when registering "),cljs.core.str(id),cljs.core.str(", expected interceptors, but got:")].join(''),not_i_19188);
}
} else {
}

return chain;
}
});
/**
 * Associate the given event `id` with the given collection of `interceptors`.
 * 
 * `interceptors` may contain nested collections and there may be nils
 * at any level,so process this structure into a simple, nil-less vector
 * before registration.
 * 
 * An `event handler` will likely be at the end of the chain (wrapped in an interceptor).
 */
re_frame.events.register = (function re_frame$events$register(id,interceptors){
return re_frame.registrar.register_handler.call(null,re_frame.events.kind,id,re_frame.events.flatten_and_remove_nils.call(null,id,interceptors));
});
re_frame.events._STAR_handling_STAR_ = null;
/**
 * Given an event vector, look up the associated intercepter chain, and execute it.
 */
re_frame.events.handle = (function re_frame$events$handle(event_v){
var event_id = re_frame.utils.first_in_vector.call(null,event_v);
var temp__4655__auto__ = re_frame.registrar.get_handler.call(null,re_frame.events.kind,event_id,true);
if(cljs.core.truth_(temp__4655__auto__)){
var interceptors = temp__4655__auto__;
if(cljs.core.truth_(re_frame.events._STAR_handling_STAR_)){
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("re-frame: while handling \""),cljs.core.str(re_frame.events._STAR_handling_STAR_),cljs.core.str("\", dispatch-sync was called for \""),cljs.core.str(event_v),cljs.core.str("\". You can't call dispatch-sync within an event handler.")].join(''));
} else {
var _STAR_handling_STAR_19203 = re_frame.events._STAR_handling_STAR_;
re_frame.events._STAR_handling_STAR_ = event_v;

try{if(re_frame.trace.is_trace_enabled_QMARK_.call(null)){
var _STAR_current_trace_STAR_19204 = re_frame.trace._STAR_current_trace_STAR_;
re_frame.trace._STAR_current_trace_STAR_ = re_frame.trace.start_trace.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"operation","operation",-1267664310),event_id,new cljs.core.Keyword(null,"op-type","op-type",-1636141668),re_frame.events.kind,new cljs.core.Keyword(null,"tags","tags",1771418977),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"event","event",301435442),event_v], null)], null));

try{try{return re_frame.interceptor.execute.call(null,event_v,interceptors);
}finally {if(re_frame.trace.is_trace_enabled_QMARK_.call(null)){
var end__8270__auto___19217 = re_frame.interop.now.call(null);
var duration__8271__auto___19218 = (end__8270__auto___19217 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
var seq__19205_19219 = cljs.core.seq.call(null,cljs.core.deref.call(null,re_frame.trace.trace_cbs));
var chunk__19206_19220 = null;
var count__19207_19221 = (0);
var i__19208_19222 = (0);
while(true){
if((i__19208_19222 < count__19207_19221)){
var vec__19209_19223 = cljs.core._nth.call(null,chunk__19206_19220,i__19208_19222);
var k__8272__auto___19224 = cljs.core.nth.call(null,vec__19209_19223,(0),null);
var cb__8273__auto___19225 = cljs.core.nth.call(null,vec__19209_19223,(1),null);
try{cb__8273__auto___19225.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__8271__auto___19218,new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now.call(null))], null));
}catch (e19212){if((e19212 instanceof java.lang.Exception)){
var e__8274__auto___19226 = e19212;
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"Error thrown from trace cb",k__8272__auto___19224,"while storing",re_frame.trace._STAR_current_trace_STAR_,e__8274__auto___19226);
} else {
throw e19212;

}
}
var G__19227 = seq__19205_19219;
var G__19228 = chunk__19206_19220;
var G__19229 = count__19207_19221;
var G__19230 = (i__19208_19222 + (1));
seq__19205_19219 = G__19227;
chunk__19206_19220 = G__19228;
count__19207_19221 = G__19229;
i__19208_19222 = G__19230;
continue;
} else {
var temp__4657__auto___19231 = cljs.core.seq.call(null,seq__19205_19219);
if(temp__4657__auto___19231){
var seq__19205_19232__$1 = temp__4657__auto___19231;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__19205_19232__$1)){
var c__6413__auto___19233 = cljs.core.chunk_first.call(null,seq__19205_19232__$1);
var G__19234 = cljs.core.chunk_rest.call(null,seq__19205_19232__$1);
var G__19235 = c__6413__auto___19233;
var G__19236 = cljs.core.count.call(null,c__6413__auto___19233);
var G__19237 = (0);
seq__19205_19219 = G__19234;
chunk__19206_19220 = G__19235;
count__19207_19221 = G__19236;
i__19208_19222 = G__19237;
continue;
} else {
var vec__19213_19238 = cljs.core.first.call(null,seq__19205_19232__$1);
var k__8272__auto___19239 = cljs.core.nth.call(null,vec__19213_19238,(0),null);
var cb__8273__auto___19240 = cljs.core.nth.call(null,vec__19213_19238,(1),null);
try{cb__8273__auto___19240.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__8271__auto___19218,new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now.call(null))], null));
}catch (e19216){if((e19216 instanceof java.lang.Exception)){
var e__8274__auto___19241 = e19216;
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"Error thrown from trace cb",k__8272__auto___19239,"while storing",re_frame.trace._STAR_current_trace_STAR_,e__8274__auto___19241);
} else {
throw e19216;

}
}
var G__19242 = cljs.core.next.call(null,seq__19205_19232__$1);
var G__19243 = null;
var G__19244 = (0);
var G__19245 = (0);
seq__19205_19219 = G__19242;
chunk__19206_19220 = G__19243;
count__19207_19221 = G__19244;
i__19208_19222 = G__19245;
continue;
}
} else {
}
}
break;
}
} else {
}
}}finally {re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR_19204;
}} else {
return re_frame.interceptor.execute.call(null,event_v,interceptors);
}
}finally {re_frame.events._STAR_handling_STAR_ = _STAR_handling_STAR_19203;
}}
} else {
return null;
}
});

//# sourceMappingURL=events.js.map