// Compiled by ClojureScript 1.9.89 {}
goog.provide('todomvc.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('todomvc.views');
goog.require('secretary.core');
goog.require('todomvc.events');
goog.require('goog.history.EventType');
goog.require('todomvc.subs');
goog.require('goog.History');
goog.require('goog.events');
goog.require('devtools.core');
goog.require('re_frame.core');
devtools.core.install_BANG_.call(null);
cljs.core.enable_console_print_BANG_.call(null);
var action__36230__auto___36608 = (function (params__36231__auto__){
if(cljs.core.map_QMARK_.call(null,params__36231__auto__)){
var map__36603 = params__36231__auto__;
var map__36603__$1 = ((((!((map__36603 == null)))?((((map__36603.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36603.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36603):map__36603);
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),new cljs.core.Keyword(null,"all","all",892129742)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__36231__auto__)){
var vec__36605 = params__36231__auto__;
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),new cljs.core.Keyword(null,"all","all",892129742)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/",action__36230__auto___36608);

var action__36230__auto___36614 = (function (params__36231__auto__){
if(cljs.core.map_QMARK_.call(null,params__36231__auto__)){
var map__36609 = params__36231__auto__;
var map__36609__$1 = ((((!((map__36609 == null)))?((((map__36609.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36609.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36609):map__36609);
var filter = cljs.core.get.call(null,map__36609__$1,new cljs.core.Keyword(null,"filter","filter",-948537934));
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),cljs.core.keyword.call(null,filter)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__36231__auto__)){
var vec__36611 = params__36231__auto__;
var filter = cljs.core.nth.call(null,vec__36611,(0),null);
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),cljs.core.keyword.call(null,filter)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/:filter",action__36230__auto___36614);

todomvc.core.history = (function (){var G__36615 = (new goog.History());
goog.events.listen(G__36615,goog.history.EventType.NAVIGATE,((function (G__36615){
return (function (event){
return secretary.core.dispatch_BANG_.call(null,event.token);
});})(G__36615))
);

G__36615.setEnabled(true);

return G__36615;
})();
todomvc.core.main = (function todomvc$core$main(){
re_frame.core.dispatch_sync.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"initialise-db","initialise-db",-533872578)], null));

return reagent.core.render.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.todo_app], null),document.getElementById("app"));
});
goog.exportSymbol('todomvc.core.main', todomvc.core.main);

//# sourceMappingURL=core.js.map?rel=1494229527372