// Compiled by ClojureScript 1.9.89 {}
goog.provide('todomvc.ajax_handlers');
goog.require('cljs.core');
todomvc.ajax_handlers.handler = (function todomvc$ajax_handlers$handler(response){
return console.log([cljs.core.str(response)].join(''));
});
todomvc.ajax_handlers.error_handler = (function todomvc$ajax_handlers$error_handler(p__21837){
var map__21840 = p__21837;
var map__21840__$1 = ((((!((map__21840 == null)))?((((map__21840.cljs$lang$protocol_mask$partition0$ & (64))) || (map__21840.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21840):map__21840);
var status = cljs.core.get.call(null,map__21840__$1,new cljs.core.Keyword(null,"status","status",-1997798413));
var status_text = cljs.core.get.call(null,map__21840__$1,new cljs.core.Keyword(null,"status-text","status-text",-1834235478));
return console.log([cljs.core.str("something bad happened: "),cljs.core.str(status),cljs.core.str(" "),cljs.core.str(status_text)].join(''));
});

//# sourceMappingURL=ajax_handlers.js.map