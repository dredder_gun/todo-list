// Compiled by ClojureScript 1.9.89 {}
goog.provide('reagent.session');
goog.require('cljs.core');
goog.require('reagent.core');
reagent.session.state = reagent.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
/**
 * Returns a cursor from the state atom.
 */
reagent.session.cursor = (function reagent$session$cursor(ks){
return reagent.core.cursor.call(null,reagent.session.state,ks);
});
/**
 * Get the key's value from the session, returns nil if it doesn't exist.
 */
reagent.session.get = (function reagent$session$get(var_args){
var args__6684__auto__ = [];
var len__6677__auto___21453 = arguments.length;
var i__6678__auto___21454 = (0);
while(true){
if((i__6678__auto___21454 < len__6677__auto___21453)){
args__6684__auto__.push((arguments[i__6678__auto___21454]));

var G__21455 = (i__6678__auto___21454 + (1));
i__6678__auto___21454 = G__21455;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return reagent.session.get.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

reagent.session.get.cljs$core$IFn$_invoke$arity$variadic = (function (k,p__21449){
var vec__21450 = p__21449;
var default$ = cljs.core.nth.call(null,vec__21450,(0),null);
var temp_a = reagent.session.cursor.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [k], null));
if(!((cljs.core.deref.call(null,temp_a) == null))){
return cljs.core.deref.call(null,temp_a);
} else {
return default$;
}
});

reagent.session.get.cljs$lang$maxFixedArity = (1);

reagent.session.get.cljs$lang$applyTo = (function (seq21447){
var G__21448 = cljs.core.first.call(null,seq21447);
var seq21447__$1 = cljs.core.next.call(null,seq21447);
return reagent.session.get.cljs$core$IFn$_invoke$arity$variadic(G__21448,seq21447__$1);
});

reagent.session.put_BANG_ = (function reagent$session$put_BANG_(k,v){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.assoc,k,v);
});
/**
 * Gets the value at the path specified by the vector ks from the session,
 *   returns nil if it doesn't exist.
 */
reagent.session.get_in = (function reagent$session$get_in(var_args){
var args__6684__auto__ = [];
var len__6677__auto___21462 = arguments.length;
var i__6678__auto___21463 = (0);
while(true){
if((i__6678__auto___21463 < len__6677__auto___21462)){
args__6684__auto__.push((arguments[i__6678__auto___21463]));

var G__21464 = (i__6678__auto___21463 + (1));
i__6678__auto___21463 = G__21464;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic = (function (ks,p__21458){
var vec__21459 = p__21458;
var default$ = cljs.core.nth.call(null,vec__21459,(0),null);
var or__5602__auto__ = cljs.core.deref.call(null,reagent.session.cursor.call(null,ks));
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
return default$;
}
});

reagent.session.get_in.cljs$lang$maxFixedArity = (1);

reagent.session.get_in.cljs$lang$applyTo = (function (seq21456){
var G__21457 = cljs.core.first.call(null,seq21456);
var seq21456__$1 = cljs.core.next.call(null,seq21456);
return reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic(G__21457,seq21456__$1);
});

/**
 * Replace the current session's value with the result of executing f with
 *   the current value and args.
 */
reagent.session.swap_BANG_ = (function reagent$session$swap_BANG_(var_args){
var args__6684__auto__ = [];
var len__6677__auto___21467 = arguments.length;
var i__6678__auto___21468 = (0);
while(true){
if((i__6678__auto___21468 < len__6677__auto___21467)){
args__6684__auto__.push((arguments[i__6678__auto___21468]));

var G__21469 = (i__6678__auto___21468 + (1));
i__6678__auto___21468 = G__21469;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (f,args){
return cljs.core.apply.call(null,cljs.core.swap_BANG_,reagent.session.state,f,args);
});

reagent.session.swap_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.swap_BANG_.cljs$lang$applyTo = (function (seq21465){
var G__21466 = cljs.core.first.call(null,seq21465);
var seq21465__$1 = cljs.core.next.call(null,seq21465);
return reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__21466,seq21465__$1);
});

/**
 * Remove all data from the session and start over cleanly.
 */
reagent.session.clear_BANG_ = (function reagent$session$clear_BANG_(){
return cljs.core.reset_BANG_.call(null,reagent.session.state,cljs.core.PersistentArrayMap.EMPTY);
});
reagent.session.reset_BANG_ = (function reagent$session$reset_BANG_(m){
return cljs.core.reset_BANG_.call(null,reagent.session.state,m);
});
/**
 * Remove a key from the session
 */
reagent.session.remove_BANG_ = (function reagent$session$remove_BANG_(k){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.dissoc,k);
});
/**
 * Associates a value in the session, where ks is a
 * sequence of keys and v is the new value and returns
 * a new nested structure. If any levels do not exist,
 * hash-maps will be created.
 */
reagent.session.assoc_in_BANG_ = (function reagent$session$assoc_in_BANG_(ks,v){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.assoc_in,ks,v);
});
/**
 * Destructive get from the session. This returns the current value of the key
 *   and then removes it from the session.
 */
reagent.session.get_BANG_ = (function reagent$session$get_BANG_(var_args){
var args__6684__auto__ = [];
var len__6677__auto___21476 = arguments.length;
var i__6678__auto___21477 = (0);
while(true){
if((i__6678__auto___21477 < len__6677__auto___21476)){
args__6684__auto__.push((arguments[i__6678__auto___21477]));

var G__21478 = (i__6678__auto___21477 + (1));
i__6678__auto___21477 = G__21478;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (k,p__21472){
var vec__21473 = p__21472;
var default$ = cljs.core.nth.call(null,vec__21473,(0),null);
var cur = reagent.session.get.call(null,k,default$);
reagent.session.remove_BANG_.call(null,k);

return cur;
});

reagent.session.get_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.get_BANG_.cljs$lang$applyTo = (function (seq21470){
var G__21471 = cljs.core.first.call(null,seq21470);
var seq21470__$1 = cljs.core.next.call(null,seq21470);
return reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__21471,seq21470__$1);
});

/**
 * Destructive get from the session. This returns the current value of the path
 *   specified by the vector ks and then removes it from the session.
 */
reagent.session.get_in_BANG_ = (function reagent$session$get_in_BANG_(var_args){
var args__6684__auto__ = [];
var len__6677__auto___21485 = arguments.length;
var i__6678__auto___21486 = (0);
while(true){
if((i__6678__auto___21486 < len__6677__auto___21485)){
args__6684__auto__.push((arguments[i__6678__auto___21486]));

var G__21487 = (i__6678__auto___21486 + (1));
i__6678__auto___21486 = G__21487;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((1) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((1)),(0),null)):null);
return reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__6685__auto__);
});

reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ks,p__21481){
var vec__21482 = p__21481;
var default$ = cljs.core.nth.call(null,vec__21482,(0),null);
var cur = reagent.session.get_in.call(null,ks,default$);
reagent.session.assoc_in_BANG_.call(null,ks,null);

return cur;
});

reagent.session.get_in_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.get_in_BANG_.cljs$lang$applyTo = (function (seq21479){
var G__21480 = cljs.core.first.call(null,seq21479);
var seq21479__$1 = cljs.core.next.call(null,seq21479);
return reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__21480,seq21479__$1);
});

/**
 * Updates a value in session where k is a key and f
 * is the function that takes the old value along with any
 * supplied args and return the new value. If key is not
 * present it will be added.
 */
reagent.session.update_BANG_ = (function reagent$session$update_BANG_(var_args){
var args__6684__auto__ = [];
var len__6677__auto___21492 = arguments.length;
var i__6678__auto___21493 = (0);
while(true){
if((i__6678__auto___21493 < len__6677__auto___21492)){
args__6684__auto__.push((arguments[i__6678__auto___21493]));

var G__21494 = (i__6678__auto___21493 + (1));
i__6678__auto___21493 = G__21494;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((2) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((2)),(0),null)):null);
return reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__6685__auto__);
});

reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (k,f,args){
return cljs.core.swap_BANG_.call(null,reagent.session.state,(function (p1__21488_SHARP_){
return cljs.core.apply.call(null,cljs.core.partial.call(null,cljs.core.update,p1__21488_SHARP_,k,f),args);
}));
});

reagent.session.update_BANG_.cljs$lang$maxFixedArity = (2);

reagent.session.update_BANG_.cljs$lang$applyTo = (function (seq21489){
var G__21490 = cljs.core.first.call(null,seq21489);
var seq21489__$1 = cljs.core.next.call(null,seq21489);
var G__21491 = cljs.core.first.call(null,seq21489__$1);
var seq21489__$2 = cljs.core.next.call(null,seq21489__$1);
return reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__21490,G__21491,seq21489__$2);
});

/**
 * 'Updates a value in the session, where ks is a
 * sequence of keys and f is a function that will
 * take the old value along with any supplied args and return
 * the new value. If any levels do not exist, hash-maps
 * will be created.
 */
reagent.session.update_in_BANG_ = (function reagent$session$update_in_BANG_(var_args){
var args__6684__auto__ = [];
var len__6677__auto___21499 = arguments.length;
var i__6678__auto___21500 = (0);
while(true){
if((i__6678__auto___21500 < len__6677__auto___21499)){
args__6684__auto__.push((arguments[i__6678__auto___21500]));

var G__21501 = (i__6678__auto___21500 + (1));
i__6678__auto___21500 = G__21501;
continue;
} else {
}
break;
}

var argseq__6685__auto__ = ((((2) < args__6684__auto__.length))?(new cljs.core.IndexedSeq(args__6684__auto__.slice((2)),(0),null)):null);
return reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__6685__auto__);
});

reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ks,f,args){
return cljs.core.swap_BANG_.call(null,reagent.session.state,(function (p1__21495_SHARP_){
return cljs.core.apply.call(null,cljs.core.partial.call(null,cljs.core.update_in,p1__21495_SHARP_,ks,f),args);
}));
});

reagent.session.update_in_BANG_.cljs$lang$maxFixedArity = (2);

reagent.session.update_in_BANG_.cljs$lang$applyTo = (function (seq21496){
var G__21497 = cljs.core.first.call(null,seq21496);
var seq21496__$1 = cljs.core.next.call(null,seq21496);
var G__21498 = cljs.core.first.call(null,seq21496__$1);
var seq21496__$2 = cljs.core.next.call(null,seq21496__$1);
return reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__21497,G__21498,seq21496__$2);
});


//# sourceMappingURL=session.js.map