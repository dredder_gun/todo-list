// Compiled by ClojureScript 1.9.89 {}
goog.provide('reagent.dom');
goog.require('cljs.core');
goog.require('reagent.impl.util');
goog.require('reagent.interop');
goog.require('reagent.ratom');
goog.require('reagent.impl.template');
goog.require('reagent.impl.batching');
goog.require('cljsjs.react.dom');
goog.require('reagent.debug');
if(typeof reagent.dom.imported !== 'undefined'){
} else {
reagent.dom.imported = null;
}
reagent.dom.module = (function reagent$dom$module(){
if(cljs.core.some_QMARK_.call(null,reagent.dom.imported)){
return reagent.dom.imported;
} else {
if(typeof ReactDOM !== 'undefined'){
return reagent.dom.imported = ReactDOM;
} else {
if(typeof require !== 'undefined'){
var or__5602__auto__ = reagent.dom.imported = require("react-dom");
if(cljs.core.truth_(or__5602__auto__)){
return or__5602__auto__;
} else {
throw (new Error("require('react-dom') failed"));
}
} else {
throw (new Error("js/ReactDOM is missing"));

}
}
}
});
if(typeof reagent.dom.roots !== 'undefined'){
} else {
reagent.dom.roots = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
reagent.dom.unmount_comp = (function reagent$dom$unmount_comp(container){
cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.dissoc,container);

return (reagent.dom.module.call(null)["unmountComponentAtNode"])(container);
});
reagent.dom.render_comp = (function reagent$dom$render_comp(comp,container,callback){
var _STAR_always_update_STAR_9355 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = true;

try{return (reagent.dom.module.call(null)["render"])(comp.call(null),container,((function (_STAR_always_update_STAR_9355){
return (function (){
var _STAR_always_update_STAR_9356 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = false;

try{cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.assoc,container,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [comp,container], null));

reagent.impl.batching.flush_after_render.call(null);

if(cljs.core.some_QMARK_.call(null,callback)){
return callback.call(null);
} else {
return null;
}
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_9356;
}});})(_STAR_always_update_STAR_9355))
);
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_9355;
}});
reagent.dom.re_render_component = (function reagent$dom$re_render_component(comp,container){
return reagent.dom.render_comp.call(null,comp,container,null);
});
/**
 * Render a Reagent component into the DOM. The first argument may be
 *   either a vector (using Reagent's Hiccup syntax), or a React element. The second argument should be a DOM node.
 * 
 *   Optionally takes a callback that is called when the component is in place.
 * 
 *   Returns the mounted component instance.
 */
reagent.dom.render = (function reagent$dom$render(var_args){
var args9357 = [];
var len__6677__auto___9360 = arguments.length;
var i__6678__auto___9361 = (0);
while(true){
if((i__6678__auto___9361 < len__6677__auto___9360)){
args9357.push((arguments[i__6678__auto___9361]));

var G__9362 = (i__6678__auto___9361 + (1));
i__6678__auto___9361 = G__9362;
continue;
} else {
}
break;
}

var G__9359 = args9357.length;
switch (G__9359) {
case 2:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args9357.length)].join('')));

}
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$2 = (function (comp,container){
return reagent.dom.render.call(null,comp,container,null);
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$3 = (function (comp,container,callback){
reagent.ratom.flush_BANG_.call(null);

var f = (function (){
return reagent.impl.template.as_element.call(null,((cljs.core.fn_QMARK_.call(null,comp))?comp.call(null):comp));
});
return reagent.dom.render_comp.call(null,f,container,callback);
});

reagent.dom.render.cljs$lang$maxFixedArity = 3;

reagent.dom.unmount_component_at_node = (function reagent$dom$unmount_component_at_node(container){
return reagent.dom.unmount_comp.call(null,container);
});
/**
 * Returns the root DOM node of a mounted component.
 */
reagent.dom.dom_node = (function reagent$dom$dom_node(this$){
return (reagent.dom.module.call(null)["findDOMNode"])(this$);
});
reagent.impl.template.find_dom_node = reagent.dom.dom_node;
/**
 * Force re-rendering of all mounted Reagent components. This is
 *   probably only useful in a development environment, when you want to
 *   update components in response to some dynamic changes to code.
 * 
 *   Note that force-update-all may not update root components. This
 *   happens if a component 'foo' is mounted with `(render [foo])` (since
 *   functions are passed by value, and not by reference, in
 *   ClojureScript). To get around this you'll have to introduce a layer
 *   of indirection, for example by using `(render [#'foo])` instead.
 */
reagent.dom.force_update_all = (function reagent$dom$force_update_all(){
reagent.ratom.flush_BANG_.call(null);

var seq__9368_9372 = cljs.core.seq.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,reagent.dom.roots)));
var chunk__9369_9373 = null;
var count__9370_9374 = (0);
var i__9371_9375 = (0);
while(true){
if((i__9371_9375 < count__9370_9374)){
var v_9376 = cljs.core._nth.call(null,chunk__9369_9373,i__9371_9375);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_9376);

var G__9377 = seq__9368_9372;
var G__9378 = chunk__9369_9373;
var G__9379 = count__9370_9374;
var G__9380 = (i__9371_9375 + (1));
seq__9368_9372 = G__9377;
chunk__9369_9373 = G__9378;
count__9370_9374 = G__9379;
i__9371_9375 = G__9380;
continue;
} else {
var temp__4657__auto___9381 = cljs.core.seq.call(null,seq__9368_9372);
if(temp__4657__auto___9381){
var seq__9368_9382__$1 = temp__4657__auto___9381;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__9368_9382__$1)){
var c__6413__auto___9383 = cljs.core.chunk_first.call(null,seq__9368_9382__$1);
var G__9384 = cljs.core.chunk_rest.call(null,seq__9368_9382__$1);
var G__9385 = c__6413__auto___9383;
var G__9386 = cljs.core.count.call(null,c__6413__auto___9383);
var G__9387 = (0);
seq__9368_9372 = G__9384;
chunk__9369_9373 = G__9385;
count__9370_9374 = G__9386;
i__9371_9375 = G__9387;
continue;
} else {
var v_9388 = cljs.core.first.call(null,seq__9368_9382__$1);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_9388);

var G__9389 = cljs.core.next.call(null,seq__9368_9382__$1);
var G__9390 = null;
var G__9391 = (0);
var G__9392 = (0);
seq__9368_9372 = G__9389;
chunk__9369_9373 = G__9390;
count__9370_9374 = G__9391;
i__9371_9375 = G__9392;
continue;
}
} else {
}
}
break;
}

return "Updated";
});

//# sourceMappingURL=dom.js.map