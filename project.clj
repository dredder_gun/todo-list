(defproject todomvc-re-frame "0.9.0"
  :dependencies [[org.clojure/clojure        "1.8.0"]
                 [org.clojure/clojurescript  "1.9.89"]
                 [reagent "0.6.0-rc"]
                 [reagent-utils "0.2.1"]
                 [re-frame "0.9.0"]
                 [binaryage/devtools "0.8.1"]
                 [secretary "1.2.3"]
                 [re-com "2.0.0"]
                 [com.pupeno/free-form "0.5.0"]

                 [ring/ring-core "1.2.1"]
                 [ring/ring-jetty-adapter "1.2.1"]
                 [ring/ring-defaults "0.3.0"]
                 [compojure "1.1.6"]
                 [ring/ring-json "0.2.0"]
                 [korma "0.4.3"]
                 [org.postgresql/postgresql "9.2-1002-jdbc4"]
                 [ragtime "0.6.0"]
                 [environ "0.4.0"]
                 [buddy "1.3.0"]
                 [cljs-ajax "0.5.9"]
                 [org.clojure/tools.logging "0.3.1"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jmdk/jmxtools
                                                    com.sun.jmx/jmxri]]

                 [org.clojure/java.jdbc "0.7.0-alpha3"]]

  :plugins [[lein-cljsbuild "1.1.4"]
            [lein-garden "0.3.0"]
            [lein-figwheel "0.5.10"]]

  :hooks [leiningen.cljsbuild]

  :profiles {:dev  {:cljsbuild
                    {:builds {:client {:compiler {:asset-path           "js"
                                                  :optimizations        :none
                                                  :source-map           true
                                                  :source-map-timestamp true
                                                  :main                 "todomvc.core"}
                                       :figwheel {:on-jsload "todomvc.core/main"}}}}}

             :prod {:cljsbuild
                    {:builds {:client {:compiler {:optimizations :advanced
                                                  :elide-asserts true
                                                  :pretty-print  false}}}}}
             :test {:ragtime {:migrations ragtime.sql.files/migrations
                        :database "jdbc:postgresql://localhost:5432/aleksandr_todos?user=todos_user&password=0000"}}}

  :figwheel {:css-dirs ["resources/public/"]
             :ring-handler backend.handler/figwheel-app
             :server-port 3450
             :repl        true}

  :clean-targets ^{:protect false} ["resources/public/js" "target"]

  :cljsbuild {:builds {:client {:source-paths ["src/frontend"]
                                :compiler     {:output-dir "resources/public/js"
                                               :output-to  "resources/public/js/client.js"}}}}

  :aliases {"migrate"  ["run" "-m" "backend.migrations/migrate"]
            "rollback"  ["run" "-m" "backend.migrations/rollback"]}

  :uberjar-name "todos-standalone.jar"
  :main backend.handler
  :aot [backend.handler]

  :garden {:builds [{
                    :id "screen"
                    :source-paths ["src"]
                    :stylesheet todomvc.css/screen
                    :compiler {
                               :output-to "resources/public/additional.css"
                               :pretty-print? false}}]})
