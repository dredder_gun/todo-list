(ns backend.db
  (:use korma.db)
  (:require [environ.core :refer [env]]))

(defdb db (postgres {:db (get env :restful-db "aleksandr_todos")
                     :user (get env :restful-db-user "todos_user")
                     :password (get env :restful-db-pass "0000")
                     :host (get env :restful-db-host "localhost")
                     :port (get env :restful-db-port 5432)}))
