(ns backend.handler
  (:gen-class)
  (:use ring.middleware.params
        ring.middleware.resource
        ring.adapter.jetty
        ring.middleware.defaults)
  (:require [compojure.core :refer :all]
            [clojure.java.io :as io]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [clojure.tools.logging :as log]
            [ring.util.response :as resp]
            [backend.models.users :as models-users]))

(defn create-user [{user :params}]
  (log/info "Поданы данные с формы регистрации" user)
  (let [new-user (models-users/create user)]
    (resp/response new-user)))

(defn middleware-add-new-body [handler body]
  (fn [request]
    (let [response (handler request)]
      (assoc response :body body))))

(defn middleware-autorized? [handler]
  (fn [request]
    (log/info "This request is after handle" request)
    (if-not (:session request)
      (assoc request :body (:params request))
      (-> (resp/response "Access Denied")
          (resp/status 403)))))

(defn main-handler [request]
  {:status 200
   :headers {"Content-Type" "text/html; charset=utf-8"}
   :body (io/input-stream (io/resource "public/index.html"))})

(defroutes main-routes
  (GET "/" [] (resp/content-type (resp/resource-response "index.html" {:root "public"}) "text/html"))
  (GET "/assets/css/re-com.css" [] )
  (GET "/foobar" [x y & z] (str x ", " y ", " z))
  (POST "/send-message" request (create-user request)))

(defn debug-response
  [arg]
  (println "Отладочный из middleware " arg)
  arg)

  ; (defn wrap-https
  ;   [client-fn]
  ;   (fn [request]
  ;     (let [site (:site request)
  ;           new-site (.replaceAll site "http:" "https:")
  ;           new-request (assoc request :site new-site)]
  ;       (client-fn new-request))))

(defn figwheel-app [request]
  (-> main-routes
      (wrap-resource "public")
      (wrap-defaults site-defaults)))

(defn -main [port] (run-jetty (wrap-defaults main-routes site-defaults) {:port 3451}))
