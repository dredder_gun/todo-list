(ns backend.db-mysql
  (:require [clojure.java.jdbc :as j]))

;; there are many ways to write a db-spec but the easiest way is to
;; use :dbtype and then provide the :dbname and any of :user, :password,
;; :host, :port, and other options as needed:
(def mysql-db {:dbtype "root"
               :dbname "new_schema"
               :user "root"
               :password "89247013710"})

(println "Я в db-mysql.clj")

(j/insert-multi! mysql-db :todos-lists
  [{:todos-items "Apple" :todos-user "rosy"}
   {:todos-items "Apple" :todos-user "rosy"}])
;; ({:generated_key 1} {:generated_key 2})
;
; (j/query mysql-db
;   ["select * from fruit where appearance = ?" "rosy"]
;   {:row-fn :cost})
;; (24)
