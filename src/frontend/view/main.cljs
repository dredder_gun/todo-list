(ns todomvc.frontend.view
  (:require [reagent.core       :as reagent]
            [re-frame.core      :refer [subscribe dispatch]]
            [re-com.modal-panel :refer [modal-panel-args-desc]]
            [re-com.core   :refer [h-box v-box box gap line input-text input-password input-textarea label checkbox radio-button slider title p button border modal-panel]]
            [reagent.core       :as reagent]
            [free-form.re-frame :as free-form]
            ;[todomvc.events :refer [ui-dispatch]]
            free-form.bootstrap-3
            [ajax.core :refer [GET POST ajax-request json-request-format json-response-format url-request-format]]))

(defn todo-app
  []
  (let [aut? @(subscribe [:login-password])
      data (subscribe [:re-frame-bootstrap-3])]
    (if (= aut? "true")
      [:div
       [button
        :label "Выйти"
        :class "btn-primary"
        :on-click #(dispatch [:exit-event])]
       [list-todos]
       [:section#todoapp
        [task-entry]
        (when (seq @(subscribe [:todos]))
          [task-list])
        [footer-controls]]
       [:footer#info
        [:p "Двойной клик для редактирования заметки"]]]
       [:div
         [:h1#enter-header {:style {:text-align "center" }} "заметки"]
         [modal-dialog]])))
